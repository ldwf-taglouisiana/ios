//
//  SpeciesDetailInfoTableViewCell.swift
//  taglouisiana
//
//  Created by Daniel Ward on 11/21/16.
//  Copyright © 2016 Daniel Ward. All rights reserved.
//

import UIKit

class SpeciesDetailInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var textViewElement: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
