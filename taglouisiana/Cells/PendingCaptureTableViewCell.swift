//
//  PendingCaptureTableViewCell.swift
//  taglouisiana
//
//  Created by Daniel Ward on 4/10/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import UIKit

class PendingCaptureTableViewCell: UITableViewCell {

   
    @IBOutlet weak var tagNumber: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var speciesName: UILabel!
    @IBOutlet weak var statusIcon: UIImageView!
    @IBOutlet weak var hasImagesIcon: UIImageView!
    @IBOutlet weak var editingTimeLeft: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
