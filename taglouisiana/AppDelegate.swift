//
//  AppDelegate.swift
//  taglouisiana
//
//  Created by Daniel Ward on 3/9/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import UIKit
import NXOAuth2Client
import Alamofire
import MagicalRecord
import CoreLocation
import CryptoSwift
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate {
    
    var window: UIWindow?
    var locationManager: CLLocationManager?
    
    override init(){
        super.init()
        
        self.setupOauth()
        self.generateAndSaveApiToken()
    }
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {        
        // initialize the crashlytics framework
        Fabric.with([Crashlytics.self])

        // setup CoreData
        MagicalRecord.setLoggingLevel(MagicalRecordLoggingLevel.Verbose)
        MagicalRecord.setupAutoMigratingStackWithSQLiteStoreNamed(Constants.STORE_NAME)
        
        // changing the status bar color to white to show up better with the darker theme
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: false)
        
        self.locationManager = CLLocationManager()
        self.locationManager?.requestWhenInUseAuthorization()
        
        let userAgent = UIWebView().stringByEvaluatingJavaScriptFromString("navigator.userAgent")!
        NSUserDefaults.standardUserDefaults().registerDefaults(["UserAgent" : userAgent])
        
        // setup the split view controllers
        self.initializeSplitViewControllers()
        
        NSNotificationCenter.defaultCenter().addObserverForName(Constants.NOTIFICATION_DATA_UPDATE_FINISHED, object: nil, queue: nil, usingBlock: { notificaton in
            self.updatePendingTabItemBadge()    
        })
        
        return true
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AppDelegate.managedObjectContextDidChange(_:)), name: NSManagedObjectContextDidSaveNotification, object: NSManagedObjectContext.defaultContext())
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AppDelegate.forceSyncStarted(_:)), name: Constants.NOTIFICATION_FORCE_SYNC_STARTED, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AppDelegate.forceSyncCompleted(_:)), name: Constants.NOTIFICATION_FORCE_SYNC_FINISHED, object: nil)
        
        Api.datastoreQueue.restart()
        Api.networkQueue.restart()
        
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        self.checkForUserSignIn()
        self.updatePendingTabItemBadge()

        Utils.delay(2.0, closure: {
            Utils.syncData()
            Utils.setupSDWebImageManager()
        })
    }
    
    func applicationWillTerminate(application: UIApplication) {
        Api.datastoreQueue.cancel()
        Api.networkQueue.cancel()
        
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        MagicalRecord.cleanUp()
    }
    
    // MARK: - Split view
    
    func splitViewController(splitViewController: UISplitViewController, collapseSecondaryViewController secondaryViewController:UIViewController, ontoPrimaryViewController primaryViewController:UIViewController) -> Bool {
       
        // if the device is an iPhone, then start the secondary controller collapsed
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return true
        }
        
        if let secondaryAsNavController = secondaryViewController as? UINavigationController {
            
            switch secondaryAsNavController.topViewController {
            case let controller as CaptureDetailPager:
                return controller.captureItem == nil
            case let controller as CapturesDetailViewController:
                return controller.detailItem == nil
            case let controller as SpeciesDetailViewController:
                return controller.detailItem == nil
            default:
                false
            }
            
        }
        return false
    }

    // MARK: - Helpers

    /**
        Sets up the UISplitViewControllers.
        Will preset the master first when using handsets and will present them sid by side on tablets
    */
    private func initializeSplitViewControllers() {
        if let tabViewController = self.window!.rootViewController as? UITabBarController {
            let controllers = tabViewController.viewControllers!
            
            let tabBar: UITabBar = tabViewController.tabBar as UITabBar
            tabBar.tintColor = Constants.UICOLOR_ORANGE
            
            for controller in controllers {
                
                if controller is UISplitViewController {
                    let splitViewController = controller as! UISplitViewController
                    let navigationController = splitViewController.viewControllers[splitViewController.viewControllers.count-1] as! UINavigationController
                    navigationController.topViewController!.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem()
                    splitViewController.delegate = self
                }
                
            }
        }
    }

    private func checkForUserSignIn() {
        if (!AuthUtils.isSignedIn()) {
            if let tabViewController = self.window!.rootViewController as? UITabBarController {
                tabViewController.selectedIndex = 0
                
                tabViewController.performSegueWithIdentifier(Constants.SEGUE_LOGIN, sender: tabViewController)
            }
        }
    }
    
    func managedObjectContextDidChange(sender: NSNotification) {
        self.updatePendingTabItemBadge()
    }
    
    func forceSyncCompleted(sender: NSNotification) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: NSManagedObjectContextDidSaveNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AppDelegate.managedObjectContextDidChange(_:)), name: NSManagedObjectContextDidSaveNotification, object: NSManagedObjectContext.defaultContext())
        
        self.updatePendingTabItemBadge()
        
        if let tabViewController = self.window!.rootViewController as? UITabBarController {
            tabViewController.selectedIndex = 0
        }
    }
    
    func syncStarted(sender: NSNotification) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    func syncFinished(sender: NSNotification) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
    
    func forceSyncStarted(sender: NSNotification) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: NSManagedObjectContextObjectsDidChangeNotification, object: nil)
    }
    
    private func updatePendingTabItemBadge() {
        // get the pending capture count
        let enteredDraftCapture = EnteredDraftCapture.MR_findAllWithPredicate(NSPredicate(format: "shouldDelete == NO", argumentArray: [])) as! [EnteredDraftCapture]
        let draftCaptures = DraftCapture.findAllWhereNotDelted() as [DraftCapture]
        
        let pendingCaptureCount = draftCaptures.count + enteredDraftCapture.count
        
        if let tabViewController = self.window!.rootViewController as? UITabBarController {
            if let tabItems = tabViewController.tabBar.items {
                for (index, tabItem) in (tabItems ).enumerate() {
                    if tabItem.tag == Constants.TAG_PENDING_CAPTURE_TAB {
                        
                        /*
                            If the count = 0, then we need to remove the label.
                            Else we will get the pending capture tab and set the badge value
                        */
                        if pendingCaptureCount == 0 {
                            tabItem.badgeValue = nil
                            tabItem.enabled = false
                            
                            if tabViewController.selectedIndex == index {
                                let controllers = tabViewController.viewControllers as [AnyObject]?
                                let splitView = controllers![index] as! UISplitViewController
                                splitView.toggleMasterView()
                                
                                Utils.delay(0.5, closure: {
                                    tabViewController.selectedIndex = 0
                                })
                            }
                            
                        } else {
                            tabItem.badgeValue = String(pendingCaptureCount)
                            tabItem.enabled = true
                            
                        }
                    }
                }
            }
        }
    }

    /**
        Sets up the OAuth Client.
    */
    private func setupOauth() {
        let oauthStore = NXOAuth2AccountStore.sharedStore() as! NXOAuth2AccountStore
        
        oauthStore.setClientID(
            Constants.CLIENT_ID,
            secret: Constants.CLIENT_SECRET,
            authorizationURL: NSURL(string: Constants.SERVER_ADDRESS + "/oauth/authorize"),
            tokenURL: NSURL(string: Constants.SERVER_ADDRESS + "/oauth/token"),
            redirectURL: NSURL(string: "myapp://taglouisiana-callback"),
            forAccountType: Constants.AOUTH_ACCOUNT
        )
    }

    /**
        Generate the hash token and saves it to NSUserDefaults under the key Constants.DEFAULTS_KEY_API_TOKEN
    */
    private func generateAndSaveApiToken() {
        let defaults = NSUserDefaults.standardUserDefaults()
        let token = generateTokenWithClientId()

        // if the token is non empty, then save it
        if !token.isEmpty {
            defaults.setObject(token, forKey: Constants.DEFAULTS_KEY_API_TOKEN)
        }
    }

    /**
        Generate the hash token based on the client id and secret to present to the server

        - returns: If successfull returns a non empty string, else returns an empty string
    */
    private func generateTokenWithClientId() -> String {
        let data = Constants.CLIENT_ID + Constants.CLIENT_SECRET
        
        return data.sha256();
    }
}

