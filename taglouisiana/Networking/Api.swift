//
//  Api.swift
//  taglouisiana
//
//  Created by Daniel Ward on 3/24/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import Foundation
import Alamofire
import MagicalRecord
import SwiftyJSON
import Fabric
import NSString_Ruby

enum Api {
    static let networkQueue = TaskQueue()
    static let datastoreQueue = TaskQueue()
    
    case FetchPublicDashboard
    case FetchAnglerDashboard
    case FetchPublicData
    case FetchUserData
    case SyncPublicDataIdsWithRemote
    case SyncUserDataIdsWithRemote
    case FetchTagHistory([String: AnyObject])
    case PostData
    
    static let allValues = [FetchPublicDashboard, FetchAnglerDashboard, FetchTagHistory([String:String]()), FetchPublicData, FetchUserData, SyncPublicDataIdsWithRemote, SyncUserDataIdsWithRemote, SyncUserDataIdsWithRemote]
    
    var etagStorageKey : String? {
        switch self {
        case .FetchPublicDashboard:
            return Constants.DEFAULTS_KEY_API_ETAG_PUBLIC_DASHBOARD
        case .FetchAnglerDashboard:
            return Constants.DEFAULTS_KEY_API_ETAG_ANGLER_DASHBOARD
        case .FetchPublicData:
            return Constants.DEFAULTS_KEY_API_ETAG_PUBLIC_DATA
        case .FetchUserData:
            return Constants.DEFAULTS_KEY_API_ETAG_USER_DATA
        case .SyncPublicDataIdsWithRemote:
            return Constants.DEFAULTS_KEY_API_ETAG_PUBLIC_IDS
        case .SyncUserDataIdsWithRemote:
            return Constants.DEFAULTS_KEY_API_ETAG_USER_IDS
        default:
            return nil
        }
    }
    
    var parameters: [String: AnyObject] {
        switch self {
        case .FetchPublicDashboard, .FetchAnglerDashboard:
            return [String:AnyObject]()
            
        case .FetchPublicData:
            return publicFetchParams()
            
        case .FetchUserData:
            return protectedFetchParams()
            
        case .SyncPublicDataIdsWithRemote:
            return syncPublicIdsParams()
            
        case .SyncUserDataIdsWithRemote:
            return syncProtectedIdsParams()
            
        case .PostData:
            return postParams()
            
        case .FetchTagHistory(let parameters):
            return parameters
        }
    }
    
    var request: Request {
        switch self {
        case .FetchPublicDashboard:
            return Router.PublicDashboard.request
            
        case .FetchAnglerDashboard:
            return Router.AnglerDashboard.request
            
        case .FetchPublicData:
            return Router.FetchPublicData(parameters).request
            
        case .FetchUserData:
            return Router.FetchUserData(parameters).request
            
        case .SyncPublicDataIdsWithRemote:
            return Router.SyncPublicIds(parameters).request
            
        case .SyncUserDataIdsWithRemote:
            return Router.SyncUserIds(parameters).request
            
        case .PostData:
            return Router.PostData(parameters).request
            
        case .FetchTagHistory:
            return Router.TagHistory(parameters).request
        }
    }
    
    var shoudlCommitToDatabase : Bool {
        switch self {
        case .FetchPublicDashboard, .FetchAnglerDashboard, .FetchTagHistory:
            return false
        default:
            return true
        }
    }
    var rawValue : Int {
        get {
            switch self {
            case .FetchPublicDashboard:
                return 0
                
            case .FetchAnglerDashboard:
                return 1
                
            case .FetchPublicData:
                return 2
                
            case .FetchUserData:
                return 3
                
            case .SyncPublicDataIdsWithRemote:
                return 4
                
            case .SyncUserDataIdsWithRemote:
                return 5
                
            case .PostData:
                return 6
                
            case .FetchTagHistory:
                return 7

                
            }
        }
    }
    
    func execute(){
        self.execute(true, completion: {})
    }
    
    func execute(completion: (JSON) -> (Void)){
        self.execute(true, completionWithResult: completion)
    }
    
    func execute(shouldNotify : Bool = true, completion: (Void) -> (Void)) {
        self.execute(true, completionWithResult: { json in completion() })
    }
    
    func execute(shouldNotify : Bool = true, completionWithResult: (JSON) -> (Void)) {
        Api.networkQueue.tasks +=~ { result, next in
            if shouldNotify {
                NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_DATA_UPDATE_STARTED, object: nil)
            }
            
            /*
             If this is a post data action and we don't have any data to upload,
             then we can return and igonore this request
             */
            if (self.parameters.isEmpty && Api.PostData.rawValue == self.rawValue) {
                self.executeCleanup(shouldNotify, completion: completionWithResult, next: next)
                return
            }
            
            // make the request and get the result as JSON
            self.request.responseJSON { (response) in
                if Api.PostData.rawValue == self.rawValue {
                    Utils.log(String(response))
                }
                
                //------------------------------------------------------------------------------------
                // Compare the eTag with the stored eTag
                
                /*
                 Compare the saved eTag with the eTag from the response.
                 If they match then we have gotten this request before.
                 */
                if let serverResponse = response.response,
                    etagkey = self.etagStorageKey,
                    storedEtag = NSUserDefaults.standardUserDefaults().valueForKey(etagkey) as? String,
                    serverEtag = serverResponse.allHeaderFields["Etag"] as? String {
                    
                    // if the eTags match then signal the syn is finshed and return. There is not need to do any database work,
                    // since the data did not change.
                    if (storedEtag == serverEtag ) {
                        self.executeCleanup(shouldNotify, completion: completionWithResult, next: next)
                        return
                    }
                }
                
                /*
                    If we do not need to commit the data to the database, then we can return the JSON value and
                    return. Else, continue on with the rest of the method
                 */
                if !self.shoudlCommitToDatabase {
                    if let resultData = response.result.value {
                        self.executeCleanup(self.shoudlCommitToDatabase, json: JSON(resultData), completion: completionWithResult, next: next)
                        return // FINISH THE REQUEST EXECUTION
                    }
                }
                
                
                //------------------------------------------------------------------------------------
                // Add a task to insert the data into the database.
                // We add the data to a queue, to prevent locking issues.
                
                /*
                 Process the data and save the items.
                 */
                Api.datastoreQueue.tasks +=~ {
                    if let resultData = response.result.value {
                        
                        let json = JSON(resultData)

                        
                        MagicalRecord.saveWithBlockAndWait({ context in
                            self.processData(json, inContext: context)
                        })
                        
                        self.executeCleanup(shouldNotify, completion: completionWithResult, next: next)
                        
                        // if this was a post data call, then we need to update the data with server changes.
                        if Api.PostData.rawValue == self.rawValue {
                            Api.FetchUserData.execute()
                        }
                        
                        //------------------------------------------------------------------------------------
                        // Save the etag after the request has finished processing.
                        
                        /*
                         Save the new etag, to use on the next request.
                         */
                        if let serverResponse = response.response,
                            etag = serverResponse.allHeaderFields["Etag"],
                            etagKey = self.etagStorageKey {
                            
                            let defaults = NSUserDefaults.standardUserDefaults()
                            defaults.setValue(etag, forKey: etagKey)
                            defaults.synchronize()
                        }
                    }
                    
                } // end datastore task add
                
                // start the queue, if not running
                Api.datastoreQueue.run()
                
            } // end request
            
        } // end queue insertion
        
        
        NSLog("Queue Count: %d", Api.networkQueue.count)
        // start the queue, if not running
        Api.networkQueue.run()
    }
    
    /*
     A common function for the various logic branchng in the execution.
     This notifies that a sync was finsihed, execute the callback, and advanced the task queue
     */
    private func executeCleanup(shouldNotify : Bool, completion: (Void) -> (Void), next: (AnyObject?) -> (Void)) {
        if shouldNotify {
            NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_DATA_UPDATE_FINISHED, object: nil)
        }
        
        // call the completion callback
        completion()
        
        // runt he next item, if any
        next(nil)
    }

    private func executeCleanup(shouldNotify : Bool, completion: (JSON) -> (Void), next: (AnyObject?) -> (Void)) {
        self.executeCleanup(shouldNotify, json: JSON("{}"), completion: completion, next: next)
    }
    
    private func executeCleanup(shouldNotify : Bool, json: JSON, completion: (JSON) -> (Void), next: (AnyObject?) -> (Void)) {
        if shouldNotify {
            NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_DATA_UPDATE_FINISHED, object: nil)
        }
        
        // call the completion callback
        completion(json)
        
        // runt he next item, if any
        next(nil)
    }
    
    /*
     PROCESS DATA HELPERS
     */
    
    private func processData(json: JSON, inContext context: NSManagedObjectContext) {
        switch self {
        case .FetchPublicData:
            processPublicData(json, inContext: context)
            
        case .FetchUserData:
            processProtectedData(json, inContext: context)
            
        case .SyncPublicDataIdsWithRemote:
            processSyncPublicDataIdsData(json, inContext: context)
            
        case .SyncUserDataIdsWithRemote:
            processSyncUserDataIdsData(json, inContext: context)
            
        case .PostData:
            processPostData(json, inContext: context)
        default:
            // left blank
            break
        }
    }
    
    private func processPublicData(json: JSON, inContext context: NSManagedObjectContext) {
        let species = json["species"]["items"]
        let speciesIdsHash = json["species"]["id_hash"].stringValue
        
        let speciesLengths = json["species_lengths"]["items"]
        let speciesLengthIdsHash = json["species_lengths"]["id_hash"].stringValue
        
        let fishConditonOptions = json["fish_conditions"]["items"]
        let fishConditonIdsHash = json["fish_conditions"]["id_hash"].stringValue
        
        let timeOfDayOptions = json["time_of_days"]["items"]
        let timeOfDayOptionIdsHash = json["time_of_days"]["id_hash"].stringValue
        
        let recaptureDispositionOptions = json["dispositions"]["items"]
        let recaptureDispositionIdsHash = json["dispositions"]["id_hash"].stringValue
        
        let shirtSizeOptions = json["shirt_sizes"]["items"]
        let shirtSizeOptionIdsHash = json["shirt_sizes"]["id_hash"].stringValue
        
        if let data = species.arrayObject {
            Species.MR_importFromArray(data, inContext: context)
        }
        
        if let data = speciesLengths.arrayObject {
            SpeciesLength.MR_importFromArray(data, inContext: context)
        }
        
        if let data = fishConditonOptions.arrayObject {
            FishConditionOption.MR_importFromArray(data, inContext: context)
        }
        
        if let data = timeOfDayOptions.arrayObject {
            TimeOfDayOption.MR_importFromArray(data, inContext: context)
        }
        
        if let data = recaptureDispositionOptions.arrayObject {
            RecaptureConditionOption.MR_importFromArray(data, inContext: context)
        }
        
        if let data = shirtSizeOptions.arrayObject {
            ShirtSize.MR_importFromArray(data, inContext: context)
        }
        
        /*
         We need to calculate the hash to make sure the data is in sync with the server
         */
        
        if  Species.remoteIdsHash(context).uppercaseString != speciesIdsHash.uppercaseString ||
            SpeciesLength.remoteIdsHash(context).uppercaseString != speciesLengthIdsHash.uppercaseString ||
            FishConditionOption.remoteIdsHash(context).uppercaseString != fishConditonIdsHash.uppercaseString ||
            TimeOfDayOption.remoteIdsHash(context).uppercaseString != timeOfDayOptionIdsHash.uppercaseString ||
            RecaptureConditionOption.remoteIdsHash(context).uppercaseString != recaptureDispositionIdsHash.uppercaseString ||
            ShirtSize.remoteIdsHash(context).uppercaseString != shirtSizeOptionIdsHash.uppercaseString{
            Api.SyncPublicDataIdsWithRemote.execute()
        }
    }
    
    private func processProtectedData(json: JSON, inContext context: NSManagedObjectContext) {
        let tags                = json["tags"]["items"]
        let tagsRemoteIdHash    = json["tags"]["id_hash"].stringValue
        
        let timeLogs                = json["time_logs"]["items"]
        let timeLogsRemoteIdHash    = json["time_logs"]["id_hash"].stringValue
        
        let tagRequests                = json["tag_requests"]["items"]
        let tagRequestsRemoteIdHash    = json["tag_requests"]["id_hash"].stringValue
        
        let captures                = json["captures"]["items"]
        let capturesRemoteIdHash    = json["captures"]["id_hash"].stringValue
        
        let draftCaptures       = json["draft_captures"]["items"]
        let draftRemoteIdHash   = json["draft_captures"]["id_hash"].stringValue
        
        let user                = json["me"]["user"]
        let angler              = json["me"]["angler"]
        
        if let data = tags.arrayObject {
            Tag.MR_importFromArray(data, inContext: context)
        }
        
        if let data = captures.arrayObject {
            Capture.MR_importFromArray(data, inContext: context)
        }
        
        if let data = draftCaptures.arrayObject {
            DraftCapture.MR_importFromArray(data, inContext: context)
        }
        
        if let data = tagRequests.arrayObject {
            UserTagRequest.MR_importFromArray(data, inContext: context)
        }
        
        if let data = timeLogs.arrayObject {
            TimeLogEntry.MR_importFromArray(data, inContext: context)
        }
        
        if let data = angler.dictionaryObject {
            Angler.MR_importFromObject(data, inContext: context)
        }
        
        if let data = user.dictionaryObject {
            User.MR_importFromObject(data, inContext: context)
        }
        
        /*
         We need to calculate the hash to make sure the data is in sync with the server
         */
        
        if  Tag.remoteIdsHash(context).uppercaseString != tagsRemoteIdHash.uppercaseString ||
            UserTagRequest.remoteIdsHash(context).uppercaseString != tagRequestsRemoteIdHash.uppercaseString ||
            TimeLogEntry.remoteIdsHash(context).uppercaseString != timeLogsRemoteIdHash.uppercaseString ||
            Capture.remoteIdsHash(context).uppercaseString != capturesRemoteIdHash.uppercaseString ||
            DraftCapture.remoteIdsHash(context).uppercaseString != draftRemoteIdHash.uppercaseString {
            Api.SyncUserDataIdsWithRemote.execute()
        }
    }
    
    private func processSyncPublicDataIdsData(json: JSON, inContext context: NSManagedObjectContext) {
        let speciesIds          = json["species"]["ids"]
        let speciesLengthsIds   = json["species_lengths"]["ids"]
        let timeOfDayIds        = json["time_of_days"]["ids"]
        let fishConditionsIds   = json["fish_conditions"]["ids"]
        let dispostionsIds      = json["dispositions"]["ids"]
        let shirtSizesIds       = json["shirt_sizes"]["ids"]
        
        if let data = speciesIds.arrayObject {
            Species.deleteAllExceptForIdList(data, inContext: context)
        }
        
        if let data = speciesLengthsIds.arrayObject {
            SpeciesLength.deleteAllExceptForIdList(data, inContext: context)
        }
        
        if let data = timeOfDayIds.arrayObject {
            TimeOfDayOption.deleteAllExceptForIdList(data, inContext: context)
        }
        
        if let data = fishConditionsIds.arrayObject {
            FishConditionOption.deleteAllExceptForIdList(data, inContext: context)
        }
        
        if let data = dispostionsIds.arrayObject {
            RecaptureConditionOption.deleteAllExceptForIdList(data, inContext: context)
        }
        
        if let data = shirtSizesIds.arrayObject {
            ShirtSize.deleteAllExceptForIdList(data, inContext: context)
        }
        
    }
    
    private func processSyncUserDataIdsData(json: JSON, inContext context: NSManagedObjectContext) {
        let tagIds          = json["tags"]["ids"]
        let captureIds      = json["captures"]["ids"]
        let draftCaptureIds = json["draft_captures"]["ids"]
        
        if let data = tagIds.arrayObject {
            Tag.deleteAllExceptForIdList(data, inContext: context)
        }
        
        if let data = captureIds.arrayObject {
            Capture.deleteAllExceptForIdList(data, inContext: context)
        }
        
        if let data = draftCaptureIds.arrayObject {
            DraftCapture.deleteAllExceptForIdList(data, inContext: context)
        }
    }
    
    private func processPostData(json: JSON, inContext context: NSManagedObjectContext) {
        processPostDataHelper(EnteredDraftCapture.self, json: json, key: "draft_captures", context: context)
        processPostDataHelper(EnteredUserTagRequest.self, json: json, key: "tag_requests", context: context)
        processPostDataHelper(EnteredTimeLogEntry.self, json: json, key: "time_logs", context: context)
    }
    
    private func processPostDataHelper(classItem: RemoteItem.Type, json: JSON, key: String, context: NSManagedObjectContext) {
        
        func processErrors(classItem: RemoteItem.Type, json: JSON, key: String, context: NSManagedObjectContext) {
            if let errors = json[key]["errors"].arrayObject {
                for item in errors {
                    if let exceptionValue = item["exception"] as? String, uuid = item["uuid"] as? String {
                        if exceptionValue.lowercaseString.contains("uuid") {
                            let predicate = NSPredicate(format: "uuid == %@", argumentArray: [uuid])
                            classItem.deleteAllMatchingPredicateWithoutSubentities(predicate, inContext: context)
                        }
                    }
                }
            }
        }
        
        
        sleep(40)
        
        if let status = json[key]["status"].int {
            switch (status) {
            case 200:
                if let uuids = json[key]["uuids"].arrayObject {
                    let predicate = NSPredicate(format: "uuid IN %@", argumentArray: [uuids])
                    classItem.deleteAllMatchingPredicateWithoutSubentities(predicate, inContext: context)
                }
                
                processErrors(classItem, json: json, key: key, context: context)
            case 422:
                processErrors(classItem, json: json, key: key, context: context)
            default:
                ()
            }
        }
        
    }
    
    /*
     PARAMETER HELPERS
     */
    
    private func publicFetchParams() -> [String: AnyObject] {
        // the parameters dicitonay to send to the server
        var params = [String: AnyObject]()
        
        /*
         We will try to get the max verison of each of the datasets
         returned by the combined data request
         */
        
        if let version = Species.getMaxVersion() as NSDate? {
            params["species"] = ["updated_after" : version.iso8601]
        }
        
        if let version = SpeciesLength.getMaxVersion() as NSDate? {
            params["species_lengths"] = ["updated_after": version.iso8601]
        }
        
        if let version = FishConditionOption.getMaxVersion() as NSDate? {
            params["fish_conditions"] = ["updated_after": version.iso8601]
        }
        
        if let version = TimeOfDayOption.getMaxVersion() as NSDate? {
            params["time_of_days"] = ["updated_after": version.iso8601]
        }
        
        if let version = RecaptureConditionOption.getMaxVersion() as NSDate? {
            params["dispositions"] = ["updated_after": version.iso8601]
        }
        
        if let version = ShirtSize.getMaxVersion() as NSDate? {
            params["shirt_sizes"] = ["updated_after": version.iso8601]
        }
        
        return params
    }
    
    private func protectedFetchParams() -> [String: AnyObject] {
        // the parameters dicitonay to send to the server
        var params = [String: AnyObject]()
        
        /*
         We will try to get the max verison of each of the datasets
         returned by the combined data request
         */
        
        if let version = Capture.getMaxVersion() as NSDate? {
            params["captures"] = ["updated_after" : version.iso8601]
        }
        
        if let version = DraftCapture.getMaxVersion() as NSDate? {
            params["draft_captures"] = ["updated_after": version.iso8601]
        }
        
        if let version = Tag.getMaxVersion() as NSDate? {
            params["tags"] = ["updated_after": version.iso8601]
        }
        
        if let version = UserTagRequest.getMaxVersion() as NSDate? {
            params["tag_requests"] = ["updated_after": version.iso8601]
        }
        
        if let version = TimeLogEntry.getMaxVersion() as NSDate? {
            params["time_logs"] = ["updated_after": version.iso8601]
        }
        
        return params
    }
    
    private func syncPublicIdsParams() -> [String: AnyObject] {
        // the parameters dicitonay to send to the server
        var params = [String: AnyObject]()
        
        params["species"]           = ["only_id_list" : "true"]
        params["species_lengths"]   = ["only_id_list" : "true"]
        params["fish_conditions"]   = ["only_id_list" : "true"]
        params["time_of_days"]      = ["only_id_list" : "true"]
        params["dispositions"]      = ["only_id_list" : "true"]
        params["shirt_sizes"]       = ["only_id_list" : "true"]
        
        return params
    }
    
    private func syncProtectedIdsParams() -> [String: AnyObject] {
        // the parameters dicitonay to send to the server
        var params = [String: AnyObject]()
        
        params["captures"]          = ["only_id_list" : "true"]
        params["draft_captures"]    = ["only_id_list" : "true"]
        params["tags"]              = ["only_id_list" : "true"]
        
        return params
    }
    
    private func postParams() -> [String: AnyObject] {
        var params = [String: AnyObject]()
        
        if (EnteredDraftCapture.MR_countOfEntities() > 0) {
            
            // TODO remove this in future releases.
            // used to massage some bad tag entries so they can be accepted by the server.
            let items = (EnteredDraftCapture.MR_findAll() as! [EnteredDraftCapture]).map({ item -> [String:AnyObject] in
                var m = item.toDictionary() // move the `let item` to a var for modification
                
                // remove any non alphanumeric characters and upper case the string.
                m["tag_number"] = m["tag_number"]!.substituteAll("[^A-Za-z0-9]", with: "").uppercaseString
                
                // get the list of images for the item, and then create a subhash out of the data
                m["images"] = item.attachedImages.map({ imageItem -> [String:AnyObject] in
                    var imageItemHash = [String: AnyObject]()
                    
                    let uiImage:UIImage = UIImage(data: (imageItem as EnteredDraftCapturePhoto).imageData)!
                    let imageData:NSData = UIImageJPEGRepresentation(uiImage, 1.0)!
                    let strBase64:String = imageData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
                    
                    imageItemHash["raw_data"] = strBase64
                    return imageItemHash
                })
                
                return m
            })
            
            params["draft_captures"] = ["items" : items]
        }
        
        if (EnteredUserTagRequest.MR_countOfEntities() > 0) {
            params["tag_requests"]   = ["items" : EnteredUserTagRequest.allAsDictionary() as [[String: AnyObject]]]
        }
        
        if (EnteredTimeLogEntry.MR_countOfEntities() > 0) {
            params["time_logs"]      = ["items" : EnteredTimeLogEntry.allAsDictionary() as [[String: AnyObject]]]
        }
        
        return params
    }
    
}
