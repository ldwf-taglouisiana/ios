//
//  AuthUtils.swift
//  taglouisiana
//
//  Created by Daniel Ward on 3/23/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import Foundation
import NXOAuth2Client

class AuthUtils {

    /**
        Get the OAuth token for the signed in user.

        - returns: If the uesr is signed in, will return the OAuth token, else returns nil
    */
    class func getOauthToken() -> String? {
        let oauthStore = NXOAuth2AccountStore.sharedStore() as! NXOAuth2AccountStore
        let accounts = oauthStore.accountsWithAccountType(Constants.AOUTH_ACCOUNT)
        
        if accounts.count > 0 {
            return (accounts[0].accessToken as NXOAuth2AccessToken).accessToken
        }
        
        return nil
    }

    /**
        Checks to see if a user is signed in.

        - Returns: Whether a user is signed in
    */
    class func isSignedIn() -> Bool {
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        let signedIn = defaults.boolForKey(Constants.DEFAULTS_KEY_SIGN_IN_SET)
        
        let oauthStore = NXOAuth2AccountStore.sharedStore() as! NXOAuth2AccountStore
        let accounts = oauthStore.accountsWithAccountType(Constants.AOUTH_ACCOUNT) as! [NXOAuth2Account]
        
        if !signedIn {
            for account in accounts {
                oauthStore.removeAccount(account)
            }
        }
        
        return signedIn && accounts.count > 0
    }
    
    class func signOut() {
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        defaults.removeObjectForKey(Constants.DEFAULTS_KEY_SIGN_IN_SET)
        defaults.synchronize()
        
        let oauthStore = NXOAuth2AccountStore.sharedStore() as! NXOAuth2AccountStore
        let accounts = oauthStore.accountsWithAccountType(Constants.AOUTH_ACCOUNT) as! [NXOAuth2Account]
        for account in accounts {
            oauthStore.removeAccount(account)
        }
    }
    
    class func setLoginedInToken() {
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        defaults.setBool(true, forKey: Constants.DEFAULTS_KEY_SIGN_IN_SET)
        defaults.synchronize()
        
        Api.FetchUserData.execute()
    }
}