//
// Created by Daniel Ward on 3/23/15.
// Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import Foundation
import Alamofire
import NXOAuth2Client

enum Router {
    static let baseURLString = Constants.API_SERVER

    case PublicDashboard
    case AnglerDashboard
    case FetchUserData([String: AnyObject])
    case FetchPublicData([String: AnyObject])
    case SyncPublicIds([String: AnyObject])
    case SyncUserIds([String: AnyObject])
    case PostData([String: AnyObject])
    case CreateDeviceToken([String: AnyObject])
    case TagHistory([String: AnyObject])
    
    var method: Alamofire.Method {
        switch self {
        case .PublicDashboard, .AnglerDashboard, .FetchPublicData, .FetchUserData, .SyncPublicIds, .SyncUserIds, .TagHistory:
            return .GET
        default:
            return .POST
        }
    }

    var path: String {
        switch self {
            
        case .PublicDashboard:
            return "/public/info/dashboard.json"
        
        case .AnglerDashboard:
            return "/protected/account/dashboard.json"
        
        case .FetchPublicData, .SyncPublicIds:
            return "/public/options/combined.json"

        case .FetchUserData, .SyncUserIds:
            return "/protected/combined_data.json"
            
        case .PostData:
            return "/protected/combined_data.json"

        case .CreateDeviceToken:
            return "/protected/mobile/notifications/register/ios.json"
            
            
        case .TagHistory(let parameters):
            return "/protected/captures/\(parameters["id"]!)/history.json"

        }
    }

    var encoding: ParameterEncoding {
        switch self {
        case .PublicDashboard, .AnglerDashboard, .FetchPublicData, .FetchUserData, .SyncPublicIds, .SyncUserIds, .TagHistory:
            return ParameterEncoding.URLEncodedInURL
        default:
            return ParameterEncoding.JSON.gzipped
        }
    }

    // MARK: URLRequestConvertible

    var request: Request {
        let url = NSURL(string: Router.baseURLString)!.URLByAppendingPathComponent(path)!.absoluteString

        var headers = [String: String]()
        
        if let token = AuthUtils.getOauthToken() {
            headers["Authorization"] = "Bearer \(token)"
        }
       
        headers["Api-Access-Token"] = Utils.getApiToken()
        headers["User-Agent"] = NSUserDefaults.standardUserDefaults().stringForKey("UserAgent")

        var requestParameters = [String: AnyObject]()
        
        switch self {
        case .PublicDashboard, .AnglerDashboard:
            break // do nothing
        case .CreateDeviceToken(let parameters):
            requestParameters = parameters

        case .PostData(let parameters):
            requestParameters = parameters

        case .FetchUserData(let parameters):
            requestParameters = parameters

        case .FetchPublicData(let parameters):
            requestParameters = parameters
            
        case.SyncPublicIds(let parameters):
            requestParameters = parameters
            
        case.SyncUserIds(let parameters):
            requestParameters = parameters
            
        case.TagHistory(let parameters):
            requestParameters = parameters
        }

        return Alamofire.request(self.method, url!, parameters: requestParameters, encoding:  self.encoding, headers: headers)
    }
}
