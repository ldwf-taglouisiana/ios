//
//  FishEntryPhoto+CoreDataProperties.swift
//  taglouisiana
//
//  Created by Daniel Ward on 8/1/16.
//  Copyright © 2016 Daniel Ward. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension FishEntryPhoto {

    @NSManaged var fullUrl: String
    @NSManaged var thumbUrl: String
    @NSManaged var mediumUrl: String
    @NSManaged var fishEvent: FishEvent?

}
