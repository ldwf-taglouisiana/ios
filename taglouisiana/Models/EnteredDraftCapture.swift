//
//  EnteredDraftCapture.swift
//  
//
//  Created by Daniel Ward on 10/30/15.
//
//

import Foundation
import CoreData
import MWPhotoBrowser


class EnteredDraftCapture: DraftCapture {

    override func imagesForGallery() -> [MWPhoto] {
        return self.attachedImages.map{ item -> MWPhoto in
            let uiImage:UIImage = UIImage(data: (item as EnteredDraftCapturePhoto).imageData)!
            return MWPhoto(image: uiImage)
        }
    }

}
