//
//  DraftCapture+CoreDataProperties.swift
//  
//
//  Created by Daniel Ward on 10/30/15.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension DraftCapture {

    @NSManaged var errorJson: String
    @NSManaged var recapture: NSNumber
    @NSManaged var savedAt: NSDate
    @NSManaged var expiresAt: NSDate?
    @NSManaged var shouldDelete: NSNumber
    @NSManaged var shouldSave: NSNumber
    @NSManaged var uuid: String
    @NSManaged var dispostion: RecaptureConditionOption
    @NSManaged var status: DraftCaptureStatus?

}
