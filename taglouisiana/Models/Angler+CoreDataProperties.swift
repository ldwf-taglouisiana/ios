//
//  Angler+CoreDataProperties.swift
//  
//
//  Created by Daniel Ward on 10/30/15.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Angler {

    @NSManaged var city: String?
    @NSManaged var email: String?
    @NSManaged var firstName: String?
    @NSManaged var lastName: String?
    @NSManaged var phoneNumber: String?
    @NSManaged var state: String?
    @NSManaged var street: String?
    @NSManaged var suite: String?
    @NSManaged var zipcode: String?
    @NSManaged var shirtSize: ShirtSize?

}
