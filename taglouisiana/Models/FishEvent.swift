//
//  FishEvent.swift
//  
//
//  Created by Daniel Ward on 10/30/15.
//
//

import Foundation
import CoreData
import MWPhotoBrowser


class FishEvent: RemoteItem {
    
    func imagesForGallery() -> [MWPhoto] {
        return self.images.map{ item -> MWPhoto in
            return MWPhoto(URL: NSURL(string: "\(Constants.SERVER_ADDRESS)/\(item.fullUrl)"))
        }
    }
    
    override func didImport(data: AnyObject!) {
        
        // if the data isn't a dictionary then return
        if (!(data is NSDictionary)) {
            return
        }
        
        // convert the type of the  dictionary to a NSDictionary, instead of AnyObject
        let dictionaryData = data as! NSDictionary
       
        if let remoteImages = dictionaryData["images"] as? [AnyObject] {
            
            // get the list of image urls form the data return from the api
            let fullUrls = remoteImages.map({ item -> String in
                item["full"] as! String
            })

            // delete any old images that may have been removed server side
            for imageItem in self.images {
                if !(fullUrls.contains(imageItem.fullUrl)) {
                    self.managedObjectContext?.deleteObject(imageItem)
                }
            }
        
            // create any images as needed for this FishEntry
            for item in remoteImages {
                if let fullUrl = item["full"] as? String, mediumUrl = item["medium"] as? String, thumbUrl = item["thumb"] as? String {
                    let entry = FishEntryPhoto.MR_findFirstOrCreateByAttribute("fullUrl", withValue: fullUrl, inContext: self.managedObjectContext!) as FishEntryPhoto
                    
                    entry.mediumUrl = mediumUrl
                    entry.thumbUrl = thumbUrl

                    entry.fishEvent = self
                }
            }
        } // end if let remoteImages
        
    }
}
