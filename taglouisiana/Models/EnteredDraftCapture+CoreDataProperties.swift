//
//  EnteredDraftCapture+CoreDataProperties.swift
//  taglouisiana
//
//  Created by Daniel Ward on 8/2/16.
//  Copyright © 2016 Daniel Ward. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension EnteredDraftCapture {

    @NSManaged var attachedImages: Set<EnteredDraftCapturePhoto>

}
