//
// Created by Daniel Ward on 3/24/15.
// Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import Foundation
import MagicalRecord
import SwiftyJSON
import XLForm
import Fabric
import Crashlytics
import CryptoSwift

// MARK - DraftCapture

extension DraftCapture {
    
    var statusEnum: DraftCaptureStatusEnum {
        if self.errorMessageString().length > 0 {
            return DraftCaptureStatusEnum.Error
        } else {
            return DraftCaptureStatusEnum.Ok
        }
    }

    // test written
    func convertToEnteredDraftCapture() -> EnteredDraftCapture {
        let result = EnteredDraftCapture.MR_createEntityInContext(self.managedObjectContext!)
        let attributes = self.entity.attributesByName 
        let relationships = self.entity.relationshipsByName 
        
        for (attribute, _) in attributes {
            result.setValue(self.valueForKey(attribute), forKey: attribute)
        }
        
        for (name, _) in relationships {
            result.setValue(self.valueForKey(name), forKey: name)
        }
        
        return result
    }
    
    func errorMessageString() -> String {
        var result = ""
        
        if let dataFromString = self.errorJson.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false) {
            let json = JSON(data: dataFromString)
            
            if let array = json["errors"].arrayObject {
                let items = array.map {
                    (input) -> String in
                    return "• \(input)"
                }
                
                result = items.joinWithSeparator("\n")
            }
        }
        
        return result
    }
    
    // test written
    class func findAllWhereNotDelted() -> [DraftCapture]! {
        let request = self.MR_requestAllSortedBy("date", ascending: false, withPredicate: NSPredicate(format: "shouldDelete == NO", argumentArray: [])) as NSFetchRequest
        request.includesSubentities = false
        return self.MR_executeFetchRequest(request) as! [DraftCapture]
    }
}

// MARK - FishEvent

extension FishEvent {
    
    // test written
    func lengthText() -> String {
        if let value = self.length as NSNumber? {
            return "Length: \(value.floatValue) inches"
        }
        
        if let value = self.speciesLength as SpeciesLength? {
            return "Length: \(value.detail) inches"
        }
        
        return "No Length"
        
    }
    
    // test written
    func location() -> FishEventLocation? {
        
        if let lat = self.latitude, let lon = self.longitude {
            let latCoordinate = Coordinate(decimal: lat.doubleValue)
            let lonCoordinate = Coordinate(decimal: lon.doubleValue)
        
            return FishEventLocation(latitude: latCoordinate, longitude: lonCoordinate, type: self.gpsTypeEnum())
        } else {
            return nil
        }
    }
    
    // test written
    func gpsTypeEnum() -> GpsType {
        return GpsType(rawValue: self.gpsType)!
    }
}

// MARK - Species

extension Species: XLFormOptionObject {
    func formDisplayText() -> String {
        return self.commonName
    }
    
    func formValue() -> AnyObject {
        return self
    }
}

// MARK - SpeciesLength

extension SpeciesLength: XLFormOptionObject {
    func formDisplayText() -> String {
        return self.detail
    }
    
    func formValue() -> AnyObject {
        return self
    }
}

// MARK - Tag

extension Tag {
    class func availableTags(context: NSManagedObjectContext?, excludedTags: [String]?) -> [Tag] {
        let context = (context == nil) ? NSManagedObjectContext.defaultContext() : context
        
        let usedDraftTags = (DraftCapture.MR_findAllInContext(context!) as NSArray).valueForKey("tagNumber") as! [String]
        let enteredDraftTags = (EnteredDraftCapture.MR_findAllInContext(context!) as NSArray).valueForKey("tagNumber") as! [String]
        var usedTags = enteredDraftTags + usedDraftTags
        
        if let otherTags = excludedTags {
            usedTags += otherTags
        }
        
        let predicate: NSPredicate = NSPredicate(format: "used == NO AND NOT (number IN %@)", argumentArray: [usedTags])
        
        return self.MR_findAllSortedBy("number", ascending: true, withPredicate: predicate, inContext: context!) as! [Tag]
    }
    
    class func availableTagsWithSearchString(context: NSManagedObjectContext?, excludedTags: [String]?, searchString: String!) -> [Tag] {
        let context = (context == nil) ? NSManagedObjectContext.defaultContext() : context
        
        let usedDraftTags = (DraftCapture.MR_findAllInContext(context!) as NSArray).valueForKey("tagNumber") as! [String]
        let enteredDraftTags = (EnteredDraftCapture.MR_findAllInContext(context!) as NSArray).valueForKey("tagNumber") as! [String]
        var usedTags = enteredDraftTags + usedDraftTags
        
        if let otherTags = excludedTags {
            usedTags += otherTags
        }
        
        let predicate: NSPredicate = NSPredicate(format: "used == NO AND NOT (number IN %@) AND number contains[c] %@", argumentArray: [usedTags, searchString])
        
        return self.MR_findAllSortedBy("number", ascending: true, withPredicate: predicate, inContext: context!) as! [Tag]
    }
}

// MARK - TimeOfDayOption

extension TimeOfDayOption: XLFormOptionObject {
    
    // test not written, is tested with the other method
    class func current(context: NSManagedObjectContext?) -> TimeOfDayOption {
        return currentTimeForDate(NSDate(), context: context)
    }
    
    // test written, but cannot run with the current Swift testing setup
    class func currentTimeForDate(date: NSDate, context: NSManagedObjectContext?) -> TimeOfDayOption {

        let context = (context == nil) ? NSManagedObjectContext.defaultContext() : context
        let options = TimeOfDayOption.MR_findAllInContext(context!) as! [TimeOfDayOption]
        
        // build the formatter for the 12 hour time
        let formatter: NSDateFormatter = NSDateFormatter()
        formatter.timeStyle = .ShortStyle
        formatter.timeZone = NSTimeZone.systemTimeZone()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") // this is needed in case a user has 24 hour time enabled
        
        // get current time
        let currentDate = date
        
        // parse the option strings into the date components
        for option in options {
            let matches: [String] = option.detail.matchesForRegex("(\\d{1,2}:\\d{0,2}\\ [A-Z]{2}){1}.+?(\\d{1,2}:\\d{0,2}\\ [A-Z]{2}){1}")
        
            // ensure there are 2 returned matches from the regex
            if matches.count == 2 {
                
                // check that both matches are parsed into date objects
                if let startDate = formatter.dateFromString(matches.first!), endDate = formatter.dateFromString(matches.last!) {
                    let lowerBound = NSDate.dateFromSettingTimeFrom(startDate)
                    let upperBound = NSDate.dateFromSettingTimeFrom(endDate)
                    
                    if currentDate.isBetween(lowerBound, secondDate: upperBound){
                        return option
                    }
                }
            }
        }
        
        return self.MR_findFirstOrderedByAttribute("remoteId", ascending: true) as TimeOfDayOption
    }
    
    // MARK: - XLFormOptionObject
    
    func formDisplayText() -> String {
        let regex = try? NSRegularExpression(pattern: "\\s\\(.*$", options: NSRegularExpressionOptions.CaseInsensitive)
        return (regex?.stringByReplacingMatchesInString(self.detail, options: NSMatchingOptions.ReportCompletion, range: NSMakeRange(0, self.detail.characters.count), withTemplate: ""))!
    }
    
    func formValue() -> AnyObject {
        return self
    }
}

// MARK - FishConditionOption

extension FishConditionOption: XLFormOptionObject {
    func formDisplayText() -> String {
        var item = "\(self.remoteId.integerValue)"
        
        if self.remoteId.integerValue == 1 || self.remoteId.integerValue == 4 {
            let quantifier = self.remoteId.integerValue == 1 ? "Good" : "Bad"
            item = "\(item) - \(quantifier)"
        }

        return item
    }
    
    func formValue() -> AnyObject {
        return self
    }
}

// MARK - FishConditionOption

extension RecaptureConditionOption: XLFormOptionObject {
    func formDisplayText() -> String {
        return "\(self.remoteId.integerValue) - \(self.detail)"
    }
    
    func formValue() -> AnyObject {
        return self
    }
}

// MARK - RemoteItem

extension RemoteItem {

    class func deleteAllMatchingPredicateWithoutSubentities(predicate: NSPredicate, inContext: NSManagedObjectContext) {
        let request = self.MR_requestAllWithPredicate(predicate) as NSFetchRequest
        request.includesSubentities = false
        let items = self.MR_executeFetchRequest(request) as! [RemoteItem]
        for item in items {
            item.MR_deleteEntityInContext(inContext)
        }
    }
    
    // test written, was wriiten for EnteredDraftCapture
    func toDictionary() -> [String: AnyObject] {
        var result = [String: AnyObject]()
        let attributes = self.entity.attributesByName 
        let relationships = self.entity.relationshipsByName 
        
        for (attributeName, attributeDescription) in attributes {
            let userInfo = attributeDescription.userInfo as! [String: AnyObject]
            
            if let key = userInfo["mappedKeyName"] as? String, instanceValue = self.valueForKey(attributeName) {
        
                if attributeDescription.attributeType == NSAttributeType.DateAttributeType, let date = instanceValue as? NSDate {
                    result[key] = date.iso8601
                }
                else if attributeDescription.attributeType == NSAttributeType.BooleanAttributeType, let number = instanceValue as? NSNumber {
                    result[key] = (number.intValue == 1) ? true : false
                }
                else {
                    result[key] = instanceValue
                }
            }
            
        }
        
        for (name, value) in relationships {
            let userInfo = value.userInfo as! [String: AnyObject]
            
            if let key = userInfo["mappedKeyName"] as? String {
                if let _ = userInfo["ignoreInDictionary"] {
                    // if the ignore key is present then we can ignore the it
                } else {
                    let relatedAttribute = userInfo["relatedByAttribute"] as! String
                    let value: AnyObject? = self.valueForKey(name)?.valueForKey(relatedAttribute)
                    result[key] = value
                }
            }
        }
        
        return result
    }
    
    // no test written, builds on toDictionary()
    class func allAsDictionary() -> [[String: AnyObject]] {
        var result = [[String: AnyObject]]()
        
        let items = self.MR_findAllSortedBy("uuid", ascending: true) as! [RemoteItem]
        
        for item in items {
            result.append(item.toDictionary())
        }
        
        return result
    }
    
    class func allRemoteIds() -> [Int] {
        return self.allRemoteIds(NSManagedObjectContext.defaultContext())
    }
    
    class func allRemoteIds(inContext: NSManagedObjectContext) -> [Int] {
        let request = self.MR_requestAllSortedBy("remoteId", ascending: true) as NSFetchRequest
        request.includesSubentities = false
        let items = self.MR_executeFetchRequest(request, inContext: inContext) as! [RemoteItem]
        
        return items.map({ (remoteItem) -> Int in
            remoteItem.valueForKey("remoteId") as! Int
        })
    }
    
    class func remoteIdsHash() -> String {
        return self.remoteIdsHash(NSManagedObjectContext.defaultContext())
    }
    
    class func remoteIdsHash(inContext: NSManagedObjectContext) -> String {
        return self.allRemoteIds(inContext).map({ (item) -> String in
            String(item)
        }).joinWithSeparator("").md5()
    }
    
    // test written
    class func entityName() -> String {
        let name = NSStringFromClass(self) // gets the runtime class name => taglouisiana.CLASS_NAME
        let parts = name.characters.split {$0 == "."}.map { String($0) }  // need to split out the first portion, since it is the package name
        return parts[1] // the second part is the internal class name
    }
    
    class func entityForName() -> String {
        return self.entityName()
    }
    
    // test written
    class func getMaxVersion() -> NSDate? {
        // get the item with the largest version string
        let request = self.MR_requestAllSortedBy("updatedAt", ascending: false)
        request.includesSubentities = false
        
        let t = self.MR_executeFetchRequestAndReturnFirstObject(request) as? RemoteItem
        
        return t?.updatedAt
    }
    
    class func deleteAllExceptForIdList(ids: [AnyObject], inContext: NSManagedObjectContext) {
        if ids.count > 0 {
            let predicate = NSPredicate(format: "NOT (remoteId IN %@)", argumentArray: [ids])
            self.deleteAllMatchingPredicateWithoutSubentities(predicate, inContext: inContext)
        } else {
            let predicate = NSPredicate(format: "YES = YES")
            self.deleteAllMatchingPredicateWithoutSubentities(predicate, inContext: inContext)
        }
    }
}
