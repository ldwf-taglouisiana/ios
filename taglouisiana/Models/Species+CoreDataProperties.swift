//
//  Species+CoreDataProperties.swift
//  
//
//  Created by Daniel Ward on 10/30/15.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Species {

    @NSManaged var commonName: String
    @NSManaged var family: String?
    @NSManaged var foodValue: String?
    @NSManaged var habitat: String?
    @NSManaged var info: String?
    @NSManaged var otherName: String?
    @NSManaged var photoUrl: String
    @NSManaged var position: NSNumber?
    @NSManaged var properName: String?
    @NSManaged var publish: NSNumber?
    @NSManaged var size: String?
    @NSManaged var target: NSNumber
    @NSManaged var fishEvents: NSSet
    @NSManaged var speciesLengths: NSOrderedSet

}
