//
//  SpeciesLength+CoreDataProperties.swift
//  
//
//  Created by Daniel Ward on 10/30/15.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension SpeciesLength {

    @NSManaged var detail: String
    @NSManaged var position: NSNumber
    @NSManaged var fishEvents: NSSet?
    @NSManaged var species: Species

}
