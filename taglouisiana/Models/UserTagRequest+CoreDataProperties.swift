//
//  UserTagRequest+CoreDataProperties.swift
//  
//
//  Created by Daniel Ward on 10/30/15.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension UserTagRequest {

    @NSManaged var fullfilled: NSNumber
    @NSManaged var quantity: NSNumber
    @NSManaged var tagType: String
    @NSManaged var status: UserTagRequestStatus?

}
