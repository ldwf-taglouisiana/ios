//
//  Tag+CoreDataProperties.swift
//  
//
//  Created by Daniel Ward on 10/30/15.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Tag {

    @NSManaged var number: String
    @NSManaged var used: NSNumber
    @NSManaged var active: NSNumber

}
