//
//  FishEvent+CoreDataProperties.swift
//  taglouisiana
//
//  Created by Daniel Ward on 8/1/16.
//  Copyright © 2016 Daniel Ward. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension FishEvent {

    @NSManaged var comments: String?
    @NSManaged var date: NSDate
    @NSManaged var gpsType: String
    @NSManaged var latitude: NSNumber?
    @NSManaged var lattitudeFormatted: String
    @NSManaged var length: NSNumber?
    @NSManaged var locationDescription: String?
    @NSManaged var longitude: NSNumber?
    @NSManaged var longitudeFormatted: String?
    @NSManaged var tagNumber: String
    @NSManaged var fishCondition: FishConditionOption?
    @NSManaged var species: Species
    @NSManaged var speciesLength: SpeciesLength?
    @NSManaged var timeOfDay: TimeOfDayOption
    @NSManaged var images: Set<FishEntryPhoto>

}
