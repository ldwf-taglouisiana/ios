//
//  SpeciesTableViewController.swift
//  taglouisiana
//
//  Created by Daniel Ward on 3/27/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import UIKit
import MagicalRecord

class SpeciesTableViewController: UITableViewController {

    var detailViewController: SpeciesDetailViewController? = nil
    
    var species : [Species]!
    var targetSpecies : [Species]!
    var dateFormatter = NSDateFormatter()
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            self.clearsSelectionOnViewWillAppear = false
            self.preferredContentSize = CGSize(width: 320.0, height: 551.0)
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        if let split = self.splitViewController {
            split.presentsWithGesture = false
            let navController = split.viewControllers[split.viewControllers.count - 1] as? UINavigationController
            
            if let controller = navController {
                self.detailViewController = controller.topViewController as? SpeciesDetailViewController
            }
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SpeciesTableViewController.forceSyncStarted(_:)), name: Constants.NOTIFICATION_FORCE_SYNC_STARTED, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SpeciesTableViewController.forceSyncFinished(_:)), name: Constants.NOTIFICATION_FORCE_SYNC_FINISHED, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SpeciesTableViewController.managedObjectContextDidChange(_:)), name: NSManagedObjectContextDidSaveNotification, object: NSManagedObjectContext.defaultContext())
        
       refreshData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: Constants.NOTIFICATION_FORCE_SYNC_STARTED, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: Constants.NOTIFICATION_FORCE_SYNC_FINISHED, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: NSManagedObjectContextObjectsDidChangeNotification, object: NSManagedObjectContext.defaultContext())
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 2
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        switch section{
        case 0:
            return targetSpecies.count
        default:
            return species.count
        }
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Target Species"
        case 1:
            return "Common Species"
        default:
            return ""
        }
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) 

        let item = itemForIndexPath(indexPath)
        
        if let label = cell.textLabel {
            label.text = item.commonName
        }
        
        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        if segue.identifier == Constants.SEGUE_SHOW_SPECIES_DETAIL {
            let navigationController = segue.destinationViewController as! UINavigationController
            let controller = navigationController.topViewController as! SpeciesDetailViewController
            if let indexPath = self.tableView.indexPathForSelectedRow {
                controller.detailItem = itemForIndexPath(indexPath)
            }
            
            self.splitViewController?.toggleMasterView()
            
            // add the left nav item so we can show the list again
            controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
            controller.navigationItem.leftItemsSupplementBackButton = true
        }
    }

    // MARK: - Helpers
    
    private func itemForIndexPath(indexPath: NSIndexPath) -> Species {
        switch indexPath.section{
        case 0:
            return targetSpecies[indexPath.row]
        default:
            return species[indexPath.row]
        }
    }
    
    private func refreshData() {
        // get the captures list
        let commonSpeciesPredicate = NSPredicate(format: "publish = YES AND target = NO")
        self.species = Species.MR_findAllSortedBy("position", ascending: true, withPredicate: commonSpeciesPredicate) as! [Species]
        
        let targetSpeciesPredicate = NSPredicate(format: "publish = YES AND target = YES")
        self.targetSpecies = Species.MR_findAllSortedBy("position", ascending: true, withPredicate: targetSpeciesPredicate) as! [Species]

        dispatch_async(dispatch_get_main_queue(),{

            if self.detailViewController?.detailItem == nil && self.targetSpecies.count > 0 {
                self.detailViewController?.detailItem = self.targetSpecies[0]
            }

            self.tableView.reloadData()
            self.refreshControl?.endRefreshing()
        })
    }
    
    func managedObjectContextDidChange(sender: NSNotification) {
        self.refreshData()
    }
    
    func forceSyncStarted(sender: NSNotification) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: NSManagedObjectContextObjectsDidChangeNotification, object: nil)
    }
    
    func forceSyncFinished(sender: NSNotification) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: NSManagedObjectContextObjectsDidChangeNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SpeciesTableViewController.managedObjectContextDidChange(_:)), name: NSManagedObjectContextDidSaveNotification, object: NSManagedObjectContext.defaultContext())
        
        self.detailViewController?.detailItem = nil
        self.refreshData()
    }
}
