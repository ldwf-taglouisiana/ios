//
//  TimeOnWaterViewController.swift
//  taglouisiana
//
//  Created by Daniel Ward on 3/30/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import MagicalRecord

class TimeOnWaterViewController: UIViewController {

    // the tags that the view is using
    let START_TIME_BUTTON_TAG: Int = 2
    let END_TIME_BUTTON: Int = 3
    
    // the dateformatter to use
    let dateFormatter = NSDateFormatter()
    
    // the date selections
    var startDate: NSDate?
    var endDate: NSDate?
    
    @IBOutlet weak var startTimeButton: UIButton!
    @IBOutlet weak var endTimeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // initialize the date objects
        startDate = NSDate().roundToNearestHalfHour()
        endDate = NSDate(timeInterval:3600, sinceDate: startDate!)
        
        // set up the formatter for displaying the dates
        dateFormatter.dateStyle = .MediumStyle
        dateFormatter.timeStyle = .ShortStyle
        
        // update the buttons to show the date values
        startTimeButton.setTitle(dateFormatter.stringFromDate(startDate!), forState: .Normal)
        endTimeButton.setTitle(dateFormatter.stringFromDate(endDate!), forState: .Normal)
        
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            self.navigationController?.navigationBar.barTintColor = nil
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK - IBActions
    
    @IBAction func dismiss(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func submit(sender: UIBarButtonItem) {
        /*
            Save the entry.
            If successful, then sync data and dismiss view.
            Else show alert indicating there was an error saving the entity
        */
        MagicalRecord.saveWithBlock({ context in
            let entry = EnteredTimeLogEntry.MR_createEntityInContext(context) as EnteredTimeLogEntry
            entry.startDate = self.startDate!
            entry.endDate = self.endDate!
            
            entry.verified = true
            entry.updatedAt = NSDate()
            entry.uuid = NSUUID().UUIDString
        }, completion: { success, error in
            if success {
                Utils.syncData()
                self.dismissViewControllerAnimated(true, completion: nil)
            } else {
                // TODO prompt for error
            }
        })
    }
    
    @IBAction func buttedSelected(sender: UIButton) {
    
        // if the sender is the end time button, then we set the min time to now, else set to nil
        let min: () -> NSDate? = {
            let request = Capture.MR_requestAll()
            request.includesSubentities = false
            
            let captures = Capture.MR_executeFetchRequest(request) as! [Capture]
            
            /*
                If the angler has at least 10 captures, then we can trus them to enter a proper time entry
            */
            if sender.tag == self.START_TIME_BUTTON_TAG && captures.count >= 10 {
                return nil
            }
            
            return (sender.tag == self.START_TIME_BUTTON_TAG) ? NSDate() : self.startDate
        }
        
        // if the sender is not the end time button, then max date is now, else no max time
        let max: NSDate? = (sender.tag == START_TIME_BUTTON_TAG) ? NSDate() : nil
        
        // choose the selected date based on the sender's tag
        // TODO let date: NSDate = (sender.tag == START_TIME_BUTTON_TAG) ? startDate! : endDate!
        
        // create the picker
        let picker: ActionSheetDatePicker = ActionSheetDatePicker(title: "", datePickerMode: .DateAndTime, selectedDate: NSDate(), minimumDate: min(), maximumDate: max, target: self, action: #selector(TimeOnWaterViewController.dateChanged(_:element:)), cancelAction: nil, origin: sender)
        picker.minuteInterval = 30
        
        picker.showActionSheetPicker()
    }
    
    // MARK - Helpers
    
    func dateChanged(selectedTime: NSDate, element: UIButton) {
        element.setTitle(dateFormatter.stringFromDate(selectedTime), forState: .Normal)

        switch element.tag  {
        case START_TIME_BUTTON_TAG:
            startDate = selectedTime
        case END_TIME_BUTTON:
            endDate = selectedTime
        default:
            ()
        }
    }
}
