//
//  TagListTableViewController.swift
//  taglouisiana
//
//  Created by Daniel Ward on 4/8/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import UIKit
import XLForm
import MagicalRecord

class TagListTableViewController: UITableViewController, XLFormRowDescriptorViewController, UISearchResultsUpdating, UISearchControllerDelegate {

    let identifier = "Cell"
    
    var rowDescriptor: XLFormRowDescriptor?
    var context: NSManagedObjectContext = NSManagedObjectContext.defaultContext()
    
    var searchController: UISearchController?
    var searchResultsController: UITableViewController?
    
    var tags: [Tag] = [Tag]()
    var excludedTagNumbers = [String]()
    var filteredList = [Tag]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let row = rowDescriptor {
            for tagRow in row.sectionDescriptor.formRows {
                let descriptor = tagRow as! XLFormRowDescriptor
                
                if let value = descriptor.value as! String? {
                    excludedTagNumbers.append(value)
                }
            }
        }
   
        self.setupSearch()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TagListTableViewController.managedObjectContextDidChange(_:)), name: Constants.NOTIFICATION_DATA_UPDATE_FINISHED, object: nil)
        
       refreshData()
    }
    
    // MARK: - UITableViewDataSource

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return tableView == self.searchResultsController?.tableView ? filteredList.count : tags.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) 

        cell.textLabel?.text = getItemAtIndexPath(tableView, indexPath: indexPath).number

        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        rowDescriptor?.value = getItemAtIndexPath(tableView, indexPath: indexPath).number
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    // MARK: - UISearchResultsUpdating
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        
        if self.searchController?.searchBar.text!.lengthOfBytesUsingEncoding(NSUTF32StringEncoding) > 0 {
        let searchString = searchController.searchBar.text
        filteredList = Tag.availableTagsWithSearchString(context, excludedTags: excludedTagNumbers, searchString: searchString)
        
        if filteredList.count == 0 {
            let tempTag = Tag.MR_createEntityInContext(NSManagedObjectContext.confinementContextWithParent(self.context))
            tempTag.number = searchString!
            filteredList = [tempTag]
        }
        
            self.searchResultsController?.tableView.reloadData()
        }
    }
    
    
    // MARK: - Helpers 
    
    func managedObjectContextDidChange(sender: NSNotification) {
        self.refreshData()
    }
    
    func refreshData() {
        tags = Tag.availableTags(context, excludedTags: excludedTagNumbers)
        self.tableView.reloadData()
    }
    
    private func getItemAtIndexPath(tableView: UITableView, indexPath: NSIndexPath) -> Tag {
        if tableView == self.searchResultsController?.tableView {
            return filteredList[indexPath.row]
        } else {
            return tags[indexPath.row]
        }
    }

    /**
        From https://github.com/Mozharovsky/iOS-Demos/blob/master/Search%20Mechanism/Swift/Search%20Mechanism/ViewController.swift
    */
    private func setupSearch() {
        // A table for search results and its controller.
        let resultsTableView = UITableView(frame: self.tableView.frame)
        self.searchResultsController = UITableViewController()
        self.searchResultsController?.tableView = resultsTableView
        self.searchResultsController?.tableView.dataSource = self
        self.searchResultsController?.tableView.delegate = self
        
        // Register cell class for the identifier.
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: identifier)
        self.searchResultsController?.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: identifier)
        
        self.searchController = UISearchController(searchResultsController: self.searchResultsController!)
        self.searchController?.searchResultsUpdater = self
        self.searchController?.delegate = self
        self.searchController?.searchBar.sizeToFit()
        self.tableView.tableHeaderView = self.searchController?.searchBar
        
        self.definesPresentationContext = true
    }
}
