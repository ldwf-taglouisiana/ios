    //
//  FormRow.swift
//  taglouisiana
//
//  Created by Daniel Ward on 4/2/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import Foundation
import MagicalRecord
import XLForm

enum FormRowTag: String {
    // standard rows
    case TagNumber = "tagNumber"
    case TagNumberSection = "tagNumberSection"
    case CaptureDate = "captureDate"
    case TimeOfDay = "timeOfDay"
    case Species = "species"
    case SpeciesLengthSwitch = "speciesLengthSwitch"
    case SpeciesLengthManual = "speciesLengthManual"
    case SpeciesLengthRange = "speciesLengthRange"
    case Condition = "condition"
    case Disposition = "disposition"
    case Location = "location"
    case Comments = "comments"
    
    // rows for entering location
    case GpsType = "gpsType"
    case LatitudeDegrees = "latitudeDegrees"
    case LatitudeDecimal = "latitudeDecimal"
    case LatitudeMinutes = "latitudeMinutes"
    case LatitudeSeconds = "latitudeSeconds"
    case LongitudeDegrees = "longitudeDegrees"
    case LongitudeDecimal = "longitudeDecimal"
    case LongitudeMinutes = "longitudeMinutes"
    case LongitudeSconds = "longitudeSeconds"
}

protocol FormRow {
    var tag:FormRowTag { get }
    var rowType:String { get }
    var title:String { get }
    var regex:String? { get }
    var errorMessage:String? { get }
    var validator: XLFormValidatorProtocol? { get }
    var row: XLFormRowDescriptor { get }
}

enum CaptureFormRow: FormRow {
    case TagNumber
    case TagNumberSection
    case CaptureDate
    case TimeOfDay(NSManagedObjectContext)
    case SpeciesOption(NSManagedObjectContext)
    case SpeciesLengthSwitch
    case SpeciesLengthManual
    case SpeciesLengthRange(NSManagedObjectContext, Species?)
    case Condition(NSManagedObjectContext)
    case Dispostion(NSManagedObjectContext)
    case Location
    case Comments
    
    var tag: FormRowTag {
        switch self {
        case .TagNumber:
            return FormRowTag.TagNumber
        case .TagNumberSection:
            return FormRowTag.TagNumberSection
        case .CaptureDate:
            return FormRowTag.CaptureDate
        case .TimeOfDay:
            return FormRowTag.TimeOfDay
        case .SpeciesOption:
            return FormRowTag.Species
        case .SpeciesLengthSwitch:
            return FormRowTag.SpeciesLengthSwitch
        case .SpeciesLengthManual:
            return FormRowTag.SpeciesLengthManual
        case .SpeciesLengthRange:
            return FormRowTag.SpeciesLengthRange
        case .Condition:
            return FormRowTag.Condition
        case .Dispostion:
            return FormRowTag.Disposition
        case .Location:
            return FormRowTag.Location
        case .Comments:
            return FormRowTag.Comments
        }
    }
    
    var rowType: String {
        switch self {
        case .TagNumber:
            return XLFormRowDescriptorTypeText
        case .SpeciesLengthManual:
            return XLFormRowDescriptorTypeDecimal
        case .CaptureDate:
            return XLFormRowDescriptorTypeDateInline
        case .SpeciesLengthSwitch:
            return XLFormRowDescriptorTypeBooleanSwitch
        case .Location:
            return XLFormRowDescriptorTypeSelectorPush
        case .TimeOfDay, .SpeciesOption, .SpeciesLengthRange, .Condition, .Dispostion:
            return XLFormRowDescriptorTypeSelectorPickerViewInline
        case .Comments:
            return XLFormRowDescriptorTypeTextView
        default:
            return XLFormRowDescriptorTypeName
        }
    }
    
    var title: String {
        switch self {
        case .TagNumber:
            return "Tag Number"
        case .TagNumberSection:
            return "Tag Numbers"
        case .CaptureDate:
            return "Capture Date"
        case .TimeOfDay:
            return "Time Of Day"
        case .SpeciesOption:
            return "Species"
        case .SpeciesLengthSwitch:
            return "Manual Length"
        case .SpeciesLengthManual:
            return "Actual Length (inches)"
        case .SpeciesLengthRange:
            return "Length Range"
        case .Condition:
            return "Condition"
        case .Dispostion:
            return "Dispostion"
        case .Location:
            return "Location"
        case .Comments:
            return "Comments"
        }

    }
    
    var regex: String? {
        switch self {
        case .TagNumber:
            return "^[A-Z]{1,2}[0-9]{6}$"
        default:
            return nil
        }
    }
    
    var errorMessage: String? {
        switch self {
        case .TagNumber:
            return "Tag Number format incorrect (ex. LW123456)"
        case .SpeciesLengthManual:
            return "Length must be greater than 0"
        default:
            return nil
        }
    }
    
    var validator: XLFormValidatorProtocol? {
        switch self {
        case .TagNumber:
            return RegexFormRowValidator(msg: self.errorMessage, regex: self.regex)
        case .SpeciesLengthManual:
            return NumberFormRowValidator(msg: self.errorMessage, options: [ NumberFormRowValidator.GREATER_THAN_VALUE : 0.0 ] )
        case .Location:
            return FishEventLocationValidator()
        default:
            return nil
        }
    }
    
    var section: XLFormSectionDescriptor {
        switch self {
        case .TagNumberSection:
            let sectionRow = XLFormSectionDescriptor.formSectionWithTitle(self.title, sectionOptions: XLFormSectionOptions.CanInsert, sectionInsertMode: XLFormSectionInsertMode.Button)
            let row = XLFormRowDescriptor(tag: nil, rowType: XLFormRowDescriptorTypeSelectorPush, title: "")
            row.action.viewControllerStoryboardId = Constants.STORYBOARD_ID_FORM_TAG_LIST
            
            if let validator = CaptureFormRow.TagNumber.validator {
                row.addValidator(validator)
            }
            
            sectionRow.addFormRow(row)
            return sectionRow
        default:
            return XLFormSectionDescriptor.formSection()
        }
    }
    
    var row: XLFormRowDescriptor {
        let row = XLFormRowDescriptor(tag: self.tag.rawValue, rowType: self.rowType, title: self.title)

        if let item = self.validator {
            row.addValidator(item)
        }
        
        switch self {
        case .TagNumber:
            row.cellConfig.setObject(NSTextAlignment.Right.rawValue, forKey: "textField.textAlignment")
            row.required = true
            
        case .CaptureDate:
            row.value = NSDate()
            row.cellConfigAtConfigure.setValue(NSDate(), forKey: "maximumDate")
            
        case .TimeOfDay(let context):
            row.selectorOptions = TimeOfDayOption.MR_findAllSortedBy("remoteId", ascending: true, inContext: context) as! [TimeOfDayOption]
            row.value = TimeOfDayOption.current(context)
            
        case .SpeciesOption(let context):
            // had to use the module prefix due to enum nameing collision
            row.selectorOptions = Species.MR_findAllSortedBy("target,position:YES", ascending: false, inContext: context) as! [Species]
            if let item = row.selectorOptions {
                if item.count > 0 {
                    row.value = item.first
                }
            }
            
        case .Location:
            row.action.viewControllerStoryboardId = Constants.STORYBOARD_ID_FORM_MAP
            row.value = FishEventLocation(latitude: Coordinate(decimal: 0.0), longitude: Coordinate(decimal: 0.0), type: GpsType.D)
            
        case .SpeciesLengthRange(let context, let species):
            let species = species == nil ? Species.MR_findFirstOrderedByAttribute("target,position:YES", ascending: false, inContext: context) as Species : species!
            let lengths  = species.speciesLengths
            row.selectorOptions = (lengths.sortedArrayUsingDescriptors([NSSortDescriptor(key: "position", ascending: true)])) as! [SpeciesLength]
            if let item = row.selectorOptions {
                if item.count > 0 {
                    row.value = item.first
                }
            }
            
        case .SpeciesLengthManual:
            row.cellConfig.setObject(NSTextAlignment.Right.rawValue, forKey: "textField.textAlignment")
            
        case .Condition(let context):
            row.selectorOptions = FishConditionOption.MR_findAllSortedBy("remoteId", ascending: true, inContext: context) as! [FishConditionOption]
            if let item = row.selectorOptions {
                if item.count > 0 {
                    row.value = item.first
                }
            }
            
        case .Dispostion(let context):
            row.selectorOptions = RecaptureConditionOption.MR_findAllSortedBy("remoteId", ascending: true, inContext: context) as! [RecaptureConditionOption]
            if let item = row.selectorOptions {
                if item.count > 0 {
                    row.value = item.first
                }
            }

            
        default:
            () // do nothing
        }
        
        return row
    }
}

enum LocationFormRow: FormRow {
    case GpsFormat
    case LatitudeDecimal
    case LatitudeDegrees(GpsType)
    case LatitudeMinutes(GpsType)
    case LatitudeSeconds(GpsType)
    case LongitudeDecimal
    case LongitudeDegrees(GpsType)
    case LongitudeMinutes(GpsType)
    case LongitudeSconds(GpsType)
    
    var tag: FormRowTag {
        switch self {
        case .GpsFormat:
            return FormRowTag.GpsType
        case .LatitudeDecimal:
            return FormRowTag.LatitudeDegrees
        case .LatitudeDegrees:
            return FormRowTag.LatitudeDecimal
        case .LatitudeMinutes:
            return FormRowTag.LatitudeMinutes
        case .LatitudeSeconds:
            return FormRowTag.LatitudeSeconds
        case .LongitudeDegrees:
            return FormRowTag.LongitudeDegrees
        case .LongitudeDecimal:
            return FormRowTag.LongitudeDecimal
        case .LongitudeMinutes:
            return FormRowTag.LongitudeMinutes
        case .LongitudeSconds:
            return FormRowTag.LongitudeSconds
        }
    }
    
    var rowType: String {
        switch self {
        case .GpsFormat:
            return XLFormRowDescriptorTypeSelectorPickerViewInline
        default:
            return XLFormRowDescriptorTypeDecimal
        }
    }
    
    var title: String {
        switch self {
        case .GpsFormat:
            return "Format"
        case .LatitudeDegrees, .LongitudeDegrees:
            return "Degrees"
        case .LatitudeDecimal, .LongitudeDecimal:
            return "Decimal"
        case .LatitudeMinutes, .LongitudeMinutes:
            return "Minutes"
        case .LatitudeSeconds, .LongitudeSconds:
            return "Seconds"
        }
    }
    
    var regex: String? {
        return nil
    }
    
    var numberValidatorOptions: [String : AnyObject]? {
        var options = [String : AnyObject]()
        
        switch self {
        case .LatitudeSeconds, .LongitudeSconds:
            options[NumberFormRowValidator.GREATER_THAN_EQUAL_VALUE] = 0.0
            options[NumberFormRowValidator.LESS_THAN_VALUE] = 60.0
            
        // yeilds a regex for a range of 10.99999999 -> 50.99999999
        case .LatitudeDecimal:
            options[NumberFormRowValidator.GREATER_THAN_EQUAL_VALUE] = 10.0
            options[NumberFormRowValidator.LESS_THAN_VALUE] = 51.0
            
        // range of 10 -> 59
        case .LatitudeDegrees:
            options[NumberFormRowValidator.GREATER_THAN_EQUAL_VALUE] = 10.0
            options[NumberFormRowValidator.LESS_THAN_VALUE] = 59
            options[NumberFormRowValidator.WHOLE_NUMBER_ONLY] = true

        // range of 0.99999999 -> 59.99999999
        case .LatitudeMinutes(let gpsType):
            switch gpsType {
            case GpsType.DM:
                options[NumberFormRowValidator.GREATER_THAN_EQUAL_VALUE] = 0.0
                options[NumberFormRowValidator.LESS_THAN_VALUE] = 60.0

            case GpsType.DMS:
                options[NumberFormRowValidator.GREATER_THAN_VALUE] = 0.0
                options[NumberFormRowValidator.LESS_THAN_VALUE] = 60.0
                options[NumberFormRowValidator.WHOLE_NUMBER_ONLY] = true

            default:
                return nil
            }
            
        // yeilds a regex for a range of -135.99999999 -> -60.99999999
        case .LongitudeDecimal:
            options[NumberFormRowValidator.GREATER_THAN_VALUE] = -136.0
            options[NumberFormRowValidator.LESS_THAN_VALUE] = -60.0
            
        // yeilds a regex for a range of -135 -> -60
        case .LongitudeDegrees:
            options[NumberFormRowValidator.GREATER_THAN_VALUE] = -135.0
            options[NumberFormRowValidator.LESS_THAN_VALUE] = -60.0
            options[NumberFormRowValidator.WHOLE_NUMBER_ONLY] = true
            
        // yeilds a regex for a range of 0 -> 59.99999999
        case .LongitudeMinutes(let gpsType):
            switch gpsType {
            case GpsType.DM:
                options[NumberFormRowValidator.GREATER_THAN_EQUAL_VALUE] = 0.0
                options[NumberFormRowValidator.LESS_THAN_VALUE] = 60.0
                
            case GpsType.DMS:
                options[NumberFormRowValidator.GREATER_THAN_EQUAL_VALUE] = 0.0
                options[NumberFormRowValidator.LESS_THAN_VALUE] = 60.0
                options[NumberFormRowValidator.WHOLE_NUMBER_ONLY] = true
            default:
                return nil
            }
        default:
           return nil
        }
        
        return options
    }
    
    var errorMessage: String? {
        switch self {
        case .LatitudeSeconds:
            return "Latitude seconds must be between 0 and 59.99"
            
        case .LongitudeSconds:
            return "Longitude seconds must be between 0 and 59.99"
            
            // yeilds a regex for a range of 10.99 -> 50.99
        case .LatitudeDecimal:
            return "Latitude decimal must be between 10 and 59.99"
            
            // yeilds a regex for a range of 10 -> 50
        case .LatitudeDegrees:
            return "Latitude degrees must be between 10 and 59"
            
            // yeilds a regex for a range of 0.99 -> 59.99
        case .LatitudeMinutes(let gpsType):
            switch gpsType {
            case GpsType.DM:
                return "Latitude minutes must be between 0 and 59.99"
            case GpsType.DMS:
                return "Latitude minutes must be between 0 and 59"
            default:
                return nil
            }
            
            // yeilds a regex for a range of -135.99 -> -69.99
        case .LongitudeDecimal:
            return "Longitude decimal must be between -135.99 and -69.99"
            
            // yeilds a regex for a range of -135 -> -60
        case .LongitudeDegrees:
            return "Longitude degrees must be between -135 and -69"
            
            // yeilds a regex for a range of 0 -> 59.99999999
        case .LongitudeMinutes(let gpsType):
            switch gpsType {
            case GpsType.DM:
                return "Longitude minutes must be between 0 and 59.99"
            case GpsType.DMS:
                return "Longitude minutes must be between 0 and 59"
            default:
                return nil
            }
            
        default:
            return nil
        }

    }
    
    var validator: XLFormValidatorProtocol? {
        
        if let value = regex {
            return RegexFormRowValidator(msg: self.errorMessage, regex: value)
        }
        else if let value = numberValidatorOptions {
            return NumberFormRowValidator(msg: self.errorMessage, options: value)
        } else {
            return nil
        }
    }
    
    var row: XLFormRowDescriptor {
        let row = XLFormRowDescriptor(tag: self.tag.rawValue, rowType: self.rowType, title: self.title)
       
        if let value = validator {
            row.addValidator(value)
        }
        
        switch self {
        case .GpsFormat:
            row.selectorOptions = GpsType.pickerValues
            row.value = GpsType.D.formOptionObject
            
        default:
            row.cellConfig.setObject(NSTextAlignment.Right.rawValue, forKey: "textField.textAlignment")
        }
        
        return row
    }
}
