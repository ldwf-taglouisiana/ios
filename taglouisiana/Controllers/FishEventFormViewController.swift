//
//  FishEventFormViewController.swift
//  taglouisiana
//
//  Created by Daniel Ward on 4/2/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import UIKit
import XLForm
import MagicalRecord
import CoreLocation
import ImagePicker

class FishEventFormViewController: XLFormViewController, CLLocationManagerDelegate, ImagePickerDelegate {
    
    var pendingCapture: FishEvent?
    var isCopying: Bool = false
    var isRecapture: Bool = false
    
    // used for keeping an image
    var tagImages: [String: [UIImage]] = [String: [UIImage]]()
    var addingInmageForTag: String? = nil
    
    var locationManager: CLLocationManager
    
    var context: NSManagedObjectContext
    
    required init?(coder aDecoder: NSCoder) {
        context = NSManagedObjectContext.defaultContext()
        locationManager = CLLocationManager()
        
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initForm()
        
        // Do any additional setup after loading the view.
        
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            self.navigationController?.navigationBar.barTintColor = nil
        }
        
        if isCreatingCapture() {
            self.form.removeFormSectionAtIndex(0)
            self.form.addFormSection(CaptureFormRow.TagNumberSection.section, atIndex: 0)
        } else {
            title = "New Recapture"
        }
        
        self.populateForm()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBActions
    
    @IBAction func dismiss(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func formNextAction(sender: UIBarButtonItem) {
        if !self.validateForm() {
           return
        }
        
        showAddPhotosDialog()
    }
    
    // MARK: - XLFormViewController
    
    override func formRowDescriptorValueHasChanged(formRow: XLFormRowDescriptor!, oldValue: AnyObject!, newValue: AnyObject!) {
        super.formRowDescriptorValueHasChanged(formRow, oldValue: oldValue, newValue: newValue)
        
        if let formTag = formRow.tag {
            
            if formTag == CaptureFormRow.SpeciesLengthSwitch.tag.rawValue {
                if let value = newValue as? Bool {
                    
                    if value {
        
                        self.form.removeFormRowWithTag(CaptureFormRow.SpeciesLengthRange(context, nil).tag.rawValue)
                        self.form.removeFormRowWithTag(CaptureFormRow.SpeciesLengthManual.tag.rawValue)
                        self.form.addFormRow(CaptureFormRow.SpeciesLengthManual.row, afterRowTag: CaptureFormRow.SpeciesLengthSwitch.tag.rawValue)
                        
                    } else {
                        self.form.removeFormRowWithTag(CaptureFormRow.SpeciesLengthManual.tag.rawValue)
                        
                        let species = self.form.formRowWithTag(CaptureFormRow.SpeciesOption(context).tag.rawValue)!.value as! Species
                        
                        self.form.removeFormRowWithTag(CaptureFormRow.SpeciesLengthRange(context, nil).tag.rawValue)
                        self.form.addFormRow(CaptureFormRow.SpeciesLengthRange(context, species).row, afterRowTag: CaptureFormRow.SpeciesLengthSwitch.tag.rawValue)
                    }
                }
            }
        }
    }
    
    // MARK: - CLLocationManagerDelegate
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        let eventDate: NSDate = location.timestamp;
        let howRecent: NSTimeInterval = eventDate.timeIntervalSinceNow
        
        // if the most recent locaiton is less then 15 seconds, then we use it.
        if (abs(howRecent) < 15.0) {
            // get the locaiton form row
            let formRow = self.form.formRowWithTag(CaptureFormRow.Location.tag.rawValue)
            
            // get the old and new locations
            let oldFishEventLocaiton = formRow!.value as! FishEventLocation
            
            // if the location is at 0,0 then use the value from the locaiton manager
            if oldFishEventLocaiton.latitude.decimal == 0 && oldFishEventLocaiton.longitude.decimal == 0 {
                
                let fishEventLocation = FishEventLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude, type: oldFishEventLocaiton.type)
                
                // update the form row value then have the row redrawn
                formRow!.value = fishEventLocation
                self.updateFormRow(formRow)
                
            }
            
            // stop updating the location
            manager.stopUpdatingLocation()
        }

    }
    
    // MARK: - ImagePickerDelegate
    
    func wrapperDidPress(imagePicker: ImagePickerController, images: [UIImage]) {
    
    }
    
    func doneButtonDidPress(imagePicker: ImagePickerController, images: [UIImage]) {
        if let tagNumber = addingInmageForTag {
            if let savedImages = tagImages[tagNumber] {
               tagImages[tagNumber] = Array(Set(savedImages + images))
            } else {
                tagImages[tagNumber] = images
            }
        }
        
        addingInmageForTag = nil
        self.dismissViewControllerAnimated(true, completion: {
            self.showPhotoTagChoiceDialog()
        })
    }
    
    func cancelButtonDidPress(imagePicker: ImagePickerController) {
        addingInmageForTag = nil
        self.dismissViewControllerAnimated(true, completion: {
            self.showPhotoTagChoiceDialog()
        })
    }
    
    // MARK: - Helpers
    
    private func submit() {
        if self.validateForm() {
            
            let alertController = UIAlertController(title: "About to submit", message: "Are you sure you want to submit the entered data?", preferredStyle: .Alert)
            
            let dismissAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Destructive) { (alert: UIAlertAction) -> Void in
                // do nothing
            }
            
            
            let action = UIAlertAction(title: "Submit", style: UIAlertActionStyle.Default) { (alert: UIAlertAction) -> Void in
                if self.isEditing() {
                    self.editEntry()
                } else {
                    self.createEntity()
                }
                
                Api.PostData.execute()
            }
            
            alertController.addAction(action)
            alertController.addAction(dismissAction)
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    private func resetForm() {
        if  NSUserDefaults.standardUserDefaults().boolForKey(Constants.PREFERENCES_RESET_FORM) {
            self.initForm()
            self.tableView.reloadData()
        }
        
        // reset the tag number
        if isCreatingCapture() {
            self.form.removeFormSectionAtIndex(0)
            self.form.addFormSection(CaptureFormRow.TagNumberSection.section, atIndex: 0)
        } else {
            self.form.formRowWithTag(CaptureFormRow.TagNumber.tag.rawValue)!.value = nil
            self.tableView.reloadData()
        }
    }
    
    private func isCreatingCapture() -> Bool {
        return !isRecapture && (pendingCapture == nil || isCopying)
    }
    
    private func isEditing() -> Bool {
        return pendingCapture != nil && isCopying == false
    }
    
    private func populateForm() {
        if let item = pendingCapture {
            if isCopying == false {
                self.form.formRowWithTag(CaptureFormRow.TagNumber.tag.rawValue)!.value = item.tagNumber
            }
            
            self.form.formRowWithTag(CaptureFormRow.CaptureDate.tag.rawValue)!.value = isCopying ? NSDate() : item.date
            self.form.formRowWithTag(CaptureFormRow.TimeOfDay(context).tag.rawValue)!.value = isCopying ? TimeOfDayOption.current(context) : item.timeOfDay
            self.form.formRowWithTag(CaptureFormRow.SpeciesOption(context).tag.rawValue)!.value = item.species
            
            self.form.formRowWithTag(CaptureFormRow.SpeciesLengthManual.tag.rawValue)!.value = item.length
            
            self.form.formRowWithTag(CaptureFormRow.Condition(context).tag.rawValue)!.value = item.fishCondition == nil ? FishConditionOption.MR_findFirstInContext(context) : item.fishCondition
            
            
            self.form.formRowWithTag(CaptureFormRow.Location.tag.rawValue)!.value = item.location()
            
            if let comments = item.comments {
                self.form.formRowWithTag(CaptureFormRow.Comments.tag.rawValue)!.value = comments
            }
            
            self.tableView.reloadData()
        }
    }
    
    private func createEntity() {
        let formValues = self.form.formValues()
        let section = self.form.formSectionAtIndex(0)! as XLFormSectionDescriptor
        
        for sectionRow in section.formRows {
            let row = sectionRow as! XLFormRowDescriptor
            if let tag_number = row.value as? String {
                let entry = EnteredDraftCapture.MR_createEntityInContext(context) as EnteredDraftCapture
                writeValuesToEntity(entry, tagNumber: tag_number, formValues: formValues)
                
                if let tagNumberImages = tagImages[tag_number] {
                    for image in tagNumberImages {
                        let photoEntry = EnteredDraftCapturePhoto.MR_createEntityInContext(context) as EnteredDraftCapturePhoto
                        photoEntry.imageData = UIImageJPEGRepresentation(image, 1.0)!
                        photoEntry.enteredDraftCapture = entry
                    }
                }
            }
        }
        
        // save the context
        context.MR_saveToPersistentStoreWithCompletion({ success, error in
            if success  {
                self.showConfirmationDialog()
                self.resetForm()
            } else {
                self.showSaveErrorDialog()
            }
        })
    }
    
    private func editEntry() {
        let formValues = self.form.formValues()
        let section = self.form.formSectionAtIndex(0)! as XLFormSectionDescriptor
        
        var draft: DraftCapture?
        
        for sectionRow in section.formRows {
            let row = sectionRow as! XLFormRowDescriptor
            if let tagNumber = row.value as? String {
                
                /*
                    If a EnteredPendingCapture is being edited
                */
                if let item = pendingCapture as? EnteredDraftCapture {
                    
                    /*
                        If the item was deleted, we try to find the converted DraftCapture item.
                        If the DraftCapture cannot be found, then
                    */
                    if item.wasDeleted() {
                        // look to see if there is a draft capture with the same uuid
                        let draftCaptures = DraftCapture.MR_findByAttribute("uuid", withValue: item.uuid) as! [DraftCapture]
                        draft = draftCaptures.first
                        
                        // if there is a draft capture, then convert it to
                        if let draftCapture = draft {
                            let entity = draftCapture.convertToEnteredDraftCapture()
                            pendingCapture = entity
                            writeValuesToEntity(entity, tagNumber: tagNumber, formValues: formValues)
                        } else {
                            showEditErrorDialog()
                        }
                        
                    } else {
                        writeValuesToEntity(item, tagNumber: tagNumber, formValues: formValues)
                    }
                }
                
                /*
                    If a DraftCapture is being edited
                */
                else if let item = pendingCapture as? DraftCapture {
                    
                    /*
                        if the item was deleeted, then we cannot edit it anymore.
                        Display a message
                    */
                    if item.wasDeleted() {
                       showEditErrorDialog()
                    }
                    /*
                        We can still edit the item, so we make a EnteredPendingCapture
                    */
                    else {
                        let entity = item.convertToEnteredDraftCapture()
                        writeValuesToEntity(entity, tagNumber: tagNumber, formValues: formValues)
                    }
                } // end else if
            } // end if let
        } // end for
        
        // save the context
        context.MR_saveToPersistentStoreWithCompletion({ success, error in
            
            if success  {
                if let item = draft {
                    item.MR_deleteEntityInContext(self.context)
                    self.context.MR_saveToPersistentStoreAndWait()
                }
                
                self.dismissViewControllerAnimated(true, completion: nil)
                self.showConfirmationDialog()
            } else {
               self.showSaveErrorDialog()
            }
        })
    }
    
    private func writeValuesToEntity(entity: DraftCapture, tagNumber: String, formValues: [NSObject : AnyObject]) {
        entity.tagNumber = tagNumber
        entity.recapture = entity.recapture.boolValue || self.isRecapture
        
        if let value = formValues[CaptureFormRow.CaptureDate.tag.rawValue] as? NSDate {
            entity.date = value
        }
        
        if let value = formValues[CaptureFormRow.TimeOfDay(context).tag.rawValue] as? TimeOfDayOption {
            entity.timeOfDay = value
        }
        
        if let value = formValues[CaptureFormRow.SpeciesOption(context).tag.rawValue] as? Species {
            entity.species = value
        }
        
        if let value = formValues[CaptureFormRow.SpeciesLengthManual.tag.rawValue] as? Double {
            entity.length =  value
        }
        else if let value = formValues[CaptureFormRow.SpeciesLengthRange(context, nil).tag.rawValue] as? SpeciesLength {
            entity.speciesLength =  value
        }
        
        if let value = formValues[CaptureFormRow.SpeciesLengthManual.tag.rawValue] as? NSString {
            entity.length = value.doubleValue
        }
        
        if let value = formValues[CaptureFormRow.Condition(context).tag.rawValue] as? FishConditionOption {
            entity.fishCondition = value
        }

        if self.isRecapture {
            if let value = formValues[CaptureFormRow.Dispostion(context).tag.rawValue] as? RecaptureConditionOption {
                entity.dispostion = value
            }
        }
        
        if let value = formValues[CaptureFormRow.Location.tag.rawValue] as? FishEventLocation {
            entity.gpsType = value.type.rawValue
            entity.latitude = value.latitude.decimal
            entity.longitude = value.longitude.decimal
        }
        
        if let value = formValues[CaptureFormRow.Comments.tag.rawValue] as? String {
            entity.comments = value
            entity.locationDescription = value
        }
        
        entity.updatedAt = NSDate()
        entity.uuid = NSUUID().UUIDString

    }
    
    private func showConfirmationDialog() {
        let alertController = UIAlertController(title: "Info Submitted", message: "Thank you for submitting your tagging information!", preferredStyle: .Alert)
        let action = UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default) { (alert: UIAlertAction) -> Void in
            self.resetForm()
        }
        
        alertController.addAction(action)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    private func showSaveErrorDialog() {
        let alertController = UIAlertController(title: "Encountered a problem", message: "Looks like we are having some issues saving your data. If this problem persists, please contact us at info@taglouisian.com.", preferredStyle: .Alert)
        let action = UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default) { (alert: UIAlertAction) -> Void in
            // do nothing
        }
        
        alertController.addAction(action)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    private func showAddPhotosDialog() {
        let alertController = UIAlertController(title: "Add Photos", message: "Would you like to add any photos?", preferredStyle: .Alert)
        let addPhotosAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) { (alert: UIAlertAction) -> Void in
            self.showPhotoTagChoiceDialog()
        }
        
        let dismissAction = UIAlertAction(title: "Skip", style: UIAlertActionStyle.Default) { (alert: UIAlertAction) -> Void in
            self.submit()
        }
        
        alertController.addAction(addPhotosAction)
        alertController.addAction(dismissAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    private func showPhotoTagChoiceDialog() {
        let alertController = UIAlertController(title: "Select Tag", message: "Select which tag to add a photo to.", preferredStyle: .ActionSheet)
        
        let section = self.form.formSectionAtIndex(0)! as XLFormSectionDescriptor

        var tagNumbers = [String]()
        
        // collect all the tag numbers on the form
        if isEditing() {
            for sectionRow in section.formRows {
                let row = sectionRow as! XLFormRowDescriptor
                if let tagNumber = row.value as? String {
                    tagNumbers.append(tagNumber)
                }
            }
        } else {
            
            for sectionRow in section.formRows {
                let row = sectionRow as! XLFormRowDescriptor
                if let tag_number = row.value as? String {
                   tagNumbers.append(tag_number)
                }
            }
        }
        
        // add an action for each teag number colelcted
        for item in tagNumbers {
            let text = tagImages[item] == nil ? item : "\(item) (\(tagImages[item]!.count))"
            let action = UIAlertAction(title: text, style: UIAlertActionStyle.Default) { (alert: UIAlertAction) -> Void in
                self.addingInmageForTag = item
                
                let imagePickerController = ImagePickerController()
                imagePickerController.delegate = self
                self.presentViewController(imagePickerController, animated: true, completion: nil)
            }
            alertController.addAction(action)
        }
        
        
        // add the next action
        let doneAction = UIAlertAction(title: "Next", style: UIAlertActionStyle.Cancel) { (alert: UIAlertAction) -> Void in
            self.submit()
        }
        alertController.addAction(doneAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    private func initForm() {
        let formDescriptor:XLFormDescriptor = XLFormDescriptor(title: "New Capture")
        
        var section = XLFormSectionDescriptor.formSectionWithTitle("Tag Numbers") as XLFormSectionDescriptor!
        section!.addFormRow(CaptureFormRow.TagNumber.row)
        formDescriptor.addFormSection(section)
        
        section = XLFormSectionDescriptor.formSection() as XLFormSectionDescriptor!
        section!.addFormRow(CaptureFormRow.CaptureDate.row)
        section!.addFormRow(CaptureFormRow.TimeOfDay(context).row)
        formDescriptor.addFormSection(section)
        
        section = XLFormSectionDescriptor.formSection() as XLFormSectionDescriptor!
        section!.addFormRow(CaptureFormRow.SpeciesOption(context).row)
        section!.addFormRow(CaptureFormRow.SpeciesLengthManual.row)
        section!.addFormRow(CaptureFormRow.Condition(context).row)
        if isRecapture {
            section!.addFormRow(CaptureFormRow.Dispostion(context).row)
        }
        formDescriptor.addFormSection(section)
        
        section = XLFormSectionDescriptor.formSection() as XLFormSectionDescriptor!
        section!.addFormRow(CaptureFormRow.Location.row)
        formDescriptor.addFormSection(section)
        
        section = XLFormSectionDescriptor.formSection() as XLFormSectionDescriptor!
        section!.addFormRow(CaptureFormRow.Comments.row)
        formDescriptor.addFormSection(section)
        
        self.form = formDescriptor
    }

    private func showEditErrorDialog() {
        let alertController = UIAlertController(title: "Could not edit", message: "The item you are editing is no longer available", preferredStyle: .Alert)
        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            // do nothing
        }
        alertController.addAction(OKAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
}
