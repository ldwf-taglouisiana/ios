//
//  DashboardItemTableViewCell.swift
//  taglouisiana
//
//  Created by Daniel Ward on 11/18/16.
//  Copyright © 2016 Daniel Ward. All rights reserved.
//

import UIKit

class DashboardItemTableViewCell: UITableViewCell {

    @IBOutlet weak var itemTitle: UILabel!
    @IBOutlet weak var itemValue: UILabel!
    @IBOutlet weak var itemSubtitle: UILabel!
    
    override func awakeFromNib() {

        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
