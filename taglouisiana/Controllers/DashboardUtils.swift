//
//  DashboardUtils.swift
//  taglouisiana
//
//  Created by Daniel Ward on 11/17/16.
//  Copyright © 2016 Daniel Ward. All rights reserved.
//

import Foundation
import SwiftyJSON

struct DashboardItem {
    var title: String = ""
    var subtitle: String?
    var value: Int64?
    var items: [DashboardItem] = [DashboardItem]()
    
    init(title: String) {
        self.title = title
    }
    
    init(title: String, value: Int64) {
        self.title = title
        self.value = value
    }
    
    init(title: String, subtitle: String, value: Int64) {
        self.title = title
        self.subtitle = subtitle
        self.value = value
    }
}


enum DashboardType {
    case Public
    case Angler
    
    var pageTitle: String {
        switch self {
            
        case .Public:
            return "Program Stats"
            
        case .Angler:
            return "My Stats"
            
        }
    }
    
    var api: Api {
        switch self {
            
        case .Public:
            return Api.FetchPublicDashboard
            
        case .Angler:
            return Api.FetchAnglerDashboard
            
        }
    }
    
    var cacheStorageKey: String {
        switch self {
            
        case .Public:
            return "DashboardItem.JSON.Cache.Public"
            
        case .Angler:
            return "DashboardItem.JSON.Cache.Angler"
            
        }
    }
    
    func json(completion: (JSON) -> (Void)) {
        let prefs = NSUserDefaults.standardUserDefaults()
        
        if let cachedData = prefs.stringForKey(self.cacheStorageKey) {
            completion(JSON.parse(cachedData))
        }
        
        switch self {
        case .Public:
            return Api.FetchPublicDashboard.execute(completion)
            
        case .Angler:
            return Api.FetchAnglerDashboard.execute(completion)
        }
    }
    
    func fetchDashboardItems(completion: ([DashboardItem]) -> (Void)) {
        let prefs = NSUserDefaults.standardUserDefaults()
        
        self.json({
            json in
            prefs.setValue(json.rawString(), forKey: self.cacheStorageKey)
            completion(self.parseDashboardData(json))
        })
    }
    
    private func parseDashboardData(json: JSON) -> [DashboardItem] {
        switch self {
        case .Public:
            return processPublicData(json)
            
        case .Angler:
            return processAnglerData(json)
            
        }
    }
    
    private func processPublicData(json: JSON) -> [DashboardItem] {
        return self.processJsonData(json, jsonDataKey: json, totalTitle: "Program Totals")
    }
    
    private func processAnglerData(json: JSON) -> [DashboardItem] {
        return self.processJsonData(json["program"], jsonDataKey: json["angler"], totalTitle: "Your Totals")
    }
    
    private func processJsonData (jsonDatesKey: JSON, jsonDataKey: JSON, totalTitle: String = "Total title") -> [DashboardItem] {
        var results = [DashboardItem]()
        
        // season date values
        
        let currentSeasonStart = NSDate(timeIntervalSince1970: jsonDatesKey["season"]["current"]["start"]["epoch"].doubleValue)
        let currentSseasonEnd = NSDate(timeIntervalSince1970: jsonDatesKey["season"]["current"]["end"]["epoch"].doubleValue)
        
        let previousSeasonStart = NSDate(timeIntervalSince1970: jsonDatesKey["season"]["previous"]["start"]["epoch"].doubleValue)
        let previousseasonEnd = NSDate(timeIntervalSince1970: jsonDatesKey["season"]["previous"]["end"]["epoch"].doubleValue)
        
        // program totals
        
        var item = DashboardItem(title: totalTitle)
        item.items.append(DashboardItem(title: "Captures Overall", value: jsonDataKey["captures"]["total"].int64Value))
        item.items.append(DashboardItem(title: "Recaptures Overall", value: jsonDataKey["recaptures"]["total"].int64Value))
        item.items.append(DashboardItem(title: "Captures this season", subtitle: dateRangeString(currentSeasonStart, end: currentSseasonEnd), value: jsonDataKey["captures"]["season"]["current"].int64Value))
        item.items.append(DashboardItem(title: "Recaptures this season", subtitle: dateRangeString(currentSeasonStart, end: currentSseasonEnd), value: jsonDataKey["recaptures"]["season"]["current"].int64Value))
        item.items.append(DashboardItem(title: "Captures last season", subtitle: dateRangeString(previousSeasonStart, end: previousseasonEnd), value: jsonDataKey["captures"]["season"]["previous"].int64Value))
        item.items.append(DashboardItem(title: "Recaptures last season", subtitle: dateRangeString(previousSeasonStart, end: previousseasonEnd), value: jsonDataKey["recaptures"]["season"]["previous"].int64Value))
        
        results.append(item)
        
        // captures by species
        
        item = DashboardItem(title: "Captures by Species (Last 30 days)")
        for jsonElement in jsonDataKey["captures"]["species"].arrayValue {
            item.items.append(DashboardItem(title: jsonElement["name"].stringValue, value: jsonElement["count"].int64Value))
        }
        
        if item.items.count == 0 {
            item.items.append(DashboardItem(title: "No Data."))
        }
        
        results.append(item)
        
        
        // recaptures by species
        item = DashboardItem(title: "Recaptures by Species (Last 30 days)")
        for jsonElement in jsonDataKey["recaptures"]["species"].arrayValue {
            item.items.append(DashboardItem(title: jsonElement["name"].stringValue, value: jsonElement["count"].int64Value))
        }
        
        if item.items.count == 0 {
            item.items.append(DashboardItem(title: "No Data."))
        }
        
        results.append(item)
        
        
        return results
    }
    
    private func dateRangeString(start: NSDate, end: NSDate) -> String {
        let formatter = NSDateFormatter()
        formatter.dateStyle = NSDateFormatterStyle.MediumStyle
    
        return "\(formatter.stringFromDate(start)) - \(formatter.stringFromDate(end))"
    }
    
}
