//
//  LoginViewController.swift
//  taglouisiana
//
//  Created by Daniel Ward on 3/23/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import UIKit
import NXOAuth2Client
import MMMaterialDesignSpinner

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var resetPasswordButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var reportRecaptureButton: UIButton!
    @IBOutlet weak var syncingLabel: UILabel!
    @IBOutlet weak var activityIndicator: MMMaterialDesignSpinner!
    
    var notificationObservers: [AnyObject]?
    var hasBegunDataSyncing: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator!.lineWidth = 2.0
        activityIndicator!.tintColor = Constants.UICOLOR_ORANGE
        
        emailField!.delegate = self
        passwordField!.delegate = self
        
        self.registerForLoginNotifications()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        if let observers = notificationObservers {
            observers.forEach({ (observer) -> () in
                NSNotificationCenter.defaultCenter().removeObserver(observer)
            })
        }
    }
    
    // MARK - UITextFieldDelegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        let nextTage = textField.tag + 1;
     
        // Try to find next responder
        if let nextResponder = textField.superview?.viewWithTag(nextTage) as UIResponder? {
            // Found next responder, so set it.
            nextResponder.becomeFirstResponder()
        }
        else
        {
            // Not found, so remove keyboard
            textField.resignFirstResponder()
        }
        
        return false // We do not want UITextField to insert line-breaks.
    }
    
    // MARK - IBActions

    /**
        When the user taps the sign in button, we will disabled the text input, hide the UI elements.
        and start the sign in process.
    */
    @IBAction func signInButtonTapped(sender: AnyObject) {
        self.emailField!.enabled = false
        self.passwordField!.enabled = false
        self.hideUIElements()
    }

    /**
     */
    @IBAction func reportRecaptureTapped(sender: AnyObject) {
        UIApplication.sharedApplication().openURL(NSURL(string: "https://taglouisiana.com/non_registered_captures/new")!)
    }
    
    
    /**
        When the register button is tapped, the user will be taken to https://taglouisiana.com/sign_up/new
        where they can create a new account.
    */
    @IBAction func registerButtonTapped(sender: AnyObject) {
        UIApplication.sharedApplication().openURL(NSURL(string: "https://taglouisiana.com/sign_up/new")!)
    }
    
    /**
        When the reset password button is tapped, the user will be taken to https://taglouisiana.com/users/help
        where they can reset thier password
    */
    @IBAction func resetPasswordButtonTapped(sender: AnyObject) {
        UIApplication.sharedApplication().openURL(NSURL(string: "https://taglouisiana.com/users/help")!)
    }
    
    // MARK - Helpers
    
    private func registerForLoginNotifications() {
        
        let oauthStore = NXOAuth2AccountStore.sharedStore() as! NXOAuth2AccountStore
        
        let successObserver = NSNotificationCenter.defaultCenter().addObserverForName(NXOAuth2AccountStoreAccountsDidChangeNotification, object: oauthStore, queue: nil, usingBlock: { notification in
            self.loginSuccessful()
        })
        
        let failedObserver = NSNotificationCenter.defaultCenter().addObserverForName(NXOAuth2AccountStoreDidFailToRequestAccessNotification, object: oauthStore, queue: nil, usingBlock: { notification in
            self.loginFailed()
        })
        
        let syncObserver = NSNotificationCenter.defaultCenter().addObserverForName(Constants.NOTIFICATION_DATA_UPDATE_FINISHED, object: nil, queue: nil, usingBlock: { notificaton in
            if self.hasBegunDataSyncing {
                Utils.setFirstSyncCompleted()
                
                dispatch_async(dispatch_get_main_queue(),{
                    self.dismissViewControllerAnimated(true, completion: nil)
                })
            }
        })
        
        notificationObservers = [successObserver, failedObserver, syncObserver]
    }
    
    private func showUIElements() {
        
        // animate all the interactable items to an alpha of 1
        let setAlphasToZero =  {  () -> Void in
            self.signInButton!.alpha = 1.0
            self.registerButton!.alpha = 1.0
            self.resetPasswordButton!.alpha = 1.0
            self.reportRecaptureButton!.alpha = 1.0
        }
        
        // set all the hidded elements back to a shown state
        self.signInButton!.hidden = false
        self.registerButton!.hidden = false
        self.resetPasswordButton!.hidden = false
        self.reportRecaptureButton!.hidden = false

        // hide the activity indicator
        self.activityIndicator!.hidden = true
        
        // start the animations
        UIView.animateWithDuration(0.5, delay: 0.1, options: .CurveEaseOut, animations: setAlphasToZero , completion: nil)
    }
    
    private func hideUIElements() {
        
        // animate all the interactable items to an alpha of 0
        let setAlphasToZero =  {  () -> Void in
            self.signInButton!.alpha = 0.0
            self.registerButton!.alpha = 0.0
            self.resetPasswordButton!.alpha = 0.0
            self.reportRecaptureButton!.alpha = 0.0
        }
        
        // used for when the animations complete
        let completedAnimations: ((Bool) -> Void)? = { (Bool) -> Void in
            
            // hide all the animated elements
            self.signInButton!.hidden = true
            self.registerButton!.hidden = true
            self.resetPasswordButton!.hidden = true
            self.reportRecaptureButton!.hidden = true
            
            // show the activity indicator and start the animations
            self.activityIndicator!.hidden = false
            self.activityIndicator!.startAnimating()
            
            // start the sign in process
            self.beginSignIn()
        }
        
        // start the animations
        UIView.animateWithDuration(0.5, delay: 0.1, options: .CurveEaseOut, animations: setAlphasToZero , completion: completedAnimations)
    }

    
    private func beginSignIn() {
        // we are adding an artificial delay, to give the perception that there is work happening
        Utils.delay(2.0, closure: {
            let oauthStore = NXOAuth2AccountStore.sharedStore() as! NXOAuth2AccountStore
            oauthStore.requestAccessToAccountWithType(Constants.AOUTH_ACCOUNT, username: self.emailField!.text, password: self.passwordField!.text)
        })
    }
    
    private func loginSuccessful() {
        // animate all the interactable items to an alpha of 0
        let setAlphasToZero =  {  () -> Void in
            self.emailField!.alpha = 0.0
            self.passwordField!.alpha = 0.0
            self.syncingLabel!.alpha = 1.0
        }
        
        // used for when the animations complete
        let completedAnimations: ((Bool) -> Void)? = { (Bool) -> Void in
            
            // hide all the animated elements
            self.emailField!.hidden = true
            self.passwordField!.hidden = true
            self.syncingLabel!.hidden = false
        
            // need add a delay, so that the account info is stored before proceeding
            Utils.delay(4.0, closure: {
                self.hasBegunDataSyncing = true
                Utils.syncData()
            })
        }
        
        AuthUtils.setLoginedInToken()
        
        Utils.setupSDWebImageManager()
        
        // start the animations
        UIView.animateWithDuration(0.5, delay: 0.1, options: .CurveEaseOut, animations: setAlphasToZero , completion: completedAnimations)

    }
    
    private func loginFailed() {
        self.activityIndicator!.stopAnimating()
        
        let alertController = UIAlertController(title: "Sign In Error", message: "Invalid email or password. Please try again.", preferredStyle: .Alert)
      
        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            self.emailField!.enabled = true
            self.passwordField!.enabled = true
            self.showUIElements();
        }
        
        alertController.addAction(OKAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
}
