//
//  NewFishEventTableViewController.swift
//  taglouisiana
//
//  Created by Daniel Ward on 3/27/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import UIKit

class NewFishEventTableViewController: UITableViewController {

    let RECAPTURE_ITEM_INDEX    : Int = 1
    let CAPTURE_ITEM_INDEX      : Int = 2
    let TIME_ENTTRY_ITEM_INDEX  : Int = 3
    let TAG_REQUEST_ITEM_INDEX  : Int = 4
    
    var choices: [[String: Int]] {
        var options = [["New Recapture" : RECAPTURE_ITEM_INDEX], ["New Capture": CAPTURE_ITEM_INDEX]]
        options = options + [["Add Time On Water": TIME_ENTTRY_ITEM_INDEX], ["Request Tags" : TAG_REQUEST_ITEM_INDEX]]
        
        return options
    }
    
    var pendingItem: DraftCapture?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            self.navigationController?.navigationBar.barTintColor = nil
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return choices.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) 

        if let label = cell.textLabel {
            label.text = choices[indexPath.row].keys.first
        }

        return cell
    }
        
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch choices[indexPath.row].values.first! {
        case RECAPTURE_ITEM_INDEX, CAPTURE_ITEM_INDEX:
            self.performSegueWithIdentifier(Constants.SEGUE_NEW_FISH_EVENT, sender: self)
        case TIME_ENTTRY_ITEM_INDEX:
            self.performSegueWithIdentifier(Constants.SEGUE_TIME_ON_WATER, sender: self)
        case TAG_REQUEST_ITEM_INDEX:
            self.performSegueWithIdentifier(Constants.SEGUE_REQUEST_TAGS, sender: self)
        default:
            return
        }
    }

    @IBAction func dismiss(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Navigation
//
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        // check if createing a recapture
        if let indexPath = self.tableView.indexPathForSelectedRow {
            let index = choices[indexPath.row].values.first!
            if index == RECAPTURE_ITEM_INDEX {
                let navController = segue.destinationViewController as? UINavigationController
                let controller = navController?.topViewController as! FishEventFormViewController
                controller.isRecapture = true
            }
        }
        
        if self.popoverPresentationController != nil {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
}
