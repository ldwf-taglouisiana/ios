//
//  NumberFormValidator.swift
//  taglouisiana
//
//  Created by Daniel Ward on 6/2/16.
//  Copyright © 2016 Daniel Ward. All rights reserved.
//

import Foundation

import XLForm
import NSString_Ruby

class NumberFormRowValidator: NSObject, XLFormValidatorProtocol {
    
    private var message: String
    private var options: [String: AnyObject]
    
    static let GREATER_THAN_VALUE       = "NumberFormRowValidator.greater_than_value"
    static let GREATER_THAN_EQUAL_VALUE = "NumberFormRowValidator.greater_than_equal_value"
    static let LESS_THAN_VALUE          = "NumberFormRowValidator.less_than_value"
    static let LESS_THAN_EQUAL_VALUE    = "NumberFormRowValidator.less_than_equal_value"
    static let WHOLE_NUMBER_ONLY        = "NumberFormRowValidator.whole_numbers_only"
    
    init!(msg: String!, options: [String: AnyObject]) {
        self.message = msg
        self.options = options
        
        //min: Double = -Double.infinity, max: Double = Double.infinity, wholeNumbersOnly: Bool = false
    }
    
    func isValid(row: XLFormRowDescriptor!) -> XLFormValidationStatus! {
        var value: Double = -1 * Double.infinity
        
        if let rowDescriptor = row, let rowValue: AnyObject = rowDescriptor.value {
            if rowValue is NSNumber {
                value = Double(rowValue as! NSNumber)
            }
            
            if rowValue is NSString {
                if let parsedValue = Double(rowValue as! String) {
                    value = parsedValue
                }
            }
        }
        
        let max = options[NumberFormRowValidator.LESS_THAN_EQUAL_VALUE]
                   ?? options[NumberFormRowValidator.LESS_THAN_VALUE]
                   ?? Double.infinity

        let min = options[NumberFormRowValidator.GREATER_THAN_EQUAL_VALUE]
                    ?? options[NumberFormRowValidator.GREATER_THAN_VALUE]
                    ?? -Double.infinity
        
        // check that the values is in the specified range
        var isValid = (min as! Double)...(max as! Double) ~= value
        
        
        // if there is a hard lower bound set, then check it
        if let lowerBound = (options[NumberFormRowValidator.GREATER_THAN_VALUE] as? Double) {
            isValid = isValid && value > lowerBound
        }
        
        // if there is a hard upper bound set, then check it
        if let upperBound = (options[NumberFormRowValidator.LESS_THAN_VALUE] as? Double) {
            isValid = isValid && value < upperBound
        }
        
        // if we should only allow whole numbers
        let wholeNumbersOnly = options[NumberFormRowValidator.WHOLE_NUMBER_ONLY] as? Bool
        if isValid && wholeNumbersOnly != nil && wholeNumbersOnly! {
            isValid = isValid && floor(value) == value
        }
        
        return XLFormValidationStatus(msg: self.message, status: isValid, rowDescriptor: row)
    }
}
