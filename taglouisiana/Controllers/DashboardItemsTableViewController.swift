//
//  TableViewController.swift
//  taglouisiana
//
//  Created by Daniel Ward on 11/17/16.
//  Copyright © 2016 Daniel Ward. All rights reserved.
//

import UIKit
import SwiftyJSON
import XLPagerTabStrip

class DashboardItemsTableViewController: UITableViewController, IndicatorInfoProvider {
    
    let numberFormatter = NSNumberFormatter()
    var pageTitle: String = "Test Title"
    
    var dashboardType: DashboardType? {
        didSet {
            self.reloadData()
        }
    }
    
    var items: [DashboardItem] = [DashboardItem]()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        numberFormatter.numberStyle = .DecimalStyle
        reloadData()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        reloadData()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: pageTitle)
    }

    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return items.count
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return items[section].items.count
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return items[section].title
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(Constants.UI_REUSE_IDENTIFIER_DASHBOARD_ITEM_CELL, forIndexPath: indexPath) as! DashboardItemTableViewCell
        let item = items[indexPath.section].items[indexPath.row]
        
        if let labelView = cell.itemTitle {
            labelView.text = item.title
        }

        if let labelView = cell.itemSubtitle {
            
            if let subtitle = item.subtitle {
                 labelView.hidden = false
                labelView.text = subtitle
            } else {
                labelView.hidden = true
            }
        }
        
        if let labelView = cell.itemValue {
            if let value = item.value {
                labelView.text = numberFormatter.stringFromNumber(NSNumber(longLong: value))
            } else {
                labelView.text = ""
            }
        }
        
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    private func reloadData() {
        if let currentDashboardType = dashboardType {
            currentDashboardType.fetchDashboardItems({
                items in
                self.items = items
                self.tableView.reloadData()
            })
        }
    }
    
   
    
}
