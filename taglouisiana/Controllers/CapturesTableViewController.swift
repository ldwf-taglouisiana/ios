//
//  CapturesTableViewController.swift
//  taglouisiana
//
//  Created by Daniel Ward on 3/26/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import UIKit
import MagicalRecord

class CapturesTableViewController: UITableViewController, UISearchResultsUpdating, UISearchControllerDelegate {
    
    let cellIdentifier = "CaptureCell"
    
    var detailViewController: CaptureDetailPager? = nil
    
    var searchController: UISearchController?
    var searchResultsController: UITableViewController?
    
    var captures = [Capture]()
    var filteredList = [Capture]()
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            self.navigationController?.navigationItem.title = "List"
            self.clearsSelectionOnViewWillAppear = false
            self.preferredContentSize = CGSize(width: 320.0, height: 551.0)
            
            self.navigationItem.rightBarButtonItem = nil
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupSearch()
        
        if let split = self.splitViewController {
            split.presentsWithGesture = false
            if let navController = split.viewControllers[split.viewControllers.count-1] as? UINavigationController {
                self.detailViewController = navController.topViewController as? CaptureDetailPager
            }
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CapturesTableViewController.forceSyncStarted(_:)), name: Constants.NOTIFICATION_FORCE_SYNC_STARTED, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CapturesTableViewController.forceSyncFinished(_:)), name: Constants.NOTIFICATION_FORCE_SYNC_FINISHED, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CapturesTableViewController.managedObjectContextDidChange(_:)), name: Constants.NOTIFICATION_DATA_UPDATE_FINISHED, object: nil)
        
        refreshData()
    }
    
    override func viewWillAppear(animated: Bool) {
        refreshData(false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: Constants.NOTIFICATION_FORCE_SYNC_STARTED, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: Constants.NOTIFICATION_FORCE_SYNC_FINISHED, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: Constants.NOTIFICATION_DATA_UPDATE_FINISHED, object: nil)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return tableView == self.searchResultsController?.tableView ? filteredList.count : captures.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as! CaptureTableViewCell
        let capture = getItemAtIndexPath(tableView, indexPath: indexPath)
        
        if let label = cell.tagNumberLabel {
            label.text = capture.tagNumber
        }
        
        if let label = cell.captureDateLabel {
            label.text = capture.date.USDate
        }
        
        if let label = cell.speciesName {
            label.text = capture.species.commonName
        }
        
        if let image = cell.hasImagesIcon {
            image.hidden = capture.images.isEmpty
        }
        
        if let label = cell.hasRecaptures {
            label.hidden = capture.recaptureCount.intValue == 0
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.tableView.rowHeight
    }
    
    
    // MARK: - UISearchResultsUpdating
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        
        if self.searchController?.searchBar.text!.lengthOfBytesUsingEncoding(NSUTF32StringEncoding) > 0 {
            let searchString = searchController.searchBar.text
            let predicate = NSPredicate(format: "tagNumber contains[c] %@ OR species.commonName contains[c] %@", argumentArray: [searchString!, searchString!])
            let request = Capture.MR_requestAllSortedBy("date", ascending: false, withPredicate: predicate)
            request.includesSubentities = false
            
            filteredList = Capture.MR_executeFetchRequest(request) as! [Capture]
            
            self.searchResultsController?.tableView.reloadData()
        }
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        if segue.identifier == Constants.SEGUE_SHOW_CAPTURE_DETAIL {
            let navigationController = segue.destinationViewController as! UINavigationController
            let controller = navigationController.topViewController as! CaptureDetailPager
            
            // get the sender cell and check if the cell is in the search results controller
            let senderCell = sender as! UITableViewCell
            let searchIndexPathOfSelectedCell = self.searchResultsController?.tableView.indexPathForCell(senderCell)
            
            var captureItem: Capture? = nil
            
            // the index path and the tableview are not nil, then a search cell was selected
            if let indexPath = searchIndexPathOfSelectedCell, let tableView = self.searchResultsController?.tableView {
                captureItem = getItemAtIndexPath(tableView, indexPath: indexPath)
                
            } else {
                
                // if the cell is not a search cell, then it is the regualr table view
                if let indexPath = self.tableView.indexPathForSelectedRow {
                    captureItem = getItemAtIndexPath(self.tableView, indexPath: indexPath)
                }
            }
            
            if let item = captureItem {
                if item.recaptureCount == 0 {
                    let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                    let detailController = storyboard.instantiateViewControllerWithIdentifier(Constants.STORYBOARD_ID_CAPTURE_DETAIL_VIEW_CONTROLLER) as! CapturesDetailViewController
                    detailController.detailItem = item
                    navigationController.setViewControllers([detailController], animated: false)
                    
                    // add the left nav item so we can show the list again
                    detailController.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                    detailController.navigationItem.leftItemsSupplementBackButton = true
                } else {
                    controller.captureItem = item
                }
            }
            
            self.splitViewController?.toggleMasterView()
            
            // add the left nav item so we can show the list again
            controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
            controller.navigationItem.leftItemsSupplementBackButton = true
        }
    }
    
    // MARK: - Helpers
    
    private func refreshData(updateDetail: Bool = true) {
        let request = Capture.MR_requestAllSortedBy("date", ascending: false)
        request.includesSubentities = false
    
        // get the captures list
        self.captures = Capture.MR_executeFetchRequest(request) as! [Capture]

        dispatch_async(dispatch_get_main_queue(), {

//            // uncomment to update the detail view with the first item in the list
//            if updateDetail && self.detailViewController?.detailItem == nil && self.captures.count > 0 {
//                self.detailViewController?.detailItem = self.captures[0]
//            }

            self.tableView.reloadData()
        })
    }
    
    private func getItemAtIndexPath(tableView: UITableView, indexPath: NSIndexPath) -> Capture {
        if tableView == self.searchResultsController?.tableView {
            return filteredList[indexPath.row]
        } else {
            return captures[indexPath.row]
        }
    }
    
    /**
        From https://github.com/Mozharovsky/iOS-Demos/blob/master/Search%20Mechanism/Swift/Search%20Mechanism/ViewController.swift
    */
    private func setupSearch() {
        // A table for search results and its controller.
        let resultsTableView = UITableView(frame: self.tableView.frame)
        self.searchResultsController = UITableViewController()
        self.searchResultsController?.tableView = resultsTableView
        self.searchResultsController?.tableView.dataSource = self
        self.searchResultsController?.tableView.delegate = self
        
        // Register cell class for the identifier.
        self.searchResultsController?.tableView.registerClass(CaptureTableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        
        self.searchController = UISearchController(searchResultsController: self.searchResultsController!)
        self.searchController?.searchResultsUpdater = self
        self.searchController?.delegate = self
        self.searchController?.searchBar.sizeToFit()
        self.tableView.tableHeaderView = self.searchController?.searchBar
        
        self.definesPresentationContext = true
    }
    
    func managedObjectContextDidChange(sender: NSNotification) {
        self.refreshData()
    }
    
    func forceSyncStarted(sender: NSNotification) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: NSManagedObjectContextObjectsDidChangeNotification, object: nil)
    }
    
    func forceSyncFinished(sender: NSNotification) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CapturesTableViewController.managedObjectContextDidChange(_:)), name: NSManagedObjectContextDidSaveNotification, object: NSManagedObjectContext.defaultContext())
        
        self.detailViewController?.captureItem = nil
        self.refreshData()
    }
}
