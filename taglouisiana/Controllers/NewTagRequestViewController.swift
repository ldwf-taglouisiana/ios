//
//  NewTagRequestViewController.swift
//  taglouisiana
//
//  Created by Daniel Ward on 3/30/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import MagicalRecord
import MessageUI

class NewTagRequestViewController: UIViewController, MFMailComposeViewControllerDelegate {
    
    let TAG_TYPE_BUTTON_TAG: Int = 1
    let QUANTITY_BUTTON_TAG: Int = 2
    
    let tagTypes = ["Redfish/Trout Tags", "Tuna Tags"]
    var quantities: [String] {
        var result = [String]()
        for i in 10.stride(to: 100, by: 10) {
            result.append(String(i))
        }
        return result;
    }
    
    var typeSelectedIndex = 0
    var quantitySelectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            self.navigationController?.navigationBar.barTintColor = nil
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismiss(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func submit(sender: AnyObject) {
        /*
            Save the entry.
            If successful, then sync data and dismiss view.
            Else show alert indicating there was an error saving the entity
        */
        MagicalRecord.saveWithBlock({ context in
            let entry = EnteredUserTagRequest.MR_createEntityInContext(context) as EnteredUserTagRequest
            entry.tagType = self.tagTypes[self.typeSelectedIndex]
            entry.quantity = Int(self.quantities[self.quantitySelectedIndex])!
            
            entry.updatedAt = NSDate()
            entry.uuid = NSUUID().UUIDString
            
            }, completion: { success, error in
                if success {
                    Utils.syncData()
                    self.dismissViewControllerAnimated(true, completion: nil)
                } else {
                    Utils.log("Could not save the request: \(error)")
                    let actionController = Utils.createAlertDialog(
                                                        title: "Error",
                                                        message: "Could not save tag request. If problem persists, please email \(Constants.CONTACT_EMAIL_ADDRESS)",
                                                        okTitle: "Send Email",
                                                        okAction: { (action) in
                                                            let mailVController = Utils.createEmailViewController(delegate: self)
                                                            self.presentViewController(mailVController, animated: true, completion: nil)
                                                        },
                                                        cancelTitle: "Close",
                                                        cancelAction: { (action) in }
                                           )
                    self.presentViewController(actionController, animated: true, completion: nil)
                }
        })
    }
    
    @IBAction func buttonSelected(sender: UIButton) {
        let items = (sender.tag == TAG_TYPE_BUTTON_TAG) ? tagTypes : quantities
        let index = (sender.tag == TAG_TYPE_BUTTON_TAG) ? typeSelectedIndex : quantitySelectedIndex
        
        let picker: ActionSheetStringPicker = ActionSheetStringPicker(title: "", rows: items, initialSelection: index, target: self, successAction: #selector(NewTagRequestViewController.itemSelected(_:element:)), cancelAction: nil, origin: sender)
        picker.showActionSheetPicker()
    }
    
    func itemSelected(index: NSNumber, element: UIButton) {
        var items = (element.tag == TAG_TYPE_BUTTON_TAG) ? tagTypes : quantities
        let title: String = items[index.integerValue]
        
        switch element.tag {
        case TAG_TYPE_BUTTON_TAG:
            typeSelectedIndex = index.integerValue
        case QUANTITY_BUTTON_TAG:
            quantitySelectedIndex = index.integerValue
        default:
            return
        }
        
        element.setTitle(title, forState: UIControlState.Normal)
    }

    // MARK: MFMailComposeViewControllerDelegate
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
}
