//
//  FishEventLocationValidator.swift
//  taglouisiana
//
//  Created by Daniel Ward on 4/8/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import Foundation
import XLForm

class FishEventLocationValidator: NSObject, XLFormValidatorProtocol {
    func isValid(row: XLFormRowDescriptor!) -> XLFormValidationStatus! {
        let message = "Location is not valid"
        var isValid = false
        
        if let value = row.value as? FishEventLocation {
            if (value.latitude.decimal > 10.0 && value.latitude.decimal < 60.0) && (value.longitude.decimal > -130.0 && value.longitude.decimal < -70.0) {
                isValid = true
            }
        }
        
        return XLFormValidationStatus(msg: message, status: isValid, rowDescriptor: row)
    }
}