//
//  MoreTabViewController.swift
//  taglouisiana
//
//  Created by Daniel Ward on 4/13/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import UIKit
import MessageUI
import MagicalRecord

class MoreTabViewController: UIViewController, MFMailComposeViewControllerDelegate, UIPopoverPresentationControllerDelegate {

    @IBOutlet weak var versionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if let label = versionLabel {
            let buildString = NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleVersion") as! String
            let versionString = NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleShortVersionString") as! String
            label.text = "Version \(versionString) (\(buildString))"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Actions
    
    @IBAction func contactUs(sender: UIButton) {
        let alertController = UIAlertController(title: "Contact Us", message: "", preferredStyle: .ActionSheet)
        let popOver = alertController.popoverPresentationController
        popOver?.sourceView  = sender as UIView
        popOver?.sourceRect = (sender as UIView).bounds
        popOver?.permittedArrowDirections = UIPopoverArrowDirection.Any
        
        let EmailAction = UIAlertAction(title: "Email", style: .Default) { (action) in
            let mailComposerVC = Utils.createEmailViewController(delegate: self)
            self.presentViewController(mailComposerVC, animated: true, completion: nil)
        }
        
        let CallAction = UIAlertAction(title: "Call", style: .Default) { (action) in
            UIApplication.sharedApplication().openURL(NSURL(string: "tel:2257635415")!)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
            
        }
        
        // if we can send mail, then add the option
        if MFMailComposeViewController.canSendMail() {
            alertController.addAction(EmailAction)
        }
        
        // if we can make a phone call, then add the option
        if UIApplication.sharedApplication().canOpenURL(NSURL(string: "tel://")!) {
            alertController.addAction(CallAction)
        }
        
        alertController.addAction(cancelAction)
        
        // preset the alert dialog
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    @IBAction func forceSync(sender: UIButton) {
        
        let message = "This action will remove all on device data. Make sure all entered pending captures, that you do not wish to lose, have been synced."
        let alertController = UIAlertController(title: "Force Resync", message: message, preferredStyle: .Alert)
        
        let CancelAction = UIAlertAction(title: "Cancel", style: .Default) { (action) in
            // do nothing
        }
        
        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            self.performSegueWithIdentifier(Constants.SEGUE_FORCE_SYNC, sender: self)
        }
        
        // add the actions
        alertController.addAction(CancelAction)
        alertController.addAction(OKAction)
        
        // preset the alert dialog
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == Constants.SEGUE_LOGIN  {
            if (Utils.rebuildDatastore()) {
                AuthUtils.signOut()
            }
        }
    }
}
