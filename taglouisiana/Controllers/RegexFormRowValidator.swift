//
//  RegexFormRowValidator.swift
//  taglouisiana
//
//  Created by Daniel Ward on 4/9/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import Foundation
import XLForm
import NSString_Ruby

class RegexFormRowValidator: NSObject, XLFormValidatorProtocol {
    
    private var message: String
    private var regex: String
    
    init!(msg: String!, regex: String!) {
        self.message = msg
        self.regex = regex
    }
    
    func isValid(row: XLFormRowDescriptor!) -> XLFormValidationStatus! {
        var value: NSString = ""
        
        if let rowDescriptor = row, let rowValue: AnyObject = rowDescriptor.value {
           if rowValue is NSNumber {
                value = (rowDescriptor.value as! NSNumber).stringValue
            }
            
            if rowValue is NSString {
                value = rowValue as! String
            }
        }
        
        // remove any trialing/leadng whitespace characters
        value = value.strip()
        
        
        // check the there is only one match
        let isValid = value.match(self.regex).count == 1
        
        return XLFormValidationStatus(msg: self.message, status: isValid, rowDescriptor: row)
    }
}
