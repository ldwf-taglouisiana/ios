//
//  ManualLocationFormViewController.swift
//  taglouisiana
//
//  Created by Daniel Ward on 4/6/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import UIKit
import XLForm

class ManualLocationFormViewController: XLFormViewController, XLFormRowDescriptorViewController {
    
    var rowDescriptor: XLFormRowDescriptor?
    var fishEventLocation: FishEventLocation = FishEventLocation(latitude: 0.0, longitude: 0.0, type: GpsType.D)
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        // create a form descriptor that this will use
        let formDescriptor = XLFormDescriptor(title: "Enter Location")
        
        // add the GPS format section to the form
        let section = XLFormSectionDescriptor.formSection() as XLFormSectionDescriptor!
        section.addFormRow(LocationFormRow.GpsFormat.row)
        formDescriptor.addFormSection(section)
        
        /*
            The other sections are added dynamcically when the view loads
        */
        
        // add the form descriptor to this
        super.form = formDescriptor
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // if the row discriptor exists and has a value
        if let row = rowDescriptor {
            if let value = row.value as? FishEventLocation {
                fishEventLocation = FishEventLocation(latitude: value.latitude.decimal, longitude: value.longitude.decimal, type: value.type)
                
                // get the format row
                let formatRow = self.form.formRowWithTag(LocationFormRow.GpsFormat.tag.rawValue)
                
                // update teh descriptor value to the correct one
                formatRow!.value = fishEventLocation.type.formOptionObject
            }
        }
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .Plain, target: self, action: #selector(ManualLocationFormViewController.saveLocationChanges(_:)))
    }
    
    // MARK: - Actions
    
    func saveLocationChanges(sender: UIBarButtonItem) {
        if self.validateForm() {
            rowDescriptor?.value = fishEventLocation
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    // MARK: - XLFormViewController
    
    override func formRowDescriptorValueHasChanged(formRow: XLFormRowDescriptor!, oldValue: AnyObject!, newValue: AnyObject!) {
        super.formRowDescriptorValueHasChanged(formRow, oldValue: oldValue, newValue: newValue)
        
        if let tag = formRow.tag {
        
        // if the change row is the format row then update the form to show the correct fields
        switch tag {
        
        /*
            If the GPS Format selector was changed, we need to update the format type
            and redraw the rows depending on the selected type
        */
        case LocationFormRow.GpsFormat.tag.rawValue:
            let updatedValue = (newValue as! XLFormOptionObject).formValue() as! String
            let type: GpsType = GpsType(rawValue: updatedValue)!
            
            switch type {
            case .D:
                self.createDecimalSections()
                
            case .DM:
                self.createDegreeMinutesSections(.DM)
                
            case .DMS:
                self.createDegreeMinutesSecondsSections(.DMS)
                
            default:
                () // do nothing
            }
            
            self.fishEventLocation = FishEventLocation(latitude: fishEventLocation.latitude, longitude: fishEventLocation.longitude, type: type)
            
        /*
            If an entered value gets changed then, update the coordinates accordingly to the 
            field that was changed.
            
            If the value of the changed form row is nil, then the changed is ignored.
        */
        case LocationFormRow.LatitudeDecimal.tag.rawValue:
            if let value = formRow.value as? NSNumber {
                fishEventLocation.latitude = Coordinate(decimal: value.doubleValue)
            }
            
        case LocationFormRow.LatitudeDegrees(fishEventLocation.type).tag.rawValue:
            if let value = formRow.value as? NSNumber {
                fishEventLocation.setDegrees(value.doubleValue, component: CoordinateComponent.Latitude)
            }
            
        case LocationFormRow.LatitudeMinutes(fishEventLocation.type).tag.rawValue:
            if let value = formRow.value as? NSNumber {
                fishEventLocation.setMinutes(value.doubleValue, component: CoordinateComponent.Latitude)
            }
            
        case LocationFormRow.LatitudeSeconds(fishEventLocation.type).tag.rawValue:
            if let value = formRow.value as? NSNumber {
                fishEventLocation.setSeconds(value.doubleValue, component: CoordinateComponent.Latitude)
            }
            
        case LocationFormRow.LongitudeDecimal.tag.rawValue:
            if let value = formRow.value as? NSNumber {
                fishEventLocation.longitude = Coordinate(decimal: value.doubleValue)
            }
            
        case LocationFormRow.LongitudeDegrees(fishEventLocation.type).tag.rawValue:
            if let value = formRow.value as? NSNumber {
                fishEventLocation.setDegrees(value.doubleValue, component: CoordinateComponent.Longitude)
            }
            
        case LocationFormRow.LongitudeMinutes(fishEventLocation.type).tag.rawValue:
            if let value = formRow.value as? NSNumber {
                fishEventLocation.setMinutes(value.doubleValue, component: CoordinateComponent.Longitude)
            }
            
        case LocationFormRow.LongitudeSconds(fishEventLocation.type).tag.rawValue:
            if let value = formRow.value as? NSNumber {
                fishEventLocation.setSeconds(value.doubleValue, component: CoordinateComponent.Longitude)
            }
            
        default:
            () // do nothing
            }
        }
    }
    
    // MARK: - Helpers
    
    private func createDecimalSections() {
        self.form.removeFormSectionAtIndex(1)
        self.form.removeFormSectionAtIndex(1)
        
        let latitudeSection  = XLFormSectionDescriptor.formSectionWithTitle("Latitude")
        let longitudeSection = XLFormSectionDescriptor.formSectionWithTitle("Longitude")
        
        var row = LocationFormRow.LatitudeDecimal.row
        row.value = (rowDescriptor?.value as! FishEventLocation).latitude.decimal
        latitudeSection.addFormRow(row)
        
        row = LocationFormRow.LongitudeDecimal.row
        row.value = (rowDescriptor?.value as! FishEventLocation).longitude.decimal
        longitudeSection.addFormRow(row)
        
        self.form.addFormSection(latitudeSection)
        self.form.addFormSection(longitudeSection)
    }
    
    private func createDegreeSections(gpsType: GpsType) -> (XLFormSectionDescriptor, XLFormSectionDescriptor) {
        self.form.removeFormSectionAtIndex(1)
        self.form.removeFormSectionAtIndex(1)
        
        let latitudeSection  = XLFormSectionDescriptor.formSectionWithTitle("Latitude")  
        let longitudeSection = XLFormSectionDescriptor.formSectionWithTitle("Longitude")
        
        let latitudeDegrees = (rowDescriptor?.value as! FishEventLocation).latitude.degrees
        let longitudeDegrees = (rowDescriptor?.value as! FishEventLocation).longitude.degrees
        
        var row = LocationFormRow.LatitudeDegrees(gpsType).row
        row.value = latitudeDegrees
        latitudeSection.addFormRow(row)
        
        row = LocationFormRow.LongitudeDegrees(gpsType).row
        row.value = longitudeDegrees
        longitudeSection.addFormRow(row)
        
        self.form.addFormSection(latitudeSection)
        self.form.addFormSection(longitudeSection)
        
        return (latitudeSection, longitudeSection)
    }
    
    private func createDegreeMinutesSections(gpsType: GpsType) -> (XLFormSectionDescriptor, XLFormSectionDescriptor) {
        let (latitudeSection, longitudeSection) = createDegreeSections(gpsType)
        
        var latitudeMinutes = (rowDescriptor?.value as! FishEventLocation).latitude.minutes
        var longitudeMinutes = (rowDescriptor?.value as! FishEventLocation).longitude.minutes
        
        switch gpsType {
        case .DM:
            latitudeMinutes  = Double(round(100 * latitudeMinutes)/100)
            longitudeMinutes = Double(round(100 * longitudeMinutes)/100)
            
        case .DMS:
            latitudeMinutes = floor(latitudeMinutes)
            longitudeMinutes = floor(longitudeMinutes)
            
        default:
            () // do nothing
            
        }
        
        var row = LocationFormRow.LatitudeMinutes(gpsType).row
        row.value = latitudeMinutes
        latitudeSection.addFormRow(row)
        
        row = LocationFormRow.LongitudeMinutes(gpsType).row
        row.value = longitudeMinutes
        longitudeSection.addFormRow(row)
        
        return (latitudeSection, longitudeSection)
    }
    
    private func createDegreeMinutesSecondsSections(gpsType: GpsType) {
        let (latitudeSection, longitudeSection) = createDegreeMinutesSections(gpsType)
        
        let latitudeSeconds = (rowDescriptor?.value as! FishEventLocation).latitude.seconds
        let longitudeSeconds = (rowDescriptor?.value as! FishEventLocation).longitude.seconds
        
        var row = LocationFormRow.LatitudeSeconds(gpsType).row
        row.value = Double(round(100 * latitudeSeconds)/100)
        latitudeSection.addFormRow(row)
        
        row = LocationFormRow.LongitudeSconds(gpsType).row
        row.value = Double(round(100 * longitudeSeconds)/100)
        longitudeSection.addFormRow(row)
    }
}
