//
//  DashboardPagerViewController.swift
//  taglouisiana
//
//  Created by Daniel Ward on 11/17/16.
//  Copyright © 2016 Daniel Ward. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class DashboardPagerViewController: ButtonBarPagerTabStripViewController {

    override func viewDidLoad() {
 

        settings.style.buttonBarBackgroundColor = Constants.UICOLOR_BLUE
        settings.style.buttonBarItemBackgroundColor = Constants.UICOLOR_BLUE
        settings.style.selectedBarBackgroundColor = Constants.UICOLOR_ORANGE
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewControllersForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        
        var controllers = [UIViewController]()
        
        if (AuthUtils.isSignedIn()) {
            let controller = storyboard.instantiateViewControllerWithIdentifier(Constants.STORYBOARD_ID_DASHBOARD_TABLE_VIEW_CONTROLLER) as! DashboardItemsTableViewController
            controller.dashboardType = DashboardType.Angler
            controller.pageTitle = (controller.dashboardType?.pageTitle)!
            controllers.append(controller)
        }
        
        let controller = storyboard.instantiateViewControllerWithIdentifier(Constants.STORYBOARD_ID_DASHBOARD_TABLE_VIEW_CONTROLLER) as! DashboardItemsTableViewController
        controller.dashboardType = DashboardType.Public
        controller.pageTitle =  (controller.dashboardType?.pageTitle)!
        controllers.append(controller)
        
        return controllers
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
