//
//  CapturesDetailViewController.swift
//  taglouisiana
//
//  Created by Daniel Ward on 3/27/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import UIKit
import MMMaterialDesignSpinner
import Kingfisher
import MagicalRecord
import MWPhotoBrowser
import SDWebImage
import XLPagerTabStrip

class CapturesDetailViewController: UIViewController, MWPhotoBrowserDelegate, IndicatorInfoProvider {
    
    //    @IBOutlet weak var mapContainer: UIView!
    
    var detailItem: FishEvent? {
        didSet {
            updateView()
        }
    }
    
    var shouldShowDate: Bool = true
    
    var dateFormatter = NSDateFormatter()
    
    var mapController: MapContainerViewController?
    var pagerController: CaptureDetailPager?
    
    @IBOutlet weak var speciesLabel: UILabel!
    @IBOutlet weak var lengthLabel: UILabel!
    @IBOutlet weak var conditionLabel: UILabel!
    @IBOutlet weak var hasRecapturesLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var speciesImage: UIImageView!
    @IBOutlet weak var spinner: MMMaterialDesignSpinner!
    
    @IBOutlet weak var detailsContainer: UIView!
    @IBOutlet weak var placeholderContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CapturesDetailViewController.managedObjectContextDidChange(_:)), name: NSManagedObjectContextDidSaveNotification, object: NSManagedObjectContext.defaultContext())
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CapturesDetailViewController.managedObjectContextDidChange(_:)), name: Constants.NOTIFICATION_DATA_UPDATE_FINISHED, object: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        
        dateFormatter.dateStyle = .MediumStyle
        
        if let view = spinner {
            view.lineWidth = 2.0
            view.tintColor = Constants.UICOLOR_ORANGE
            view.startAnimating()
        }
        
        if let imageView = speciesImage {
            imageView.layer.borderColor = Constants.UICOLOR_BLUE.CGColor
            imageView.layer.borderWidth = 2.0
        }
        
        updateView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK - IndicatorInfoProvider
    
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        
        var title = "Placeholder"
        
        if let capture = detailItem {
            title = dateFormatter.stringFromDate(capture.date)
        }
        
        return IndicatorInfo(title: title)
    }
    
    // MARK - Actions
    
    func editFishEventSelected(sender: AnyObject) {
        self.performSegueWithIdentifier(Constants.SEGUE_EDIT_FISH_EVENT, sender: self)
    }
    
    func timeLogEntrySelected(sender: AnyObject) {
        self.performSegueWithIdentifier(Constants.SEGUE_TIME_ON_WATER, sender: self)
    }
    
    func newFishEventSelected(sender: UIBarButtonItem) {
        let storyboard =  self.storyboard
        if let vc = storyboard?.instantiateViewControllerWithIdentifier("new fish event controller") {
            vc.modalPresentationStyle = .Popover
            vc.popoverPresentationController?.barButtonItem = sender
            self.presentViewController(vc, animated: true, completion: nil)
        }
    }
    
    func tagRequestSelected(sender: AnyObject) {
        self.performSegueWithIdentifier(Constants.SEGUE_REQUEST_TAGS, sender: self)
    }
    
    func listErrors(sender: AnyObject) {
        if let item = detailItem as? DraftCapture {
            let optionMenu  = UIAlertController(title: "Errors", message: item.errorMessageString(), preferredStyle: .Alert)
            let closeOption = UIAlertAction(title: "Dismiss", style: .Destructive, handler: nil)
            
            optionMenu.addAction(closeOption)
            
            self.presentViewController(optionMenu, animated: true, completion: nil)
        }
    }
    
    func showActions(sender: AnyObject) {
        if let item = detailItem {
            let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .ActionSheet)
            
            if let pendingItem = item as? DraftCapture {
                let editAction = UIAlertAction(title: "Edit", style: .Default, handler: {
                    (alert: UIAlertAction) -> Void in
                    
                    self.performSegueWithIdentifier(Constants.SEGUE_EDIT_FISH_EVENT, sender: self)
                })
                
                optionMenu.addAction(editAction)
                
                if pendingItem.statusEnum.tag == DraftCaptureStatusEnum.Error.tag {
                    
                    let errorsAction = UIAlertAction(title: "Show Errors", style: .Default, handler: {
                        (alert: UIAlertAction) -> Void in
                        self.listErrors(sender)
                    })
                    
                    optionMenu.addAction(errorsAction)
                }
                
            }
            
            if item.imagesForGallery().count > 0 {
                let imageAction = UIAlertAction(title: "Show Images", style: .Default, handler: {
                    (alert: UIAlertAction) -> Void in
                    let browser = MWPhotoBrowser(delegate: self)
                    browser.startOnGrid = true
                    self.navigationController?.pushViewController(browser, animated: true)
                })
                
                optionMenu.addAction(imageAction)
                
            }
            
            let copyAction = UIAlertAction(title: "Copy to New Tag", style: .Default, handler: {
                (alert: UIAlertAction) -> Void in
                self.performSegueWithIdentifier(Constants.SEGUE_COPY_FISH_EVENT, sender: self)
            })
            
            
            optionMenu.addAction(copyAction)
            
            if let item = detailItem {
                if let comments = item.comments {
                    if comments.length > 0 {
                        let action = UIAlertAction(title: "Show Comments", style: .Default, handler: {
                            (alert: UIAlertAction) -> Void in
                            
                            let commentDialog = UIAlertController(title: "Comments", message: comments, preferredStyle: .Alert)
                            
                            let closeAction = UIAlertAction(title: "Close", style: UIAlertActionStyle.Cancel, handler: nil)
                            
                            commentDialog.addAction(closeAction)
                            
                            self.presentViewController(commentDialog, animated: true, completion: nil)
                        })
                        
                        optionMenu.addAction(action)
                    }
                }
            }
            
            if let pendingItem = detailItem as? DraftCapture {
                let cancelAction = UIAlertAction(title: "Delete", style: .Destructive, handler: {
                    (alert: UIAlertAction) -> Void in
                    
                    let m = pendingItem.convertToEnteredDraftCapture()
                    m.shouldDelete = true
                    pendingItem.MR_deleteEntity()
                    
                    MagicalRecord.saveWithBlockAndWait({ (context) -> Void in
                        Api.PostData.execute()
                        
                        self.performSegueWithIdentifier(Constants.SEGUE_RETURN_TO_LIST, sender: self)
                    })
                })
                
                optionMenu.addAction(cancelAction)
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
                (alert: UIAlertAction) -> Void in
                // left blank
            })
            
            optionMenu.addAction(cancelAction)
                    
            self.presentViewController(optionMenu, animated: true, completion: nil)
        }
    }
    
    
    func showMoreActions(sender: AnyObject) {
        if let item = detailItem {
            let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .ActionSheet)
            
            if item.imagesForGallery().count > 0 {
                let imageAction = UIAlertAction(title: "Show Images", style: .Default, handler: { (alert: UIAlertAction) -> Void in
                    let browser = MWPhotoBrowser(delegate: self)
                    browser.startOnGrid = true
                    self.navigationController?.pushViewController(browser, animated: true)
                })
                
                optionMenu.addAction(imageAction)
                
            }
            
            let copyAction = UIAlertAction(title: "Copy to New Tag", style: .Default, handler: {
                (alert: UIAlertAction) -> Void in
                self.performSegueWithIdentifier(Constants.SEGUE_COPY_FISH_EVENT, sender: self)
            })
            
            
            optionMenu.addAction(copyAction)
            
            if let comments = item.comments {
                if comments.length > 0 {
                    let action = UIAlertAction(title: "Show Comments", style: .Default, handler: {
                        (alert: UIAlertAction) -> Void in
                        
                        let commentDialog = UIAlertController(title: "Comments", message: comments, preferredStyle: .Alert)
                        
                        let closeAction = UIAlertAction(title: "Close", style: UIAlertActionStyle.Cancel, handler: nil)
                        
                        commentDialog.addAction(closeAction)
                        
                        self.presentViewController(commentDialog, animated: true, completion: nil)
                    })
                    
                    optionMenu.addAction(action)
                }
            }
            
            if let pendingItem = detailItem as? DraftCapture {
                let cancelAction = UIAlertAction(title: "Delete", style: .Destructive, handler: {
                    (alert: UIAlertAction) -> Void in
                    
                    let m = pendingItem.convertToEnteredDraftCapture()
                    m.shouldDelete = true
                    pendingItem.MR_deleteEntity()
                    
                    MagicalRecord.saveWithBlockAndWait({ (context) -> Void in
                        Api.PostData.execute()
                        
                        self.performSegueWithIdentifier(Constants.SEGUE_RETURN_TO_LIST, sender: self)
                    })
                })
                
                optionMenu.addAction(cancelAction)
            }

            optionMenu.popoverPresentationController?.sourceView = sender.view
            optionMenu.popoverPresentationController?.sourceRect = sender.view.bounds
            
            self.presentViewController(optionMenu, animated: true, completion: nil)
        }

    }
    
    // MARK: MWPhotoBrowserDelegate Methods

    func numberOfPhotosInPhotoBrowser(photoBrowser: MWPhotoBrowser!) -> UInt {
        if let item = detailItem {
            return UInt(item.imagesForGallery().count)
        } else {
            return 0
        }
    }

    func photoBrowser(photoBrowser: MWPhotoBrowser!, photoAtIndex index: UInt) -> MWPhotoProtocol! {
        if let item = detailItem {
            let images = item.imagesForGallery()
            let count = UInt(images.count)
            if (index < count) {
                return images[Int(index)];
            }
        } else {
            return nil
        }
       
        return nil;
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == Constants.SEGUE_MAP_VIEW {
            mapController = segue.destinationViewController as? MapContainerViewController
        }
        
        if segue.identifier == Constants.SEGUE_EDIT_FISH_EVENT {
            
            if let item = detailItem as? DraftCapture {
                let controller = segue.destinationViewController as? UINavigationController
                let formController = controller?.topViewController as? FishEventFormViewController
                formController?.pendingCapture = item
            }
        }
        
        if segue.identifier == Constants.SEGUE_COPY_FISH_EVENT {
            if let item = detailItem {
                let controller = segue.destinationViewController as? UINavigationController
                let formController = controller?.topViewController as? FishEventFormViewController
                
                formController?.isCopying = true
                formController?.pendingCapture = item.MR_inContext((formController?.context)!)
            }
        }
        
        if segue.identifier == Constants.SEGUE_NEW_FISH_EVENT {
            
            //            if let controller = segue.destinationViewController as? NewFishEventTableViewController, item = detailItem {
            //               controller.pendingItem
            //            }
        }
    }
    
    // MARK - Actions
    
    func managedObjectContextDidChange(sender: NSNotification) {
        if let tagNumber = self.navigationItem.title {
            if let item: DraftCapture = DraftCapture.MR_findFirstByAttribute("tagNumber", withValue: tagNumber) {
                self.detailItem = item
                
            } else if let item: EnteredDraftCapture  = EnteredDraftCapture.MR_findFirstByAttribute("tagNumber", withValue: tagNumber) {
                self.detailItem = item
                
            } else {
                //                let alertController = UIAlertController(title: "Tag has been removed from device", message: "Please choose antoher tag", preferredStyle: .Alert)
                //                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }

    }
    
    // MARK - Helpers
    
    private func updateView() {
        
        /*
            Show or hide the items depending on whether we have an item to work with
        */
        if let containerView = detailsContainer {
            // hide the details container if the item is nil
            containerView.hidden = (detailItem == nil)
        }
        
//        if let containerView = placeholderContainer {
//            // hide the placeholder container if the item is not nil
//            containerView.hidden = (detailItem != nil)
//        }
//        
        
        /*
            Show more navigation mbar items if on an iPad
        */
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            var buttons: [UIBarButtonItem] = [UIBarButtonItem]()
            
            buttons.append(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Action, target: self, action: #selector(CapturesDetailViewController.showMoreActions(_:))))
            
            buttons.append(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: #selector(CapturesDetailViewController.newFishEventSelected(_:))))

            if let item = detailItem {                
                
                if let pendingItem = item as? DraftCapture {
                    buttons.append(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Edit, target: self, action: #selector(CapturesDetailViewController.editFishEventSelected(_:))))
                    
                    if pendingItem.statusEnum.tag == DraftCaptureStatusEnum.Error.tag {
                        buttons.append(UIBarButtonItem(image: UIImage(named: "Icon Error"), style: .Plain, target: self, action: #selector(CapturesDetailViewController.listErrors(_:))))
                    }
                }
                
 
            }
            
            if let pager = pagerController {
                pager.navigationItem.rightBarButtonItems = buttons
            } else {
                self.navigationItem.rightBarButtonItems = buttons
            }
        }
        
        /*
            Collapse all the bar button items into one action
         */
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            let barButton =  UIBarButtonItem(barButtonSystemItem: .Action, target: self, action: #selector(CapturesDetailViewController.showActions(_:)))
            
            if let pager = pagerController {
                pager.navigationItem.rightBarButtonItem = barButton
            }else {
                self.navigationItem.rightBarButtonItem = barButton
            }
        }
        
        /*
            Update the related capture items
         */
        if let capture = detailItem {
            
            if let containerView = placeholderContainer {
                containerView.hidden = true
            }
            
            self.navigationItem.title = capture.tagNumber
            
            if let image = speciesImage {
                image.kf_setImageWithURL(NSURL(string: capture.species.photoUrl))
            }
            
            if let label = speciesLabel {
                label.text = capture.species.commonName
            }
            
            if let label = dateLabel {
                label.text = dateFormatter.stringFromDate(capture.date)
                label.hidden = !shouldShowDate
            }
            
            if let label = hasRecapturesLabel {
                if let item = capture as? Capture {
                    label.hidden = (item.recaptureCount == 0)
                }
            }
            
            if let label = lengthLabel {
                label.text = capture.lengthText()
            }
            
            if let label = conditionLabel {
                if let condition = capture.fishCondition {
                    label.text = "Condition: \(condition.remoteId.stringValue)"
                }
            }
            
            if let label = locationLabel {
                if let location = capture.location() {
                    label.text = location.text
                } else {
                    label.text = "No Location provided"
                }
            }
            
            if let mapController = mapController {
                mapController.map.addFishEvent(capture)
            }
            
        } else {
            if let containerView = placeholderContainer {
                containerView.hidden = false
            }
        }
    }
    
}
