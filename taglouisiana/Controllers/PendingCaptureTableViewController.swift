//
//  PendingCaptureTableViewController.swift
//  taglouisiana
//
//  Created by Daniel Ward on 4/10/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import UIKit
import CoreData
import Crashlytics

enum Section {
    case EnteredCaptures
    case DraftCaptures
    case None
    
    var title: String {
        switch self {
        case .EnteredCaptures:
            return "Syncing Pending Captures"
        case .DraftCaptures:
            return "Synced Pending Captures"
        case .None:
            return ""
        }
    }
}

class PendingCaptureTableViewController: UITableViewController {
    
    var detailViewController: CapturesDetailViewController? = nil
    
    private var enteredCaptures: [EnteredDraftCapture]?
    private var draftCaptures: [DraftCapture]?
    
    private var firstSection: Section  = .None
    private var secondSection: Section = .None
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // do some extra configuration for when we are on an iPad
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            self.navigationController?.navigationItem.title = "List"
            self.clearsSelectionOnViewWillAppear = false
            self.preferredContentSize = CGSize(width: 320.0, height: 551.0)
            
            self.navigationItem.rightBarButtonItem = nil
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // If the detail view is visible, then save the reference
        
        if let split = self.splitViewController {
            split.presentsWithGesture = false
            if let navController = split.viewControllers[split.viewControllers.count-1] as? UINavigationController {
                self.detailViewController = navController.topViewController as? CapturesDetailViewController
            }
        }
        
        // configure the UIRefreshControl
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.backgroundColor = Constants.UICOLOR_ORANGE
        self.refreshControl?.tintColor = UIColor.whiteColor()
        self.refreshControl?.addTarget(self, action: #selector(PendingCaptureTableViewController.refreshAction(_:)), forControlEvents: UIControlEvents.ValueChanged)
        tableView.addSubview(self.refreshControl!);
        
        // endable the edit button
        
        self.navigationItem.leftBarButtonItem = self.editButtonItem()
        
        // add the observers
       
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PendingCaptureTableViewController.forceSyncStarted(_:)), name: Constants.NOTIFICATION_FORCE_SYNC_STARTED, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PendingCaptureTableViewController.forceSyncFinished(_:)), name: Constants.NOTIFICATION_FORCE_SYNC_FINISHED, object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PendingCaptureTableViewController.managedObjectContextDidChange(_:)), name: Constants.NOTIFICATION_DATA_UPDATE_FINISHED, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PendingCaptureTableViewController.managedObjectContextDidChange(_:)), name: NSManagedObjectContextObjectsDidChangeNotification, object: NSManagedObjectContext.defaultContext())
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        self.refreshData(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Remove the observers
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: Constants.NOTIFICATION_FORCE_SYNC_STARTED, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: Constants.NOTIFICATION_FORCE_SYNC_FINISHED, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: Constants.NOTIFICATION_DATA_UPDATE_FINISHED, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: NSManagedObjectContextObjectsDidChangeNotification, object: NSManagedObjectContext.defaultContext())
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        // start the section count at 0
        var sections = 0;
        
        // if there are EnteredPendingCaptures, then the first section
        // will contain those items
        
        if let items = enteredCaptures {
            if items.count > 0 {
                sections += 1
                firstSection = .EnteredCaptures
            }
        }
        
        // if thre are DraftCaptures, the we need to
        // add a section for them as well
        
        if let items = draftCaptures {
            if items.count > 0 {
                sections += 1
                
                // if there is not first section, then we will set
                // the draft captures as the first
                if firstSection == .None {
                    firstSection = .DraftCaptures
                }
                
                // if there is a first section, then set the
                // draft captures as the second section
                else {
                    secondSection = .DraftCaptures
                }
            }
        }
        
        // configure the view for when the list is empty, if needed
        
        if sections != 0 {
            self.tableView.backgroundView = nil
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.SingleLine
            
        } else {
            
            // create the label to hold the empty list message
            let label = UILabel(frame: CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height))
            
            label.text = "No data is currently available. Please pull down to refresh."
            label.textColor = UIColor.blackColor()
            label.numberOfLines = 0
            label.textAlignment = NSTextAlignment.Center
            label.sizeToFit()
            
            self.tableView.backgroundView = label
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        }
        
        return sections
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            switch firstSection {
            case .EnteredCaptures:
                return enteredCaptures!.count
            case .DraftCaptures:
                return draftCaptures!.count
            case .None:
                return 0
            }
        } else {
            return draftCaptures!.count
        }
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let
            cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as? PendingCaptureTableViewCell,
            let item = itemForIndexPath(indexPath)
        {
            
            /*
                TODO: remove if this is no longer a problem
            */
            Crashlytics.sharedInstance().setObjectValue(item.toDictionary(), forKey: "pending capture item")
            
            //------------
            
            cell.tagNumber.text = item.tagNumber
            cell.date.text = item.date.USDate
            cell.speciesName.text = item.species.commonName
            
            cell.editingTimeLeft.hidden = item.errorMessageString().isEmpty ? false : true
            
            if let expires = item.expiresAt {
                let offset = expires.offsetFrom(NSDate())
                
                if offset.isEmpty {
                    cell.editingTimeLeft.text = "Editing has expired"
                } else {
                    cell.editingTimeLeft.text = "Can edit for " + expires.offsetFrom(NSDate())
                }
            }
            
            
            if (item is EnteredDraftCapture) {
                cell.statusIcon.hidden = true
                cell.hasImagesIcon.hidden = (item as! EnteredDraftCapture).attachedImages.count == 0    
            } else {
                cell.statusIcon.hidden = false
                cell.statusIcon.image = item.statusEnum.icon
                cell.hasImagesIcon.hidden = item.images.count == 0
            }
            
            return cell
        }

        return tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! PendingCaptureTableViewCell
    }

    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            switch firstSection {
            case .EnteredCaptures:
                return Section.EnteredCaptures.title
            case .DraftCaptures:
                return nil
            case .None:
                return nil
            }
        } else {
            return Section.DraftCaptures.title
        }
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete  {
            let item: DraftCapture? = itemForIndexPath(indexPath)
            
            if item is EnteredDraftCapture {
                item?.MR_deleteEntity()
                NSManagedObjectContext.defaultContext().MR_saveToPersistentStoreAndWait()
            
            } else {
                item?.shouldDelete = true
                NSManagedObjectContext.defaultContext().MR_saveToPersistentStoreAndWait()

            }
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        if segue.identifier == Constants.SEGUE_SHOW_CAPTURE_DETAIL {
            let navigationController = segue.destinationViewController as! UINavigationController
            let controller = navigationController.topViewController as! CapturesDetailViewController
            
            if let indexPath = self.tableView.indexPathForSelectedRow {
                controller.detailItem = itemForIndexPath(indexPath)
            }
            
            self.splitViewController?.toggleMasterView()
            
            // add the left nav item so we can show the list again
            controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
            controller.navigationItem.leftItemsSupplementBackButton = true
        }
    }

    
    // MARK - Actions
    
    func managedObjectContextDidChange(sender: NSNotification) {
        self.refreshData()
    }
    
    func refreshAction(sender: AnyObject) {
        Utils.syncData({
            dispatch_async(dispatch_get_main_queue(),{
                self.refreshData()
            });
        })
    }
    
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue) {
        if segue.identifier == Constants.SEGUE_RETURN_TO_LIST {
           
            self.refreshData(true)
        }
    }
    
    // MARK - Helpers
    
    private func refreshData(updateDetail: Bool = true) {
        
        firstSection = .None
        secondSection = .None
        
        enteredCaptures = EnteredDraftCapture.MR_findAllSortedBy("date", ascending: false, withPredicate: NSPredicate(format: "shouldDelete == NO", argumentArray: [])) as? [EnteredDraftCapture]
        draftCaptures = DraftCapture.findAllWhereNotDelted() as [DraftCapture]

        self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
        
//        dispatch_async(dispatch_get_main_queue(),{
//            if (self.enteredCaptures!.count + self.draftCaptures!.count) > 0 && updateDetail {
//                self.detailViewController?.detailItem = nil
//            }
//
////            // uncomment to update the detail view with the first item in the list
////            if self.detailViewController?.detailItem == nil {
////
////                if let item = self.enteredCaptures?.first {
////                    self.detailViewController?.detailItem = item
////                } else {
////                    self.detailViewController?.detailItem = self.draftCaptures?.first
////                }
////            }
//
//            self.tableView.reloadData()
//            self.refreshControl?.endRefreshing()
//        })
    }
    
    private func itemForIndexPath(indexPath: NSIndexPath) -> DraftCapture? {
        if indexPath.section == 0 {
            switch firstSection {
            case .EnteredCaptures:
                return enteredCaptures?[indexPath.row]
            case .DraftCaptures:
                return draftCaptures?[indexPath.row]
            default:
                return nil
            }
        } else {
            return draftCaptures?[indexPath.row]
        }
    }
    
    func forceSyncStarted(sender: NSNotification) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: NSManagedObjectContextObjectsDidChangeNotification, object: nil)
    }
    
    func forceSyncFinished(sender: NSNotification) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: NSManagedObjectContextObjectsDidChangeNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PendingCaptureTableViewController.managedObjectContextDidChange(_:)), name: NSManagedObjectContextDidSaveNotification, object: NSManagedObjectContext.defaultContext())
        
        self.detailViewController?.detailItem = nil
        self.refreshData()
    }
}
