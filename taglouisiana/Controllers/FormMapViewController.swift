//
//  FormMapViewController.swift
//  taglouisiana
//
//  Created by Daniel Ward on 4/6/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import UIKit
import XLForm
import MapKit

class FormMapViewController: UIViewController, XLFormRowDescriptorViewController, MKMapViewDelegate {

    var rowDescriptor: XLFormRowDescriptor?
    
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addTouchRecognizerToMapView()
        
        // add the bar button that will let the user enter their location in manually
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Manual", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(FormMapViewController.enterManualLength(_:)))
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
     
        // if there row descriptor has a value, we need to update the map
        if let row = rowDescriptor {
            if let value = row.value as? FishEventLocation {
                
                // if we have a (0,0) locaiton, default a view of louisiana
                if value.latitude.decimal == 0.0 {
                    let region = MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(30.2233, -90.87), 1000000, 1000000)
                    mapView.setRegion(region, animated: false)
                    
                } else {
                    // remove all the annotations
                    mapView.removeAnnotations(mapView.annotations)
                    // add the annotation for this event
                    mapView.addFishEventLocation(value)
                    
                    self.navigationItem.title = value.text
                }
            }
        }
    }
    
    // MARK: - MKMapViewDelegate
    
    func mapView(mapView: MKMapView, didUpdateUserLocation userLocation: MKUserLocation) {
        if let row = rowDescriptor {
            if let _ = row.value as? FishEventLocation {
                // there is a valid location, so we can ignore the update
            } else {
                // create a fish event location from the userLocation
                let fishEventLocation = FishEventLocation(latitude: userLocation.location!.coordinate.latitude, longitude: userLocation.location!.coordinate.longitude, type: GpsType.D)
                
                // set the value of the row
                rowDescriptor?.value = fishEventLocation
                // update the map
                mapView.addFishEventLocation(fishEventLocation)
            }
        }
    }
    
    // MARK: - Actions
    
    func enterManualLength(sender: UIBarButtonItem) {
        self.performSegueWithIdentifier(Constants.SEGUE_ENTER_MANUAL_LOCATION, sender: self)
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == Constants.SEGUE_ENTER_MANUAL_LOCATION {
            let controller = segue.destinationViewController as! ManualLocationFormViewController
            controller.rowDescriptor = self.rowDescriptor
        }
    }
    
    // Mark: - Actions
    
    func mapTouched(sender: UITapGestureRecognizer) {
        let touchPoint = sender.locationInView(mapView)
        let touchCoord: CLLocationCoordinate2D = mapView.convertPoint(touchPoint, toCoordinateFromView: mapView)
        
        var location: FishEventLocation
        
        if let value = rowDescriptor?.value as? FishEventLocation {
            location = FishEventLocation(latitude: touchCoord.latitude, longitude: touchCoord.longitude, type: value.type)
        } else {
            location = FishEventLocation(latitude: touchCoord.latitude, longitude: touchCoord.longitude, type: GpsType.D)
        }
        
        rowDescriptor?.value = location
        
        self.navigationItem.title = location.text
        mapView.removeAnnotations(mapView.annotations)
        
        mapView.addFishEventLocation(location, title: location.text, mileSpan: 0.0, moveToLocation: false)
    }
    
    // MARK: - Helpers
    
    private func addTouchRecognizerToMapView() {
        let touchRecognizer = UITapGestureRecognizer(target: self, action: #selector(FormMapViewController.mapTouched(_:)))
        touchRecognizer.numberOfTapsRequired = 1
        self.mapView.addGestureRecognizer(touchRecognizer)
    }
}
