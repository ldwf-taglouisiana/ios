//
//  CaptureTableViewCell.swift
//  taglouisiana
//
//  Created by Daniel Ward on 3/26/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import UIKit

class CaptureTableViewCell: UITableViewCell {
    
    @IBOutlet weak var tagNumberLabel: UILabel!
    @IBOutlet weak var captureDateLabel: UILabel!
    @IBOutlet weak var speciesName: UILabel!
    @IBOutlet weak var hasRecaptures: UILabel!
    @IBOutlet weak var hasImagesIcon: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
