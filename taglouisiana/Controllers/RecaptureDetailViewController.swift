//
//  RecaptureDetailViewController.swift
//  taglouisiana
//
//  Created by Daniel Ward on 11/21/16.
//  Copyright © 2016 Daniel Ward. All rights reserved.
//

import UIKit
import SwiftyJSON
import XLPagerTabStrip
import MapKit
import GEOSwift

class RecaptureDetailViewController: UIViewController, IndicatorInfoProvider {
    
    var jsonData: JSON? {
        didSet {
            updateView()
        }
    }
    
     var mapController: MapContainerViewController?

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var anglerName: UILabel!
    @IBOutlet weak var lengthValue: UILabel!
    @IBOutlet weak var dsitanceTraveled: UILabel!
    @IBOutlet weak var disposition: UILabel!
    @IBOutlet weak var locationDescription: UITextView!
    @IBOutlet weak var lcoationStatus: UILabel!
    
    var dateFormatter: NSDateFormatter {
        let formatter = NSDateFormatter()
        formatter.dateStyle = .MediumStyle
        formatter.timeStyle = .NoStyle
        formatter.timeZone = NSTimeZone.defaultTimeZone()
        
        return formatter
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        updateView()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK - IndicatorInfoProvider
    
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        
        var title = "Placeholder"
        
        if let json = jsonData, let epochTime = json["capture_date_epoch"].double {
            let date = NSDate(timeIntervalSince1970: epochTime)
            let item = dateFormatter.stringFromDate(date)
            title = item
        }
        
        return IndicatorInfo(title: title)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == Constants.SEGUE_MAP_VIEW {
            mapController = segue.destinationViewController as? MapContainerViewController
        }
    }
    
    // MARK: - Helpers
    
    private func updateView() {
        if let json = jsonData {
            
            if let label = dateLabel {
                label.text = dateFormatter.stringFromDate(NSDate(timeIntervalSince1970: json["capture_date_epoch"].doubleValue))
            }
            
            if let label = anglerName {
                label.text = "Angler: \(json["angler_name"].stringValue)"
            }
            
            if let label = lengthValue {
                if let length = json["length"].double {
                    label.text = "Length: \(length) in."
                    
                } else if let length = json["length"].string {
                    label.text = "Length: \(length) in."
                }
                else if let remoteId = json["length_range"].number, let lengthItem = SpeciesLength.MR_findFirstByAttribute("remoteId", withValue: remoteId) as? SpeciesLength {
                    label.text = "Length: \(lengthItem.detail) in."
                }
            }
            
            if let label = disposition, let value = json["recapture_disposition"].string {
                label.text = "Disposition: \(value)"
            }
            
            if let label = dsitanceTraveled, let value = json["distance_traveled_meters"].double {
                let distanceInMiles = value * 0.000621371
                label.text = "Dist: \(String(format: "%.2f", distanceInMiles)) mi"
            }
            
            if let label = locationDescription, let value = json["location_description"].string {
                label.text = value
            }
            
            if let label = lcoationStatus, let gpsType = json["entered_gps_type"].string {
                if gpsType == "NOGPS" {
                    label.text = "No location provided"
                }
            }
            
            if  let geoJson = json["geo_json"].dictionaryObject,
                let geometries = Geometry.fromGeoJSONDictionary(geoJson),
                let map = mapController?.map,
                let label = lcoationStatus
            {
                
                if (geometries.count > 0) {
                    for geometry in geometries {
                        
                        if let geom = geometry as? Polygon,
                        let annotation = geom.mapShape() as? MKPolygon {
                            map.addOverlay(annotation)
                            
                            let rect = map.overlays.reduce(annotation.boundingMapRect, combine: {MKMapRectUnion($0, $1.boundingMapRect)})
                            map.setVisibleMapRect(rect, edgePadding: UIEdgeInsets(top: Constants.UI_MAP_INSET, left: Constants.UI_MAP_INSET, bottom: Constants.UI_MAP_INSET, right: Constants.UI_MAP_INSET), animated: false)
                            
                            label.text = "Location withheld"
                            
                        } else if let geom = geometry as? Waypoint,
                            let annotation = geom.mapShape() as? MKPointAnnotation
                        {
//                            map.addAnnotation(annotation)
                            map.addFishEventLocation(
                                FishEventLocation(
                                    latitude: annotation.coordinate.latitude,
                                    longitude: annotation.coordinate.longitude,
                                    type: GpsType.D
                                ),
                                title:  "",
                                mileSpan: 5.0,
                                moveToLocation: true
                            )
                            
                            label.text = "\(json["latitude_formatted"].stringValue), \(json["longitude_formatted"].stringValue)"
                            
                        } else {
                            // do nothing
                        }
                    }
                }
            }
            
        }
    }
}
