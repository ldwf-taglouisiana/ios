//
//  MapContainerViewController.swift
//  taglouisiana
//
//  Created by Daniel Ward on 3/31/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import UIKit
import MapKit

class MapContainerViewController: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var map: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        map.showsUserLocation = true
        map.delegate = self

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        if (overlay is MKPolygon) {
            let polygonRender = MKPolygonRenderer(overlay: overlay)
            polygonRender.strokeColor = UIColor.blueColor()
            polygonRender.fillColor = UIColor(colorLiteralRed: 0, green: 0, blue: 1.0, alpha: 0.2)
            return polygonRender
        } else {
            return MKOverlayRenderer()
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
