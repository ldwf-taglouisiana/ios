//
//  ForceSyncViewController.swift
//  taglouisiana
//
//  Created by Daniel Ward on 5/6/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import UIKit
import Alamofire
import MMMaterialDesignSpinner

class ForceSyncViewController: UIViewController {
    
    @IBOutlet weak var spinner: MMMaterialDesignSpinner!
    
    let queue = TaskQueue()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Utils.delay(1.0, closure: {
            self.spinner!.lineWidth = 2.5
            self.spinner!.tintColor = Constants.UICOLOR_ORANGE
            self.spinner!.hidden = false
            self.spinner!.startAnimating()
        })
        
        performResync()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func performResync() {
        
        NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_FORCE_SYNC_STARTED, object: nil)
        
        queue.tasks +=~ {
            
            // cancel the queue execution
            Api.datastoreQueue.cancel()
            Api.networkQueue.cancel()
            
            // do a small sleep to catch any other entries into the queues
            sleep(2)
            
            // cancel the queue execution
            Api.datastoreQueue.cancel()
            Api.networkQueue.cancel()

            // do a small sleep to catch any other entries into the queues
            sleep(2)
            
            // empty out the queues
            Api.datastoreQueue.removeAll()
            Api.networkQueue.removeAll()
        
            if Utils.rebuildDatastore() {
                
                Api.datastoreQueue.restart()
                Api.networkQueue.restart()

                // do a small sleep to catch any other entries into the queues
                sleep(2)
                
                Utils.syncData({
                    dispatch_async(dispatch_get_main_queue(), {
                        NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_FORCE_SYNC_FINISHED, object: nil)
                        
                        Utils.delay(3, closure: {
                            self.dismissViewControllerAnimated(true, completion: nil)
                        })
                    })
                })
                
            } else {
                dispatch_async(dispatch_get_main_queue(), {
                    // the file was not deleted, let the user know there was a problem.
                    let alertController = UIAlertController(title: "Could not resync", message: "There was an error resyncing.\nPlease try again later.", preferredStyle: .Alert)
                    let OKAction = UIAlertAction(title: "Close", style: .Default) { (action) in
                        // do nothing
                    }
                    alertController.addAction(OKAction)
                    self.presentViewController(alertController, animated: true, completion: nil)
                })
            }
        }
//
        self.queue.run()
    }
}
