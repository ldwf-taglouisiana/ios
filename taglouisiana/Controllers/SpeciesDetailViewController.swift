//
//  SpeciesDetailViewController.swift
//  taglouisiana
//
//  Created by Daniel Ward on 3/27/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import UIKit
import Kingfisher

class SpeciesDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {

    var detailItem: Species? {
        didSet {
            updateView()
        }
    }
    
    var items: [[String:String]] = [[String: String]]()
    
    @IBOutlet weak var speciesImage: UIImageView!
    @IBOutlet weak var properNameLabel: UILabel!
    @IBOutlet weak var fishFamilyLabel: UILabel!    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tableView.estimatedRowHeight = 33
        tableView.rowHeight = UITableViewAutomaticDimension
        
        updateView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK - UITextViewDelegate
    
    func textViewDidChange(textView: UITextView) {
        // http://candycode.io/self-sizing-uitextview-in-a-uitableview-using-auto-layout-like-reminders-app/
        let currentOffset = tableView.contentOffset
        UIView.setAnimationsEnabled(false)
        tableView.beginUpdates()
        tableView.endUpdates()
        UIView.setAnimationsEnabled(true)
        tableView.setContentOffset(currentOffset, animated: false)
    }
    
    // MARK - UITableViewDelegate
    
    /* Not Implemented Yet */
    
    // MARK - UITableViewDataSource
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return items[section].keys.first!
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! SpeciesDetailInfoTableViewCell
        cell.textViewElement.delegate = self
        cell.textViewElement.text = getItemAtIndexPath(indexPath).values.first
        return cell
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return items.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    // MARK - Helpers
    
    private func getItemAtIndexPath(indexPath: NSIndexPath) -> [String:String] {
        return items[indexPath.section]
    }
    
    func updateView() {
        if let item = detailItem {
            
            self.navigationItem.title = item.commonName
            
            if let image = speciesImage {
                if let path = item.photoUrl as String? {
                    image.kf_setImageWithURL(NSURL(string: path))
                }
            }
            
            if let label = properNameLabel {
                if let text = item.properName as String? {
                    label.text = text
                } else {
                    label.text = "N/A"
                }
            }
            
            if let label = fishFamilyLabel {
                if let text = item.family as String? {
                    label.text = text
                } else {
                    label.text = "N/A"
                }
            }
            
            items = [[String: String]]()
            
            items.append([ "Other names" : item.otherName == nil ? "N/A" : item.otherName!.html2String ])
            items.append([ "Habitat" : item.habitat == nil ? "N/A" : item.habitat!.html2String ])
            items.append([ "Size Description" : item.size == nil ? "N/A" : item.size!.html2String ])
            items.append([ "Food Value" : item.foodValue == nil ? "N/A" : item.foodValue!.html2String ])
            items.append([ "Description" : item.info == nil ? "N/A" : item.info!.html2String ])

            tableView?.reloadData()
        }
    }

}
