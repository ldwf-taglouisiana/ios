//
//  CaptureDetailPager.swift
//  taglouisiana
//
//  Created by Daniel Ward on 11/21/16.
//  Copyright © 2016 Daniel Ward. All rights reserved.
//

import UIKit
import SwiftyJSON
import XLPagerTabStrip

class CaptureDetailPager: ButtonBarPagerTabStripViewController {
    
    var captureItem : Capture? {
        didSet {
            self.title = self.captureItem?.tagNumber
            self.fetchHistory()
            self.reloadPagerTabStripView()
        }
    }
    
    var reacptureItems : [JSON] = [JSON]() {
        didSet {
            self.reloadPagerTabStripView()
        }
    }
    
    override func viewDidLoad() {
        
        settings.style.buttonBarBackgroundColor = Constants.UICOLOR_BLUE
        settings.style.buttonBarItemBackgroundColor = Constants.UICOLOR_BLUE
        settings.style.selectedBarBackgroundColor = Constants.UICOLOR_ORANGE
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewControllersForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        
        var controllers = [UIViewController]()
        
        let controller = storyboard.instantiateViewControllerWithIdentifier(Constants.STORYBOARD_ID_CAPTURE_DETAIL_VIEW_CONTROLLER) as! CapturesDetailViewController
        controller.pagerController = self
        controller.detailItem = captureItem
        controller.shouldShowDate = false
        controllers.append(controller)
        
        for recaptureitem in reacptureItems {
            let controller = storyboard.instantiateViewControllerWithIdentifier(Constants.STORYBOARD_ID_RECAPTURE_DETAIL_VIEW_CONTROLLER) as! RecaptureDetailViewController
            controller.jsonData = recaptureitem
            controllers.append(controller)
        }
        
        return controllers
    }
    
    private func fetchHistory() {
        
        if let item = captureItem {
            Api.FetchTagHistory([ "id" : item.remoteId ]).execute({
                json in
                self.reacptureItems = json["recaptures"].arrayValue
            })
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
