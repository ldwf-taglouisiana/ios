//
//  DraftCaptureStatusEnum.swift
//  taglouisiana
//
//  Created by Daniel Ward on 4/23/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//
import UIKit

enum DraftCaptureStatusEnum {
    case Error
    case Ok
    
    var tag: Int {
        switch self {
        case .Error:
            return 1
            
        case .Ok:
            return 2
        }
    }
    
    var color: UIColor {
        switch self {
        case .Error:
            // red
            return UIColor(rgba: "#ff3300")
            
        case .Ok:
            // green
            return UIColor(rgba: "#33CC33")
        }
    }
    
    var icon: UIImage {
        switch self {
        case .Error:
            return UIImage(named: "Status Error")!.tintWithColor(self.color)
        
        case .Ok:
            return UIImage(named: "Status Ok")!.tintWithColor(self.color)
        }
    }
    
//    var message: String {
//        switch self {
//        case .Error:
//            return "Pending capture has some errors"
//            
//        case .Ok(let date):
//            return  "" //"Draft has no errors and can be edited for \(NSDate().timeIntervalSinceDate(date).toTimeString())"
//            
//        }
//    }
}
