//
// Created by Daniel Ward on 3/23/15.
// Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import Foundation
import MagicalRecord
import Crashlytics
import MessageUI
import SDWebImage

class Utils {

    /**
        Runs a closure after a given number of seconds

        - Parameters:
            - formatString: An Obj-C style format string
            - ap: A `CVaListPointer` list for the substitution values for the `formatString. (Optional, default is an empty list)
    */
    class func log(formatString: String, _ ap: CVaListPointer = getVaList([])) {
        CLSLogv(formatString, ap)
    }

    /**
        Runs a closure after a given number of seconds

        - Parameters:
            - delay: number of seconds to wait to execute
            - closure: closure to execute after the specified time has expired
    */
    class func delay(delay: Double, closure: ()->()) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW,Int64(delay * Double(NSEC_PER_SEC))), dispatch_get_main_queue(), closure)
    }

    /**
        Get the generated API token that is used to authenticate the OAuth Client, not the user

        - Returns: The token to be sent in the params using the Constants.API_TOKEN_PARAMETER as the key
    */
    class func getApiToken() -> String? {
        let defaults = NSUserDefaults.standardUserDefaults()
        return defaults.objectForKey(Constants.DEFAULTS_KEY_API_TOKEN) as! String?
    }

    /**
        Sets the `Constants.DEFAULTS_KEY_FIRST_SYNC` key in `NSUserDefaults`
     */
    class func setFirstSyncCompleted() {
        let defaults = NSUserDefaults.standardUserDefaults()

        defaults.setBool(true, forKey: Constants.DEFAULTS_KEY_FIRST_SYNC)
        defaults.synchronize()
    }

    /**
        Returns whether a sync has been performed in the past.

        - Returns: If `true` then this is the first time a sync action was triggered
     */
    class func isFirstSync() -> Bool {
        let defaults = NSUserDefaults.standardUserDefaults()
        return defaults.boolForKey(Constants.DEFAULTS_KEY_FIRST_SYNC)
    }

    /**
       Deletes the old `sqlite` store and creates a new store. Effectively starting the
       app data fresh.

       Also clears out the store etags bases on the `Api.allValues` and each's `etagStorageKey`
       `NSUserDefaults` storage key.

       - Returns: If `true` then the store was rebuilt, else the previous store should have been left intact
    */
    class func rebuildDatastore() -> Bool {
        /*
            Cleaning up the datastore, there were some changes in iOS 9, that made the
            old way cause a crash.
            http://stackoverflow.com/a/18013281/2614663
        */
        let url = NSPersistentStore.MR_fileURLForStoreNameIfExistsOnDisk(Constants.STORE_NAME)
        let walURL = url.URLByDeletingPathExtension?.URLByAppendingPathExtension("sqlite-wal")
        let shmURL = url.URLByDeletingPathExtension?.URLByAppendingPathExtension("sqlite-shm")

        // cancel the queue execution
        Api.datastoreQueue.cancel()
        Api.networkQueue.cancel()

        // empty out the queues
        Api.datastoreQueue.removeAll()
        Api.networkQueue.removeAll()

        // cleanup any open contexts, if any
        MagicalRecord.cleanUp()

        // remove all the stored etags
        let defaults = NSUserDefaults.standardUserDefaults()
        Api.allValues.forEach( { api in
            if let etag = api.etagStorageKey {
                defaults.removeObjectForKey(etag)
                defaults.synchronize()
            }
        })

        // delete the sql store. Save the error and the success
        var removeError: NSError?
        do {
            try NSFileManager.defaultManager().removeItemAtURL(url)
            try NSFileManager.defaultManager().removeItemAtURL(walURL!)
            try NSFileManager.defaultManager().removeItemAtURL(shmURL!)
        } catch let error as NSError {
            removeError = error
        }

        // if there was not an error then we can create a new sqlite store
        if removeError == nil {
            MagicalRecord.setupAutoMigratingStackWithSQLiteStoreNamed(Constants.STORE_NAME)
        } else {
            Utils.log("An error has occured while deleting %@", getVaList([Constants.STORE_NAME]))
            Utils.log("Error description: %@", getVaList([removeError!.description]))
        }

        // if the error was not set, then the deletion was successful
        return removeError == nil
    }

    /**
        Creates a `UIAlertViewController` that is is configured based on the given
        input parameters

        - Parameters:
            - title: The title for the dialog.
            - message: The message to display in the alert box
            - okTitle: The title of the `ok` button (`"OK"` by default)
            - okAction: The block to execute when `ok` button is pressed. If `nil`, then the button is not added. (default `nil`)
            - cancelTitle: The block to execute when `cancel` button is pressed (`"Cancel"` by default)
            - cancelAction: The block to execute when `cancel` button is pressed. If `nil`, then the button is not added. (default `nil`)

        - Returns: A `UIAlertViewController` that is configured with the given parameters.
   */
    class func createAlertDialog(title title: String, message: String, okTitle: String = "Ok", okAction: ((UIAlertAction) -> Void)? = nil, cancelTitle: String = "Cancel", cancelAction: ((UIAlertAction) -> Void)? = nil) -> UIAlertController {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)

        if cancelAction != nil {
            let cancelAction = UIAlertAction(title: cancelTitle, style: .Cancel, handler: cancelAction)
            alertController.addAction(cancelAction)
        }

        if okAction != nil {
            let OKAction = UIAlertAction(title: okTitle, style: .Default, handler: okAction)
            alertController.addAction(OKAction)
        }

        return alertController
    }

    /**
        Creates a `MFMailComposeViewController` that is configured to
        send email to `Constanst.CONTACT_EMAIL_ADDRESS`.

        - Parameter delegate: The `MFMailComposeViewControllerDelegate` to call back to.
    */
    class func createEmailViewController(delegate delegate: MFMailComposeViewControllerDelegate) -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = delegate

        mailComposerVC.setSubject("Tag Lousiana - Feedback")
        mailComposerVC.setMessageBody("", isHTML: false)
        mailComposerVC.setToRecipients([Constants.CONTACT_EMAIL_ADDRESS])

        return mailComposerVC
    }

    /**
        Sync all the data with the remote server and this client.

        If the user is signed in, then the protected data will also be pulled down.
        Any pending data to post to the server will also be uploaded if the user is
        signed in.

        - Parameter completed: A closure to execute when the sync is finished.
    */
    class func syncData(completed: (Void) -> (Void) = {}) {
        if (AuthUtils.isSignedIn()) {
            Api.FetchPublicData.execute(false, completion: {
                Api.FetchUserData.execute(false, completion: {
                    Api.PostData.execute(completion: completed)
                })
            })
        } else {
            Api.FetchPublicData.execute(completion: completed)
        }
    }
    
    class func setupSDWebImageManager() {
        let manager = SDWebImageManager.sharedManager().imageDownloader
        if let token = AuthUtils.getOauthToken() {
            manager.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        }
    }
}
