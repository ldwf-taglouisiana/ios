//
//  FishEventLocation.swift
//  taglouisiana
//
//  Created by Daniel Ward on 3/31/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import XLForm

enum GpsType: String {
    case D = "D"
    case DM = "DM"
    case DMS = "DMS"
    case NoGPS = "NOGPS"
    
    static let pickerValues = [
        D.formOptionObject,
        DM.formOptionObject,
        DMS.formOptionObject
    ]
    
    var name: String {
        switch self {
        case .D:
            return "Decimal"
        case .DM:
            return "Degrees Minutes"
        case .DMS:
            return "Degree Minutes Seconds"
        case .NoGPS:
            return "No GPS"
        }
    }
    
    var formOptionObject: XLFormOptionsObject {
        return XLFormOptionsObject(value: self.rawValue, displayText: self.name)
    }
}

struct Coordinate {
    var decimal: Double
    
    var degrees: Double {
        // if the value is 0.0, then return 0.0. Otherwise the calculation will 
        // cause a NaN to be returned.
        if decimal == 0.0 {
            return 0.0
        } else {
            // the division at the end makes sure that we don't lose the sign
            return floor(fabs(decimal)) * (decimal / fabs(decimal))
        }
    }
    
    var minutes: Double {
        return ((fabs(decimal) - floor(fabs(decimal))) * 60.0)
    }
    
    var seconds: Double {
        return ((fabs(minutes) - floor(fabs(minutes))) * 60.0)
    }
    
    func textForType(type: GpsType) -> String {
        switch type {
        case .D:
            let sign = fabs(decimal)/decimal
            return decimal == 0.0 ? "Unknown" : "\(sign * Double(round(10000*fabs(decimal))/10000.0))"
        case .DM:
            return "\(Int(round(degrees)))° \(Double(round(100*minutes)/100))\""
        case .DMS:
            return "\(Int(round(degrees)))° \(Int(round(minutes)))\" \((round(100*seconds)/100))'"
        case .NoGPS:
            return ""
        }
    }
}

enum CoordinateComponent {
    case Latitude
    case Longitude
}

class FishEventLocation: NSObject, XLFormOptionObject {
    var type: GpsType
    var latitude: Coordinate
    var longitude: Coordinate

    var CLCoordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(
            latitude: latitude.decimal,
            longitude: longitude.decimal
        )
    }
    
    var text: String {
        return "\(latitude.textForType(type)), \(longitude.textForType(type))"
    }
    
    init(latitude: Coordinate, longitude: Coordinate, type: GpsType){
        self.latitude = latitude
        self.longitude = longitude
        self.type = type
    }
    init(latitude: Double, longitude: Double, type: GpsType){
        self.latitude = Coordinate(decimal: latitude)
        self.longitude = Coordinate(decimal: longitude)
        self.type = type
    }
    
    func setDegrees(degrees: Double, component: CoordinateComponent) {
        switch component {
        case .Latitude:
            switch type {
            case .DM:
                latitude = buildCoordinate(degrees, minutes: latitude.minutes, seconds: 0.0)
            case .DMS:
                latitude = buildCoordinate(degrees, minutes: latitude.minutes, seconds: latitude.seconds)
            default:
                () // do nothing
            }
            
        case .Longitude:
            switch type {
            case .DM:
                longitude = buildCoordinate(degrees, minutes: longitude.minutes, seconds: 0.0)
            case .DMS:
                longitude = buildCoordinate(degrees, minutes: longitude.minutes, seconds: longitude.seconds)
            default:
                () // do nothing
            }
        }
    }
    
    func setMinutes(minutes: Double, component: CoordinateComponent) {
        switch component {
        case .Latitude:
            switch type {
            case .DM:
                latitude = buildCoordinate(latitude.degrees, minutes: minutes, seconds: 0.0)
            case .DMS:
                latitude = buildCoordinate(latitude.degrees, minutes: minutes, seconds: latitude.seconds)
            default:
                () // do nothing
            }
            
        case .Longitude:
            switch type {
            case .DM:
                longitude = buildCoordinate(longitude.degrees, minutes: minutes, seconds: 0.0)
            case .DMS:
                longitude = buildCoordinate(longitude.degrees, minutes: minutes, seconds: longitude.seconds)
            default:
                () // do nothing
            }
        }
    }
    
    func setSeconds(seconds: Double, component: CoordinateComponent) {
        switch component {
        case .Latitude:
            switch type {
            case .DMS:
                latitude = buildCoordinate(latitude.degrees, minutes: latitude.minutes, seconds: seconds)
            default:
                () // do nothing
            }
            
        case .Longitude:
            switch type {
            case .DMS:
                longitude = buildCoordinate(longitude.degrees, minutes: longitude.minutes, seconds: seconds)
            default:
                () // do nothing
            }
        }
    }
    
    private func buildCoordinate(degrees: Double, minutes: Double, seconds: Double) -> Coordinate {
        let sign = (degrees < 0.0) ? -1.0 : 1.0;
        let value = floor(fabs(degrees)) +  (floor(minutes) / 60.0) + (seconds / 3600.0)
        
        return Coordinate(decimal: (value * sign))
    }
    
    
    // MARK - XLFormObject
    
    func formDisplayText() -> String {
        return self.text
    }
    
    func formValue() -> AnyObject {
        return self
    }

}