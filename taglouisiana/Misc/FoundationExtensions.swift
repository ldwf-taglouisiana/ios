//
//  Extensions.swift
//  taglouisiana
//
//  Created by Daniel Ward on 3/25/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import MagicalRecord

/**
  A `NSManagedObject` extension to add custom functionality for this
  application.

  Author: Daniel Ward
  Date: 11/12/15
  Copyright: (c) 2015 LDWF all rights reserved.
  Note: Built in conjuction with UNO GulfSCEI
*/
extension NSManagedObject {
    /**
        A convience function to get check if an item was deleted.

        - Returns: `true` if `this` was deleted from the context.
    */
    func wasDeleted() -> Bool {
        return self.managedObjectContext == nil
    }
}

/**
  A `NSManagedObjectContext` extension to add custom functionality for this
  application.

  Author: Daniel Ward
  Date: 11/12/15
  Copyright: (c) 2015 LDWF all rights reserved.
  Note: Built in conjuction with UNO GulfSCEI
*/
extension NSManagedObjectContext {
    /**
        A convience function to get the default `NSManagedObjectContext`.

        - Returns: The default `NSManagedObjectContext` that `MagicalRecord` is using.
    */
    class func defaultContext() -> NSManagedObjectContext {
        return MagicalRecordStack.defaultStack()!.context;
    }
}

/**
  A `Dictionary` extension to add custom functionality for this
  application.

  Author: Daniel Ward
  Date: 11/12/15
  Copyright: (c) 2015 LDWF all rights reserved.
  Note: Built in conjuction with UNO GulfSCEI
*/
extension Dictionary {

    /**
        A convience function determine if `this` is empty.

        - Returns: `true` if there are no keys in `this`, else `false`.
    */
    func isEmpty() -> Bool {
        return self.count == 0
    }
    
    func combinedWith(other: Dictionary<Key,Value>) -> Dictionary<Key,Value> {
        var result = Dictionary<Key,Value>()
        
        for (key, value) in self {
            result[key] = other[key] ?? value
        }
        
        return other
    }
}

/**
  A `NSDate` extension to add custom functionality for this
  application.

  Author: Daniel Ward
  Date: 11/12/15
  Copyright: (c) 2015 LDWF all rights reserved.
  Note: Built in conjuction with UNO GulfSCEI
*/
extension NSDate {

    /**
        Creates a date string that matches the US date format.
        A conveince wrapper around `.MediumStyle`.

        - Returns: `this` as a formatted date string. `MM/dd/YYYY`
    */
    var USDate: String {
        let formatter = NSDateFormatter()
        formatter.dateStyle = .MediumStyle
        return formatter.stringFromDate(self)
    }

    /**
        Creates a date string that matches the ISO 8601 standard.

        - Returns: `this` as a formatted ISO 8601 datetime string. `yyyy-MM-dd'T'HH:mm:ss'Z'`
    */
    var iso8601: String {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        return formatter.stringFromDate(self)
    }

    /**
        Checks that `this` is between the two given dates.

        - Parameters:
            - firstDate: The first date for the comparison
            - secondDate: The second date for the comparison

        - Returns: `true` if this is between `firstDate` and `secondDate`, else `false`.
    */
    func isBetween(firstDate: NSDate, secondDate: NSDate) -> Bool {
        if self.compare(firstDate) == NSComparisonResult.OrderedAscending {
            return false
        }

        if self.compare(secondDate) == NSComparisonResult.OrderedDescending {
            return false
        }

        return true;
    }

    /**
        Will create a new `NSDate` instance with the same time components as the given parameter.

        - Parameter date: The `NSDate` instance that will be used to create the new instance with.
        - Returns: A new `NSDate` with the components properly set to the incoming `NSDate`
    */
    class func dateFromSettingTimeFrom(date: NSDate) -> NSDate {
        let calendar: NSCalendar = NSCalendar.currentCalendar()
        let components: NSDateComponents = calendar.components([.Year, .Month, .Day], fromDate: self.init())
        let incomingComponents: NSDateComponents = calendar.components([.Hour, .Minute, .Second], fromDate: date)

        components.hour = incomingComponents.hour
        components.minute = incomingComponents.minute
        components.second = incomingComponents.second

        return calendar.dateFromComponents(components)!
    }

    /**
        Creates a new `NSDate` of `this` that has the minutes set to either `00` or `30`.
        The minutes component will be `floor`ed to find the closest hour or half hour.

        - Returns: If the minutes value could be floored, then a new `NSDate` instace with the correct values set. Else `this` will be returned unaltered.
    */
    func roundToNearestHalfHour() -> NSDate {
        // get the calendar to use for the time adjsutments
        let calendar: NSCalendar? = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)

        // get the components of self
        if let components = calendar?.components([.Year, .Month, .Day, .Hour, .Minute], fromDate: self) {
            let minute: Int = components.minute

            // round the minute to the floor then multiply by 30, will produce 0 or 30
            let rounded = floor(Float(minute) / 30.0) * 30.0
            // create an Int from the adjusted time
            let adjustedMinutes = Int(rounded)
            // set the minute value
            components.minute = adjustedMinutes

            // if we can create a NSDate object then we return it
            if let output = calendar?.dateFromComponents(components) {
                return output
            }
        }

        // if something failed along the way, then just return self
        return self
    }
}

/**
  A `NSTimeInterval` extension to add custom functionality for this
  application.

  Author: Daniel Ward
  Date: 11/12/15
  Copyright: (c) 2015 LDWF all rights reserved.
  Note: Built in conjuction with UNO GulfSCEI
*/
extension NSTimeInterval {

    /**
        Converts `this` to a human readable string.

        - Returns: A string representing `this` e.g. `"1 hrs 1 min"`
    */
    func toTimeString() -> String {
        let minutes = (self / 60) % 60
        let hours = self / 3600

        return "\(hours) hrs \(minutes) min"
    }
}

/**
  A `String` extension to add custom functionality for this
  application.

  Author: Daniel Ward
  Date: 11/12/15
  Copyright: (c) 2015 LDWF all rights reserved.
  Note: Built in conjuction with UNO GulfSCEI
*/
extension String {

    /**
        Converts an html embedded string to a `NSAttributedString`.

        - Returns: A `NSAttributedString` that is properly marked up with the contents of `this`
    */
    var html2String: String {
        return (try! NSAttributedString(data: dataUsingEncoding(NSUTF8StringEncoding)!, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)).string.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
    }

    /**
        A convience `var` that wraps `String.count`.

        - Returns: The number of characters in `this` string.
    */
    var length: Int {
        return self.characters.count
    }

    /**
        Finds the number of given regex matches for `this`.

        - Paramter regex: The regex as a string to check against `this`.
        - Returns: An array of matches of the given regex.
    */
    func matchesForRegex(regex: String) -> [String] {
        let regex: NSRegularExpression = try! NSRegularExpression(pattern: regex, options: [])
        let results = regex.matchesInString(self, options: [], range: NSMakeRange(0, self.characters.count))

        var matches = [String]()

        for match in results {
            for index in 1 ..< match.numberOfRanges {
                let nsrange: Range<Int> = match.rangeAtIndex(index).toRange()!
                let range = self.startIndex.advancedBy(nsrange.startIndex)..<self.startIndex.advancedBy(nsrange.endIndex)
                matches.append(self.substringWithRange(range))
            }
        }

        return matches
    }

    /**
        Checks to see if `this` contains the given substring.

        - Parameter find: The subsring you want to see if `this` contains.
        - Returns: `true` if `find` paramter is contained in `this`.
    */
    func contains(find: String) -> Bool {
        return self.rangeOfString(find) != nil
    }
}

//

/**
    Create a string to determine the amount of time from instance date to parameter date

    Note: http://stackoverflow.com/a/27184261
 */
extension NSDate {
    func yearsFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(NSCalendarUnit.Year, fromDate: date, toDate: self, options: []).year
    }

    func monthsFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(NSCalendarUnit.Month, fromDate: date, toDate: self, options: []).month
    }

    func weeksFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(NSCalendarUnit.WeekOfYear, fromDate: date, toDate: self, options: []).weekOfYear
    }

    func daysFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(NSCalendarUnit.Day, fromDate: date, toDate: self, options: []).day
    }

    func hoursFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(NSCalendarUnit.Hour, fromDate: date, toDate: self, options: []).hour
    }

    func minutesFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(NSCalendarUnit.Minute, fromDate: date, toDate: self, options: []).minute
    }

    func secondsFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(NSCalendarUnit.Second, fromDate: date, toDate: self, options: []).second
    }

    func offsetFrom(date: NSDate) -> String {
        return self.offsetFrom(date, longName: true)
    }

    func offsetFrom(date: NSDate, longName: Bool) -> String {
        if yearsFrom(date) > 0 {
            return "\(yearsFrom(date))\(longName ? " years" : "y" )"
        }
        if monthsFrom(date) > 0 {
            return "\(monthsFrom(date))\(longName ? " months" : "M" )"
        }
        if weeksFrom(date) > 0 {
            return "\(weeksFrom(date))\(longName ? " weeks" : "w" )"
        }
        if daysFrom(date) > 0 {
            return "\(daysFrom(date))\(longName ? " days" : "d" )"
        }
        if hoursFrom(date) > 0 {
            return "\(hoursFrom(date))\(longName ? " hours" : "h" )"
        }
        if minutesFrom(date) > 0 {
            return "\(minutesFrom(date))\(longName ? " minutes" : "m" )"
        }
        if secondsFrom(date) > 0 {
            return "\(secondsFrom(date))\(longName ? " seconds" : "s" )"
        }
        return ""
    }
}
