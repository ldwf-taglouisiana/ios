//
//  RegexFormRowValidator.swift
//  taglouisiana
//
//  Created by Daniel Ward on 4/9/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import Foundation
import XLForm

class RegexFormRowValidator: NSObject, XLFormValidatorProtocol {
    
    private var message: String
    private var regex: String
    
    init!(msg: String!, regex: String!) {
        self.message = msg
        self.regex = regex
    }
    
    func isValid(row: XLFormRowDescriptor!) -> XLFormValidationStatus! {
        var value: NSString = ""
        
        if let rowDescriptor = row {
            if let rowValue: AnyObject = row.value {
                if rowValue is NSNumber {
                    value = (row.value as! NSNumber).stringValue
                }
                
                if rowValue is NSString {
                    value = rowValue as! String
                }
            }
        }
        
        value = value.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        let isValid = NSPredicate(format: "SELF MATCHES %@", argumentArray: [value]).evaluateWithObject(value)
        return XLFormValidationStatus(msg: self.message, status: isValid, rowDescriptor: row)
    }
}
