    //
//  FormRow.swift
//  taglouisiana
//
//  Created by Daniel Ward on 4/2/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import Foundation
import MagicalRecord
import XLForm

enum FormRowTag: String {
    // standard rows
    case TagNumber = "tagNumber"
    case TagNumberSection = "tagNumberSection"
    case CaptureDate = "captureDate"
    case TimeOfDay = "timeOfDay"
    case Species = "species"
    case SpeciesLengthSwitch = "speciesLengthSwitch"
    case SpeciesLengthManual = "speciesLengthManual"
    case SpeciesLengthRange = "speciesLengthRange"
    case Condition = "condition"
    case Disposition = "disposition"
    case Location = "location"
    case Comments = "comments"
    
    // rows for entering location
    case GpsType = "gpsType"
    case LatitudeDegrees = "latitudeDegrees"
    case LatitudeDecimal = "latitudeDecimal"
    case LatitudeMinutes = "latitudeMinutes"
    case LatitudeSeconds = "latitudeSeconds"
    case LongitudeDegrees = "longitudeDegrees"
    case LongitudeDecimal = "longitudeDecimal"
    case LongitudeMinutes = "longitudeMinutes"
    case LongitudeSconds = "longitudeSeconds"
}

protocol FormRow {
    var tag:FormRowTag { get }
    var rowType:String { get }
    var title:String { get }
    var regex:String? { get }
    var errorMessage:String? { get }
    var validator: XLFormValidatorProtocol? { get }
    var row: XLFormRowDescriptor { get }
}

enum CaptureFormRow: FormRow {
    case TagNumber
    case TagNumberSection
    case CaptureDate
    case TimeOfDay(NSManagedObjectContext)
    case SpeciesOption(NSManagedObjectContext)
    case SpeciesLengthSwitch
    case SpeciesLengthManual
    case SpeciesLengthRange(NSManagedObjectContext, Species?)
    case Condition(NSManagedObjectContext)
    case Dispostion(NSManagedObjectContext)
    case Location
    case Comments
    
    var tag: FormRowTag {
        switch self {
        case .TagNumber:
            return FormRowTag.TagNumber
        case .TagNumberSection:
            return FormRowTag.TagNumberSection
        case .CaptureDate:
            return FormRowTag.CaptureDate
        case .TimeOfDay:
            return FormRowTag.TimeOfDay
        case .SpeciesOption:
            return FormRowTag.Species
        case .SpeciesLengthSwitch:
            return FormRowTag.SpeciesLengthSwitch
        case .SpeciesLengthManual:
            return FormRowTag.SpeciesLengthManual
        case .SpeciesLengthRange:
            return FormRowTag.SpeciesLengthRange
        case .Condition:
            return FormRowTag.Condition
        case .Dispostion:
            return FormRowTag.Disposition
        case .Location:
            return FormRowTag.Location
        case .Comments:
            return FormRowTag.Comments
        }
    }
    
    var rowType: String {
        switch self {
        case .TagNumber, .SpeciesLengthManual:
            return XLFormRowDescriptorTypeText
        case .CaptureDate:
            return XLFormRowDescriptorTypeDateInline
        case .SpeciesLengthSwitch:
            return XLFormRowDescriptorTypeBooleanSwitch
        case .Location:
            return XLFormRowDescriptorTypeSelectorPush
        case .TimeOfDay, .SpeciesOption, .SpeciesLengthRange, .Condition, .Dispostion:
            return XLFormRowDescriptorTypeSelectorPickerViewInline
        case .Comments:
            return XLFormRowDescriptorTypeTextView
        default:
            return XLFormRowDescriptorTypeName
        }
    }
    
    var title: String {
        switch self {
        case .TagNumber:
            return "Tag Number"
        case .TagNumberSection:
            return "Tag Numbers"
        case .CaptureDate:
            return "Capture Date"
        case .TimeOfDay:
            return "Time Of Day"
        case .SpeciesOption:
            return "Species"
        case .SpeciesLengthSwitch:
            return "Manual Length"
        case .SpeciesLengthManual:
            return "Actual Length"
        case .SpeciesLengthRange:
            return "Length Range"
        case .Condition:
            return "Condition"
        case .Dispostion:
            return "Dispostion"
        case .Location:
            return "Location"
        case .Comments:
            return "Comments"
        }

    }
    
    var regex: String? {
        switch self {
        case .TagNumber:
            return "^[A-Za-z]{1,2}[0-9]{6}$"
        case .SpeciesLengthManual:
            return "^0*[1-9][0-9]*(\\.[0-9]+)$"
        default:
            return nil
        }
    }
    
    var errorMessage: String? {
        switch self {
        case .TagNumber:
            return "Tag Number format incorrect (ex. LW123456)"
        case .SpeciesLengthManual:
            return "Length must be greater than 0"
        default:
            return nil
        }
    }
    
    var validator: XLFormValidatorProtocol? {
        switch self {
        case .TagNumber, .SpeciesLengthManual:
            return RegexFormRowValidator(msg: self.errorMessage, regex: self.regex)
        case .Location:
            return FishEventLocationValidator()
        default:
            return nil
        }
    }
    
    var section: XLFormSectionDescriptor {
        switch self {
        case .TagNumberSection:
            let sectionRow = XLFormSectionDescriptor.formSectionWithTitle(self.title, sectionOptions: XLFormSectionOptions.CanInsert | XLFormSectionOptions.CanDelete, sectionInsertMode: XLFormSectionInsertMode.Button) as! XLFormSectionDescriptor
            let row = XLFormRowDescriptor.formRowDescriptorWithTag(nil, rowType: XLFormRowDescriptorTypeSelectorPush, title: "") as! XLFormRowDescriptor
            row.action.viewControllerStoryboardId = Constants.STORYBOARD_ID_FORM_TAG_LIST
            row.addValidator(CaptureFormRow.TagNumber.validator)
            sectionRow.addFormRow(row)
            return sectionRow
        default:
            return XLFormSectionDescriptor.formSection() as! XLFormSectionDescriptor
        }
    }
    
    var row: XLFormRowDescriptor {
        let row = XLFormRowDescriptor(tag: self.tag.rawValue, rowType: self.rowType, title: self.title)

        if let item = self.validator {
            row.addValidator(item)
        }
        
        switch self {
        case .TagNumber:
            row.cellConfig.setObject(NSTextAlignment.Right.rawValue, forKey: "textField.textAlignment")
            row.required = true
            
        case .CaptureDate:
            row.value = NSDate()
            row.cellConfigAtConfigure.setValue(NSDate(), forKey: "minimumDate")
            
        case .TimeOfDay(let context):
            row.selectorOptions = TimeOfDayOption.MR_findAllSortedBy("remoteId", ascending: true, inContext: context) as! [TimeOfDayOption]
            row.value = TimeOfDayOption.current(context)
            
        case .SpeciesOption(let context):
            // had to use the module prefix due to enum nameing collision
            row.selectorOptions = Species.MR_findAllSortedBy("target,position:YES", ascending: false, inContext: context) as! [Species]
            row.value = row.selectorOptions[0]
            
        case .Location:
            row.action.viewControllerStoryboardId = Constants.STORYBOARD_ID_FORM_MAP
            row.value = FishEventLocation(latitude: Coordinate(decimal: 0.0), longitude: Coordinate(decimal: 0.0), type: GpsType.D)
            
        case .SpeciesLengthRange(let context, let species):
            let species = species == nil ? Species.MR_findFirstOrderedByAttribute("target,position:YES", ascending: false, inContext: context) as Species : species!
            let lengths  = species.speciesLengths as NSSet!
            row.selectorOptions = (lengths.sortedArrayUsingDescriptors([NSSortDescriptor(key: "position", ascending: true)])) as! [SpeciesLength]
            row.value = row.selectorOptions[0]
            
        case .SpeciesLengthManual:
            row.cellConfig.setObject(NSTextAlignment.Right.rawValue, forKey: "textField.textAlignment")
            
        case .Condition(let context):
            row.selectorOptions = FishConditionOption.MR_findAllSortedBy("remoteId", ascending: true, inContext: context) as! [FishConditionOption]
            row.value = row.selectorOptions[0]
            
        default:
            () // do nothing
        }
        
        return row
    }
}

enum LocationFormRow: FormRow {
    case GpsFormat
    case LatitudeDecimal
    case LatitudeDegrees(GpsType)
    case LatitudeMinutes(GpsType)
    case LatitudeSeconds(GpsType)
    case LongitudeDecimal
    case LongitudeDegrees(GpsType)
    case LongitudeMinutes(GpsType)
    case LongitudeSconds(GpsType)
    
    var tag: FormRowTag {
        switch self {
        case .GpsFormat:
            return FormRowTag.GpsType
        case .LatitudeDecimal:
            return FormRowTag.LatitudeDegrees
        case .LatitudeDegrees:
            return FormRowTag.LatitudeDecimal
        case .LatitudeMinutes:
            return FormRowTag.LatitudeMinutes
        case .LatitudeSeconds:
            return FormRowTag.LatitudeSeconds
        case .LongitudeDegrees:
            return FormRowTag.LongitudeDegrees
        case .LongitudeDecimal:
            return FormRowTag.LongitudeDecimal
        case .LongitudeMinutes:
            return FormRowTag.LongitudeMinutes
        case .LongitudeSconds:
            return FormRowTag.LongitudeSconds
        }
    }
    
    var rowType: String {
        switch self {
        case .GpsFormat:
            return XLFormRowDescriptorTypeSelectorPickerViewInline
        default:
            return XLFormRowDescriptorTypeDecimal
        }
    }
    
    var title: String {
        switch self {
        case .GpsFormat:
            return "Format"
        case .LatitudeDegrees, .LongitudeDegrees:
            return "Degrees"
        case .LatitudeDecimal, .LongitudeDecimal:
            return "Decimal"
        case .LatitudeMinutes, .LongitudeMinutes:
            return "Minutes"
        case .LatitudeSeconds, .LongitudeSconds:
            return "Seconds"
        }
    }
    
    var regex: String? {
        switch self {
        case .LatitudeSeconds, .LongitudeSconds:
            return "^([1-9]|[1-5][0-9])(\\.[0-9]+){0,1}$"
      
        // yeilds a regex for a range of 10.99999999 -> 50.99999999
        case .LatitudeDecimal:
            return "^([1-4][0-9]|50)(\\.[0-9]+){0,1}$"
            
        // yeilds a regex for a range of 10 -> 59
        case .LatitudeDegrees:
            return "^([1-4][0-9]|50)$"
   
        // yeilds a regex for a range of 0.99999999 -> 59.99999999
        case .LatitudeMinutes(let gpsType):
            switch gpsType {
            case GpsType.DM:
                return "^([1-9]|[1-5][0-9])(\\.[0-9]+){0,1}$"
            case GpsType.DMS:
                return "^([1-9]|[1-5][0-9])$"
            default:
                return nil
            }
            
        // yeilds a regex for a range of -135.99999999 -> -60.99999999
        case .LongitudeDecimal:
            return "^-([6-9][0-9]|1[0-2][0-9]|13[0-5])(\\.[0-9]+){0,1}$"

        // yeilds a regex for a range of -135 -> -60
        case .LongitudeDegrees(let gpsType):
            return "^-([6-9][0-9]|1[0-2][0-9]|13[0-5])$"

        // yeilds a regex for a range of 0 -> 59.99999999
        case .LongitudeMinutes(let gpsType):
            switch gpsType {
            case GpsType.DM:
                return "^([1-9]|[1-5][0-9])(\\.[0-9]+){0,1}$"
            case GpsType.DMS:
                return "^([1-9]|[1-5][0-9])$"
            default:
                return nil
            }
        default:
            return nil
        }
    }
    
    var errorMessage: String? {
        switch self {
        case .LatitudeSeconds:
            return "Latitude seconds must be between 0 and 59.99"
            
        case .LongitudeSconds:
            return "Longitude seconds must be between 0 and 59.99"
            
            // yeilds a regex for a range of 10.99 -> 50.99
        case .LatitudeDecimal:
            return "Latitude decimal must be between 10 and 59.99"
            
            // yeilds a regex for a range of 10 -> 50
        case .LatitudeDegrees:
            return "Latitude degrees must be between 10 and 59"
            
            // yeilds a regex for a range of 0.99 -> 59.99
        case .LatitudeMinutes(let gpsType):
            switch gpsType {
            case GpsType.DM:
                return "Latitude minutes must be between 0 and 59.99"
            case GpsType.DMS:
                return "Latitude minutes must be between 0 and 59"
            default:
                return nil
            }
            
            // yeilds a regex for a range of -135.99 -> -69.99
        case .LongitudeDecimal:
            return "Longitude decimal must be between -135.99 and -69.99"
            
            // yeilds a regex for a range of -135 -> -60
        case .LongitudeDegrees(let gpsType):
            return "Longitude degrees must be between -135 and -69"
            
            // yeilds a regex for a range of 0 -> 59.99999999
        case .LongitudeMinutes(let gpsType):
            switch gpsType {
            case GpsType.DM:
                return "Longitude minutes must be between 0 and 59.99"
            case GpsType.DMS:
                return "Longitude minutes must be between 0 and 59"
            default:
                return nil
            }
            
        default:
            return nil
        }

    }
    
    var validator: XLFormValidatorProtocol? {
        
        if let value = regex {
            return RegexFormRowValidator(msg: self.errorMessage, regex: value)
        } else {
            return nil
        }
    }
    
    var row: XLFormRowDescriptor {
        let row = XLFormRowDescriptor(tag: self.tag.rawValue, rowType: self.rowType, title: self.title)
       
        if let value = validator {
            row.addValidator(value)
        }
        
        switch self {
        case .GpsFormat:
            row.selectorOptions = GpsType.pickerValues
            row.value = GpsType.D.formOptionObject
            
        default:
            row.cellConfig.setObject(NSTextAlignment.Right.rawValue, forKey: "textField.textAlignment")
        }
        
        return row
    }
}
