/**
  UIKitExtensions.swift
  taglouisiana

  Author: Daniel Ward
  Date: 4/14/15
  Copyright: (c) 2015 LDWF all rights reserved.
  Note: Built in conjuction with UNO GulfSCEI
*/

import Foundation
import MapKit
import XLForm

/**
  A `MKMapView` extension to add custom functionality for this
  application.

  Author: Daniel Ward
  Date: 11/12/15
  Copyright: (c) 2015 LDWF all rights reserved.
  Note: Built in conjuction with UNO GulfSCEI
*/
extension MKMapView {

    // MARK: Additions for `FishEvent`

    /**
        Will add the given `FishEvent` to the `this` map.

        `event.location().type == GpsType.NoGPS` will be **ignored**

        - Paramters:
            - event: The `FishEvent` to add to the map (`FishEventLocation.location().type == GpsType.NoGPS` will be ignored).
            - mileSpan: The sile span around the event to make the view region (default `5.0`).
    */
    func addFishEvent(event: FishEvent, mileSpan: Double = 5.0) {
        if let location = event.location() {
            self.addFishEventLocation(location, title: event.tagNumber, mileSpan: mileSpan, moveToLocation: true)
        }
    }

    /**
        Will add the given `FishEventLocation` to the `this` map.

        `eventLocation.type == GpsType.NoGPS` will be **ignored**

        - Paramters:
            - eventLocation: The `FishEventLocation` to add to the map (`eventLocation.type == GpsType.NoGPS` will be ignored).
            - title: The title of the pin that will be added to the map (default `nil`).
            - mileSpan: The sile span around the event to make the view region (default `5.0`).
            - moveToLocation: If we should move the camera to the region of the new pin (default `false`).
    */
    func addFishEventLocation(eventLocation: FishEventLocation, title: String? = nil, mileSpan: Double = 5.0, moveToLocation: Bool = false) {
        // if the event has no gps indicated, then ignore
        if eventLocation.type == GpsType.NoGPS {
            return
        }

        // get the coordinate
        let location = eventLocation.CLCoordinate

        // move to the point, if indicated
        if moveToLocation {
            let region = MKCoordinateRegion(center: location, span: makeMileSpan(mileSpan, eventLocation: eventLocation))
            self.setRegion(region, animated: true)
        }

        // create the pin
        let annotation: MKPointAnnotation = MKPointAnnotation()
        annotation.coordinate = location

        // set the pin title, if any
        if let annotationTitle = title {
            annotation.title = annotationTitle
        }

        // add the pin to the map
        self.addAnnotation(annotation)
    }
    
    /**
        Will calculate the span for an area of the given miles around the `FishEventLocation` object

        - Paramters:
            - miles: the number of miles around the center to expand the region.
            - eventLocation: The `FishEventocation` to use as the center of the region.
        - Returns: A `MKCoordinateSpan` that has the proper miles around the event center.
        - Note: http://stackoverflow.com/a/5025369
    */
    private func makeMileSpan(miles: Double, eventLocation: FishEventLocation) -> MKCoordinateSpan {
        let m = (2.0 * M_PI * eventLocation.latitude.decimal) / 360.0
        let scalingFactor = Double(abs(cos(m)));
        
        let latDelta = miles / 69.0
        let lonDelta = miles / (scalingFactor * 69.0)
        
        return MKCoordinateSpanMake(latDelta, lonDelta)
    }
    
}

/**
  A `UIImageView` extension to add custom functionality for this
  application.

  Author: Daniel Ward
  Date: 11/12/15
  Copyright: (c) 2015 LDWF all rights reserved.
  Note: Built in conjuction with UNO GulfSCEI
*/
extension UIImageView  {
    
    func loadImageFromUrl(imageUrl: String) {
        let imageCache = Constants.IMAGE_CACHE
        
        if let image = imageCache.objectForKey(imageUrl) as? UIImage {
            self.image = image
        } else {
            self.image = nil
            
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                
                let image =  UIImage(data: NSData(contentsOfURL: NSURL(string: imageUrl)!)!)
                imageCache.setObject(image!, forKey: imageUrl)
                self.image = image
            })
        }
    }
}

/**
  A `UISplitViewController` extension to add custom functionality for this
  application.

  Author: Daniel Ward
  Date: 11/12/15
  Copyright: (c) 2015 LDWF all rights reserved.
  Note: Built in conjuction with UNO GulfSCEI
*/
extension UISplitViewController {
    func toggleMasterView() {
        let barButtonItem = self.displayModeButtonItem()
        UIApplication.sharedApplication().sendAction(barButtonItem.action, to: barButtonItem.target, from: nil, forEvent: nil)
    }
}

/**
  A `XLFormViewController` extension to add custom functionality for this
  application.

  Author: Daniel Ward
  Date: 11/12/15
  Copyright: (c) 2015 LDWF all rights reserved.
  Note: Built in conjuction with UNO GulfSCEI
*/
extension XLFormViewController {

    /**
        Will validate the form, by looking at the current form errors, if any.

        If there are any errors, then am `UIAlertController` will be shown with
        errors.

        - Returns: 'true` if there are no errors, else `false`
    */
    func validateForm() -> Bool {
        let errors = self.formValidationErrors() as! [NSError]
        var errorStrings = [String]()
        
        // go through each error
        for error in errors {
            if  let status: XLFormValidationStatus = (error.userInfo as! [NSString: AnyObject])[XLValidationStatusErrorKey] as? XLFormValidationStatus,
                cell: UITableViewCell = self.tableView.cellForRowAtIndexPath(self.form.indexPathOfFormRow(status.rowDescriptor!)!) {

                cell.textLabel?.textColor = UIColor.redColor()
                errorStrings.append("• \(status.msg)")

            }
        }

        // if there were any errors, create and show an alert dialog with the errors
        if errors.count > 0 {
            // Add combine the error messages into one string
            let errorMessage = errorStrings.joinWithSeparator("\n")
           
            // create the alert view to show the error messages
            let alertController = UIAlertController(title: "Form Errors", message: errorMessage, preferredStyle: .Alert)
            let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
                // no other action needed
            }
            alertController.addAction(OKAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        
        // return whether there were errors
        return errors.count == 0
    }
}

/**
  A `UIImage` extension to add custom functionality for this
  application.

  Author: Daniel Ward
  Date: 11/12/15
  Copyright: (c) 2015 LDWF all rights reserved.
  Note: Built in conjuction with UNO GulfSCEI
*/
extension UIImage {
    /**
        Creates a new `UIImage` that is tinted with the given `UIColor`.

        - Parameter color: A `UIColor` To tint `this` image with.
        - Returns: A new `UIImage` that is the same as `this`, but has been tinted with the given color
    */
    func tintWithColor(color: UIColor) -> UIImage {
        
        UIGraphicsBeginImageContext(self.size)
        let context = UIGraphicsGetCurrentContext()
        
        // flip the image
        CGContextScaleCTM(context!, 1.0, -1.0)
        CGContextTranslateCTM(context!, 0.0, -self.size.height)
        
        // multiply blend mode
        CGContextSetBlendMode(context!, CGBlendMode.Multiply)
        
        let rect = CGRectMake(0, 0, self.size.width, self.size.height)
        CGContextClipToMask(context!, rect, self.CGImage!)
        color.setFill()
        CGContextFillRect(context!, rect)
        
        // create uiimage
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

/**
  A `UIColor` extension to convert a color hex `String` to
  a `UIColor` instance.

  Author: R0CKSTAR
  Date: 6/13/14
  Copyright: (c) 2014 P.D.Q. All rights reserved.
  Note: https://github.com/yeahdongcn/UIColor-Hex-Swift
*/
extension UIColor {

    /**
        Creates a new `UIColor` instance with the given hax `String` parameter.

        - Parameter rgba: A `String` that represents a hex color. Aplha component is optional.
    */
    convenience init(rgba: String) {
        var red:   CGFloat = 0.0
        var green: CGFloat = 0.0
        var blue:  CGFloat = 0.0
        var alpha: CGFloat = 1.0
        
        if rgba.hasPrefix("#") {
            let index   = rgba.startIndex.advancedBy(1)
            let hex     = rgba.substringFromIndex(index)
            let scanner = NSScanner(string: hex)
            var hexValue: CUnsignedLongLong = 0
            if scanner.scanHexLongLong(&hexValue) {
                switch (hex.characters.count) {
                case 3:
                    red   = CGFloat((hexValue & 0xF00) >> 8)       / 15.0
                    green = CGFloat((hexValue & 0x0F0) >> 4)       / 15.0
                    blue  = CGFloat(hexValue & 0x00F)              / 15.0
                case 4:
                    red   = CGFloat((hexValue & 0xF000) >> 12)     / 15.0
                    green = CGFloat((hexValue & 0x0F00) >> 8)      / 15.0
                    blue  = CGFloat((hexValue & 0x00F0) >> 4)      / 15.0
                    alpha = CGFloat(hexValue & 0x000F)             / 15.0
                case 6:
                    red   = CGFloat((hexValue & 0xFF0000) >> 16)   / 255.0
                    green = CGFloat((hexValue & 0x00FF00) >> 8)    / 255.0
                    blue  = CGFloat(hexValue & 0x0000FF)           / 255.0
                case 8:
                    red   = CGFloat((hexValue & 0xFF000000) >> 24) / 255.0
                    green = CGFloat((hexValue & 0x00FF0000) >> 16) / 255.0
                    blue  = CGFloat((hexValue & 0x0000FF00) >> 8)  / 255.0
                    alpha = CGFloat(hexValue & 0x000000FF)         / 255.0
                default:
                    print("Invalid RGB string, number of characters after '#' should be either 3, 4, 6 or 8", terminator: "")
                }
            } else {
                print("Scan hex error")
            }
        } else {
            print("Invalid RGB string, missing '#' as prefix", terminator: "")
        }
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
}
