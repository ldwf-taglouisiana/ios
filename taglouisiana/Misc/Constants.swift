//
//  Constants.swift
//  taglouisiana
//
//  Created by Daniel Ward on 3/20/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import Foundation
import UIKit

public struct Constants {

    /*
        ENVIROMENT SPECIFIC SETTINGS
    */

#if DEBUG
//    static let SERVER_ADDRESS = "https://narwhal.taglouisiana.com"
//    static let CLIENT_ID = "2a624e7df1f479dfbedac01131d908a789383455c10cfc5ed2dd285b01ca3496";
//    static let CLIENT_SECRET = "68012976f9de302a8927016660f6406e11dc7982ed56c81218ca9d540c76ff17";
    static let SERVER_ADDRESS = "http://localhost:4000"
    static let CLIENT_ID = "2a624e7df1f479dfbedac01131d908a789383455c10cfc5ed2dd285b01ca3496"
    static let CLIENT_SECRET = "68012976f9de302a8927016660f6406e11dc7982ed56c81218ca9d540c76ff17"
#else
    static let SERVER_ADDRESS = "https://narwhal.taglouisiana.com"
    static let CLIENT_ID = "2a624e7df1f479dfbedac01131d908a789383455c10cfc5ed2dd285b01ca3496";
    static let CLIENT_SECRET = "68012976f9de302a8927016660f6406e11dc7982ed56c81218ca9d540c76ff17";
#endif

    /*
       -------
    */

    static let API_TOKEN_PARAMETER = "app_token"

    static let UICOLOR_BLUE = UIColor(red:0.05, green:0.14, blue:0.25, alpha:1.0) // #0D2440
    static let UICOLOR_ORANGE = UIColor(red:0.94, green:0.51, blue:0.12, alpha:1.0) // #EF831E

    static let AOUTH_ACCOUNT = "TagLouisiana"
    static let API_SERVER = SERVER_ADDRESS + "/api/v2"

    static let IMAGE_CACHE = NSCache()

    static let CONTACT_EMAIL_ADDRESS = "info@taglouisiana.com"
    static let STORE_NAME = "taglouisiana.sqlite"
    
    static let UI_MAP_INSET: CGFloat = 100.0
    
    /*
      View TAGS
    */
    
    static let TAG_PENDING_CAPTURE_TAB = 3
    
    /*
      NSUserDefaults KEYS
    */

    static let DEFAULTS_KEY_FIRST_SYNC  = "first_sync"
    static let DEFAULTS_KEY_API_TOKEN   = "key_api_token"
    static let DEFAULTS_KEY_SIGN_IN_SET = "sign_in_set"
    
    // api etag storage
    
    static let DEFAULTS_KEY_API_ETAG_PUBLIC_DATA        = "API.ETAG.public_data"
    static let DEFAULTS_KEY_API_ETAG_USER_DATA          = "API.ETAG.user_data"
    static let DEFAULTS_KEY_API_ETAG_PUBLIC_IDS         = "API.ETAG.public_ids"
    static let DEFAULTS_KEY_API_ETAG_USER_IDS           = "API.ETAG.user_ids"
    static let DEFAULTS_KEY_API_ETAG_PUBLIC_DASHBOARD   = "API.ETAG.public_dashboard"
    static let DEFAULTS_KEY_API_ETAG_ANGLER_DASHBOARD   = "API.ETAG.angler_dashboard"
    
    /*
        Settings preferences
    */
    
    static let PREFERENCES_RESET_FORM                   = "preference_reset_from"
    static let PREFERENCES_REFILL_FORM_WITH_PREVIOUS    = "preference_refill_with_previous"
    
    /*
      Segueu Identifiers
    */

    static let SEGUE_LOGIN                  = "login"
    static let SEGUE_TIME_ON_WATER          = "add time on water"
    static let SEGUE_NEW_FISH_EVENT         = "new fish event"
    static let SEGUE_EDIT_FISH_EVENT        = "edit fish event"
    static let SEGUE_COPY_FISH_EVENT        = "copy fish event"
    static let SEGUE_REQUEST_TAGS           = "request tags"
    static let SEGUE_MAP_VIEW               = "map view"
    static let SEGUE_SHOW_CAPTURE_DETAIL    = "show capture detail"
    static let SEGUE_SHOW_SPECIES_DETAIL    = "show species detail"
    static let SEGUE_SHOW_FORM_LOCATION_MAP = "show location map"
    static let SEGUE_ENTER_MANUAL_LOCATION  = "show maual location form"
    static let SEGUE_FORCE_SYNC             = "show resync"
    static let SEGUE_RETURN_TO_LIST         = "returnToList"
    
    /*
        UI Reuse Identifiers
    */
    
    static let UI_REUSE_IDENTIFIER_DASHBOARD_ITEM_CELL = "dashboard_item_table_cell"
    
    /*
        StoryBoard Identifiers
    */
    
    static let STORYBOARD_ID_FORM_MAP                           = "show form location map"
    static let STORYBOARD_ID_FORM_TAG_LIST                      = "TagListForm"
    static let STORYBOARD_ID_PENDING_CAPTURE_LIST               = "pending_capture_controller"
    static let STORYBOARD_ID_DASHBOARD_TABLE_VIEW_CONTROLLER    = "dashboard_table_view_controller"
    static let STORYBOARD_ID_CAPTURE_DETAIL_VIEW_CONTROLLER     = "capture_detail_view_controller"
    static let STORYBOARD_ID_RECAPTURE_DETAIL_VIEW_CONTROLLER   = "recapture_detail_view_controller"
    
    /*
        NOTIFICATION ACTIONS
    */
    static let NOTIFICATION_DATA_UPDATE_STARTED  = "gov.louisiana.wlf.taglouisiana.dataWillUpdate"
    static let NOTIFICATION_DATA_UPDATE_FINISHED = "gov.louisiana.wlf.taglouisiana.dataDidUpdate"
    static let NOTIFICATION_FORCE_SYNC_STARTED   = "gov.louisiana.wlf.taglouisiana.forceSyncStarted"
    static let NOTIFICATION_FORCE_SYNC_FINISHED  = "gov.louisiana.wlf.taglouisiana.forceSyncFinished"
}
