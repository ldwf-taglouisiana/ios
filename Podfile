source 'https://github.com/CocoaPods/Specs.git'
platform :ios, '8.0'
use_frameworks!

post_install do |installer|
  installer.pods_project.build_configuration_list.build_configurations.each do |configuration|
    configuration.build_settings['CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES'] = 'YES'
  end
end

def taglouisiana_pods

  # used for parsing the GeoJSON data to display on MapKit
  pod 'GEOSwift', git: 'https://github.com/andreacremaschi/GEOSwift.git', branch: 'feature/Swift-2.0'
	
  # used for network connections
  pod 'Alamofire', '~> 3.5.1'

  # used for generating hash tokens
  pod 'CryptoSwift', :git => "https://github.com/krzyzanowskim/CryptoSwift", :branch => "swift2"

  # used for better JSON handling in swift
  pod 'SwiftyJSON', git: 'https://github.com/SwiftyJSON/SwiftyJSON.git', branch: 'swift2'

  # pages for the dashboard
  pod 'XLPagerTabStrip', '~> 5.0'

  # used for creating actionsheet pickers
  pod 'ActionSheetPicker-3.0', '~> 2.0.0'

  # used to manage the OAuth connections
  pod 'NXOAuth2Client', '~> 1.2.8', :inhibit_warnings => true

  # used to select images from the gallery or the camera
  pod 'ImagePicker', git: 'https://github.com/hyperoslo/ImagePicker.git', branch: 'swift-2.3'

  # used to create photo galleries
  pod "MWPhotoBrowser"

  # used to create some nicer activity Spinners
  pod 'MMMaterialDesignSpinner'

  # used for caching and loading images
  #pod 'ImageLoader'#, '~> 0.7.2'

  pod 'Kingfisher', git: 'https://github.com/onevcat/Kingfisher.git', branch: 'swift2.3'

  # used for better form management
  pod 'XLForm', '~> 3.0.0'

  # adds some fuctionality from ruby for strings
  pod 'NSString+Ruby'

  # used as an interface layer to CoreData
  pod 'MagicalRecord/Core+Logging', git: 'https://github.com/dwa012/MagicalRecord.git', branch: 'dwa012-working', :inhibit_warnings => true
end

target 'taglouisiana' do
	taglouisiana_pods
end

target 'taglouisianaTests' do
	taglouisiana_pods
end
