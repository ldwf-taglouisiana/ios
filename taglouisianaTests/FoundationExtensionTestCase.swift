//
//  ExtensionTestCase.swift
//  taglouisiana
//
//  Created by Daniel Ward on 4/14/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import UIKit
import XCTest
@testable import taglouisiana

class FoundationExtensionTestCase: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testNSDate() {
        // this is the date Tue, 14 Apr 2015 17:50:49 GMT
        let date = NSDate(timeIntervalSince1970: 1429033849)
        
        // -------------------------------------------------
        
        // test the USDate var
        XCTAssertEqual(date.USDate, "Apr 14, 2015", "")
        
        // -------------------------------------------------
        
        // test the iso8601 var
        XCTAssertEqual(date.iso8601, "2015-04-14T17:50:49+0000", "")
        
        // -------------------------------------------------
        
        // test the isBetween function
        XCTAssertFalse(date.isBetween(NSDate(timeIntervalSince1970: 1429033847), secondDate: NSDate(timeIntervalSince1970: 1429033848)), "")
        XCTAssertTrue(date.isBetween(NSDate(timeIntervalSince1970: 1429033848), secondDate: NSDate(timeIntervalSince1970: 1429033850)), "")
        XCTAssertFalse(date.isBetween(NSDate(timeIntervalSince1970: 1429033850), secondDate: NSDate(timeIntervalSince1970: 1429033851)), "")
        
        // -------------------------------------------------
        
        // test the dateFromSettingTimeFrom, should have the same hour, minure, and seconds as passed in date
        let date1: NSDate = NSDate.dateFromSettingTimeFrom(date)
        XCTAssertTrue(date1.iso8601.rangeOfString("17:50:49") != nil, "")
    
        // -------------------------------------------------
        
        // test the roundToNearestHalfHour
        
        // Tue, 14 Apr 2015 17:29:50 GMT
        let date2 = NSDate(timeIntervalSince1970: 1429032590)
        // Tue, 14 Apr 2015 17:59:50 GMT
        let date3 = NSDate(timeIntervalSince1970: 1429034390)
        
        XCTAssertEqual(date2.roundToNearestHalfHour().iso8601, "2015-04-14T17:00:00+0000", "")
        XCTAssertEqual(date3.roundToNearestHalfHour().iso8601, "2015-04-14T17:30:00+0000", "")
    }
    
    func testString() {
        
        // test matchesForRegex
        let regex = "([A-Z]{0,2})([0-9]{6})"
        let string = "LW123456"
        let matches = string.matchesForRegex(regex)
        
        XCTAssertTrue(matches.count == 2, "")
        XCTAssertEqual(matches[0], "LW", "")
        XCTAssertEqual(matches[1], "123456", "")
        
        // -------------------------------------------------
    }

}
