//
//  FishEventTestCase.swift
//  taglouisiana
//
//  Created by Daniel Ward on 4/17/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import UIKit
import XCTest
import MagicalRecord
@testable import taglouisiana

class FishEventTestCase: XCTestCase {

    override func setUp() {
        super.setUp()
        
        MagicalRecord.setupStackWithInMemoryStore()
//        MagicalRecord.setDefaultModelFromClass(self.classForCoder)
    }
    
    override func tearDown() {
        MagicalRecord.cleanUp()
        
        super.tearDown()
    }

    func testLengthText() {
        
        let expectation = expectationWithDescription("api")
        
        Api.FetchPublicData.execute({
            let date1 = NSDate()
            let date2 = NSDate()
            let date3 = NSDate()
            let condition = FishConditionOption.MR_findFirst()
            let species = Species.MR_findFirst()
            let speciesLength = SpeciesLength.MR_findFirst()
            let timeOfDay = TimeOfDayOption.MR_findFirst()
            let uuid = NSUUID().UUIDString
            
            let draft = EnteredDraftCapture.MR_createEntity() as EnteredDraftCapture
            draft.remoteId = -3
            draft.comments = "comments"
            draft.date = date1
            draft.gpsType = GpsType.DM.rawValue
            draft.latitude = 30.000
            draft.longitude = -90.000
            draft.length = 34
            draft.locationDescription = "location description"
            draft.tagNumber = "LW123222"
            draft.fishCondition = condition
            draft.species = species
            draft.speciesLength = speciesLength
            draft.timeOfDay = timeOfDay
            draft.errorJson = "{\"errors\":null}"
            draft.recapture = NSNumber(bool: true)
            draft.savedAt = date2
            draft.shouldDelete = NSNumber(bool: false)
            draft.shouldSave = NSNumber(bool: true)
            draft.status = nil
            draft.uuid = uuid
            draft.updatedAt = date3
            
            XCTAssertEqual(draft.lengthText(), "34.0 inches", "")
            
            draft.length = nil
            XCTAssertEqual(draft.lengthText(), speciesLength.detail, "")
            
            draft.speciesLength = nil
            XCTAssertEqual(draft.lengthText(), "No Length", "")
            
            expectation.fulfill()
        })
        
        waitForExpectationsWithTimeout(25, handler: { error in
            if error != nil {
                print("\(error)")
            }
        })

    }
    
    func testFishEventLocation() {
        
        let expectation = expectationWithDescription("api")
        
        Api.FetchPublicData.execute({
            let date1 = NSDate()
            let date2 = NSDate()
            let date3 = NSDate()
            let condition = FishConditionOption.MR_findFirst()
            let species = Species.MR_findFirst()
            let speciesLength = SpeciesLength.MR_findFirst()
            let timeOfDay = TimeOfDayOption.MR_findFirst()
            let uuid = NSUUID().UUIDString
            
            let draft = EnteredDraftCapture.MR_createEntity() as EnteredDraftCapture
            draft.remoteId = -3
            draft.comments = "comments"
            draft.date = date1
            draft.gpsType = GpsType.DM.rawValue
            draft.latitude = 30.000
            draft.longitude = -90.000
            draft.length = 34
            draft.locationDescription = "location description"
            draft.tagNumber = "LW123222"
            draft.fishCondition = condition
            draft.species = species
            draft.speciesLength = speciesLength
            draft.timeOfDay = timeOfDay
            draft.errorJson = "{\"errors\":null}"
            draft.recapture = NSNumber(bool: true)
            draft.savedAt = date2
            draft.shouldDelete = NSNumber(bool: false)
            draft.shouldSave = NSNumber(bool: true)
            draft.status = nil
            draft.uuid = uuid
            draft.updatedAt = date3
            
            XCTAssertEqual(draft.location().latitude.decimal, draft.latitude, "")
            XCTAssertEqual(draft.location().longitude.decimal, draft.longitude, "")
            XCTAssertEqual(draft.location().type.rawValue, GpsType.DM.rawValue, "")
            
            expectation.fulfill()
        })
        
        waitForExpectationsWithTimeout(25, handler: { error in
            if error != nil {
                print("\(error)")
            }
        })
        
    }
    
    func testGPSTypeEnum() {
        
        let expectation = expectationWithDescription("api")
        
        Api.FetchPublicData.execute({
            let date1 = NSDate()
            let date2 = NSDate()
            let date3 = NSDate()
            let condition = FishConditionOption.MR_findFirst()
            let species = Species.MR_findFirst()
            let speciesLength = SpeciesLength.MR_findFirst()
            let timeOfDay = TimeOfDayOption.MR_findFirst()
            let uuid = NSUUID().UUIDString
            
            let draft = EnteredDraftCapture.MR_createEntity() as EnteredDraftCapture
            draft.remoteId = -3
            draft.comments = "comments"
            draft.date = date1
            draft.gpsType = GpsType.DM.rawValue
            draft.latitude = 30.000
            draft.longitude = -90.000
            draft.length = 34
            draft.locationDescription = "location description"
            draft.tagNumber = "LW123222"
            draft.fishCondition = condition
            draft.species = species
            draft.speciesLength = speciesLength
            draft.timeOfDay = timeOfDay
            draft.errorJson = "{\"errors\":null}"
            draft.recapture = NSNumber(bool: true)
            draft.savedAt = date2
            draft.shouldDelete = NSNumber(bool: false)
            draft.shouldSave = NSNumber(bool: true)
            draft.status = nil
            draft.uuid = uuid
            draft.updatedAt = date3
            
            XCTAssertEqual(draft.gpsTypeEnum().rawValue, GpsType.DM.rawValue, "")
            
            expectation.fulfill()
        })
        
        waitForExpectationsWithTimeout(25, handler: { error in
            if error != nil {
                print("\(error)")
            }
        })
        
    }
}
