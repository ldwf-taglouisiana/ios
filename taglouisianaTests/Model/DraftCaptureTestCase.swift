//
//  DraftCapture.swift
//  taglouisiana
//
//  Created by Daniel Ward on 4/13/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import UIKit
import XCTest
import MagicalRecord
@testable import taglouisiana

class DraftCaptureTestCase: XCTestCase {

    override func setUp() {
        super.setUp()
        
        MagicalRecord.setupStackWithInMemoryStore()
//        MagicalRecord.setDefaultModelFromClass(self.classForCoder)
    }
    
    override func tearDown() {
        MagicalRecord.cleanUp()
        
        super.tearDown()
    }
    
    func testThatEntityWasSaved() {

        let expectation = expectationWithDescription("api")
        
        Api.FetchPublicData.execute({
            let date1 = NSDate()
            let date2 = NSDate()
            let date3 = NSDate()
            let condition = FishConditionOption.MR_findFirst()
            let species = Species.MR_findFirst()
            let speciesLength = SpeciesLength.MR_findFirst()
            let timeOfDay = TimeOfDayOption.MR_findFirst()
            let uuid = NSUUID().UUIDString
            
            let draft = DraftCapture.MR_createEntity()
            draft.remoteId = -3
            draft.comments = "comments"
            draft.date = date1
            draft.gpsType = GpsType.DM.rawValue
            draft.latitude = 30.000
            draft.longitude = -90.000
            draft.length = 34
            draft.locationDescription = "location description"
            draft.tagNumber = "LW123222"
            draft.fishCondition = condition
            draft.species = species
            draft.speciesLength = speciesLength
            draft.timeOfDay = timeOfDay
            draft.errorJson = "{\"erros\":null}"
            draft.recapture = NSNumber(bool: true)
            draft.savedAt = date2
            draft.shouldDelete = NSNumber(bool: false)
            draft.shouldSave = NSNumber(bool: true)
            draft.status = nil
            draft.uuid = uuid
            draft.updatedAt = date3
            
            draft.managedObjectContext?.MR_saveToPersistentStoreAndWait()
            
            // ----------------
            
            let draftFromStore = DraftCapture.MR_findFirstByAttribute("uuid", withValue: uuid) as DraftCapture?
            
            if let item: DraftCapture = draftFromStore {
                print("\(item)")
                XCTAssertEqual(item.remoteId, -3, "")
                XCTAssertEqual(item.comments!, "comments", "")
                XCTAssertEqual(item.date, date1, "")
                XCTAssertEqual(item.gpsType, GpsType.DM.rawValue, "")
                XCTAssertEqual(item.latitude, 30.000, "")
                XCTAssertEqual(item.longitude, -90.000, "")
                XCTAssertEqual(item.length!, 34, "")
                XCTAssertEqual(item.locationDescription!, "location description", "")
                XCTAssertEqual(item.tagNumber, "LW123222", "")
                XCTAssertEqual(item.fishCondition!.remoteId, condition.remoteId, "")
                XCTAssertEqual(item.species.remoteId, species.remoteId, "")
                XCTAssertEqual(item.speciesLength!.remoteId, speciesLength.remoteId, "")
                XCTAssertEqual(item.timeOfDay!.remoteId, timeOfDay.remoteId, "")
                
                XCTAssertEqual(item.errorJson, "{\"erros\":null}", "")
                
                XCTAssertEqual(item.recapture, NSNumber(bool: true), "")
                XCTAssertEqual(item.savedAt, date2, "")
                XCTAssertEqual(item.shouldDelete, NSNumber(bool: false), "")
                XCTAssertEqual(item.shouldSave, NSNumber(bool: true), "")
                XCTAssertNil(item.status, "")
                XCTAssertEqual(item.uuid, uuid, "")
                XCTAssertEqual(item.updatedAt, date3, "")
                
                
            } else {
                XCTFail("Did not fetch any items")
            }
            
            expectation.fulfill()
            
        })
        
        waitForExpectationsWithTimeout(30, handler: { error in
            if error != nil {
                print("\(error)")
            }
        })
    }
    
    func testConvertToEnteredDraftCapture() {
        /*
            @NSManaged var comments: String?
            @NSManaged var date: NSDate
            @NSManaged var gpsType: String
            @NSManaged var latitude: NSNumber
            @NSManaged var length: NSNumber?
            @NSManaged var locationDescription: String?
            @NSManaged var longitude: NSNumber
            @NSManaged var tagNumber: String
            @NSManaged var fishCondition: FishConditionOption?
            @NSManaged var species: Species
            @NSManaged var speciesLength: SpeciesLength?
            @NSManaged var timeOfDay: TimeOfDayOption?
            @NSManaged var errorJson: String
            @NSManaged var recapture: NSNumber
            @NSManaged var savedAt: NSDate
            @NSManaged var shouldDelete: NSNumber
            @NSManaged var shouldSave: NSNumber
            @NSManaged var status: DraftCaptureStatus
            @NSManaged var uuid: String?
        */
        
        let expectation = expectationWithDescription("api")
        
        Api.FetchPublicData.execute({
            let date1 = NSDate()
            let date2 = NSDate()
            let date3 = NSDate()
            let condition = FishConditionOption.MR_findFirst()
            let species = Species.MR_findFirst()
            let speciesLength = SpeciesLength.MR_findFirst()
            let timeOfDay = TimeOfDayOption.MR_findFirst()
            let uuid = NSUUID().UUIDString
            
            let draft = DraftCapture.MR_createEntity()
            draft.remoteId = -3
            draft.comments = "comments"
            draft.date = date1
            draft.gpsType = GpsType.DM.rawValue
            draft.latitude = 30.000
            draft.longitude = -90.000
            draft.length = 34
            draft.locationDescription = "location description"
            draft.tagNumber = "LW123222"
            draft.fishCondition = condition
            draft.species = species
            draft.speciesLength = speciesLength
            draft.timeOfDay = timeOfDay
            draft.errorJson = "{\"erros\":null}"
            draft.recapture = NSNumber(bool: true)
            draft.savedAt = date2
            draft.shouldDelete = NSNumber(bool: false)
            draft.shouldSave = NSNumber(bool: true)
            draft.status = nil
            draft.uuid = uuid
            draft.updatedAt = date3
            
            draft.managedObjectContext?.MR_saveToPersistentStoreAndWait()
            
            let enteredDraft = draft.convertToEnteredDraftCapture()
            
            XCTAssertEqual(draft.remoteId, enteredDraft.remoteId, "")
            XCTAssertEqual(draft.comments!, enteredDraft.comments!, "")
            XCTAssertEqual(draft.date, enteredDraft.date, "")
            XCTAssertEqual(draft.gpsType, GpsType.DM.rawValue, "")
            XCTAssertEqual(draft.latitude, enteredDraft.latitude, "")
            XCTAssertEqual(draft.longitude, enteredDraft.longitude, "")
            XCTAssertEqual(draft.length!, enteredDraft.length!, "")
            XCTAssertEqual(draft.locationDescription!, enteredDraft.locationDescription!, "")
            XCTAssertEqual(draft.tagNumber, enteredDraft.tagNumber, "")
            XCTAssertEqual(draft.fishCondition!.remoteId, enteredDraft.fishCondition!.remoteId, "")
            XCTAssertEqual(draft.species.remoteId, enteredDraft.species.remoteId, "")
            XCTAssertEqual(draft.speciesLength!.remoteId, enteredDraft.speciesLength!.remoteId, "")
            XCTAssertEqual(draft.timeOfDay!.remoteId, enteredDraft.timeOfDay!.remoteId, "")
            
            XCTAssertEqual(draft.errorJson, enteredDraft.errorJson, "")
            
            XCTAssertEqual(draft.recapture, enteredDraft.recapture, "")
            XCTAssertEqual(draft.savedAt, enteredDraft.savedAt, "")
            XCTAssertEqual(draft.shouldDelete, enteredDraft.shouldDelete, "")
            XCTAssertEqual(draft.shouldSave, enteredDraft.shouldSave, "")
            XCTAssertNil(enteredDraft.status, "")
            XCTAssertEqual(draft.uuid, enteredDraft.uuid, "")
            XCTAssertEqual(draft.updatedAt, enteredDraft.updatedAt, "")
            
            expectation.fulfill()
            
        })
        
        waitForExpectationsWithTimeout(30, handler: { error in
            if error != nil {
                print("\(error)")
            }
        })
        
    }
    
    func testFindAllNotDeleted() {
        /*
            @NSManaged var comments: String?
            @NSManaged var date: NSDate
            @NSManaged var gpsType: String
            @NSManaged var latitude: NSNumber
            @NSManaged var length: NSNumber?
            @NSManaged var locationDescription: String?
            @NSManaged var longitude: NSNumber
            @NSManaged var tagNumber: String
            @NSManaged var fishCondition: FishConditionOption?
            @NSManaged var species: Species
            @NSManaged var speciesLength: SpeciesLength?
            @NSManaged var timeOfDay: TimeOfDayOption?
            @NSManaged var errorJson: String
            @NSManaged var recapture: NSNumber
            @NSManaged var savedAt: NSDate
            @NSManaged var shouldDelete: NSNumber
            @NSManaged var shouldSave: NSNumber
            @NSManaged var status: DraftCaptureStatus
            @NSManaged var uuid: String?
        */
        
        let expectation = expectationWithDescription("api")
        
        Api.FetchPublicData.execute({
            let date1 = NSDate()
            let date2 = NSDate()
            let date3 = NSDate()
            let condition = FishConditionOption.MR_findFirst()
            let species = Species.MR_findFirst()
            let speciesLength = SpeciesLength.MR_findFirst()
            let timeOfDay = TimeOfDayOption.MR_findFirst()
            let uuid = NSUUID().UUIDString
            
            let draft = DraftCapture.MR_createEntity()
            draft.remoteId = -3
            draft.comments = "comments"
            draft.date = date1
            draft.gpsType = GpsType.DM.rawValue
            draft.latitude = 30.000
            draft.longitude = -90.000
            draft.length = 34
            draft.locationDescription = "location description"
            draft.tagNumber = "LW123222"
            draft.fishCondition = condition
            draft.species = species
            draft.speciesLength = speciesLength
            draft.timeOfDay = timeOfDay
            draft.errorJson = "{\"erros\":null}"
            draft.recapture = NSNumber(bool: true)
            draft.savedAt = date2
            draft.shouldDelete = NSNumber(bool: true)
            draft.shouldSave = NSNumber(bool: true)
            draft.status = nil
            draft.uuid = uuid
            draft.updatedAt = date3
            
            draft.managedObjectContext?.MR_saveToPersistentStoreAndWait()
            
            let items = DraftCapture.findAllWhereNotDelted()
            
            for item in items  {
                XCTAssertNotEqual(item.uuid, uuid, "")
                XCTAssertEqual(item.shouldDelete, NSNumber(bool: false),  "")
            }
            
            expectation.fulfill()
            
        })
        
        waitForExpectationsWithTimeout(30, handler: { error in
            if error != nil {
                print("\(error)")
            }
        })
        
    }
    
}
