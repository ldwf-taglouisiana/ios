
//
//  TimeOfDayOptionTestCase.swift
//  taglouisiana
//
//  Created by Daniel Ward on 4/20/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import UIKit
import XCTest
import MagicalRecord
@testable import taglouisiana

class TimeOfDayOptionTestCase: XCTestCase {

    override func setUp() {
        super.setUp()
        
        MagicalRecord.setupStackWithInMemoryStore()
//        MagicalRecord.setDefaultModelFromClass(self.classForCoder)
    }
    
    override func tearDown() {
        MagicalRecord.cleanUp()
        
        super.tearDown()
    }
    
    // TODO when Swift testing is fixed, we can try this again.

//    func testcurrentTimeForDate() {
//        let expectation = expectationWithDescription("api")
//        
//        Api.FetchAllData.execute({
//            
//            // time is in America/Central
//           
//            // 2:00 AM
//            var date = NSDate(timeIntervalSince1970: 1429513200)
//            
//            XCTAssertEqual(TimeOfDayOption.currentTimeForDate(date, context: nil).remoteId, 1, "")
//            
//            // ----------------
//            
//            // 5:00 AM
//            date = NSDate(timeIntervalSince1970: 1429524000)
//            
//            XCTAssertEqual(TimeOfDayOption.currentTimeForDate(date, context: nil).remoteId, 2, "")
//            
//            // ----------------
//            
//            // 7:00 AM
//            date = NSDate(timeIntervalSince1970: 1429531200)
//            
//            XCTAssertEqual(TimeOfDayOption.currentTimeForDate(date, context: nil).remoteId, 3, "")
//            
//            // ----------------
//            
//            // 11:00 AM
//            date = NSDate(timeIntervalSince1970: 1429545600)
//            
//            XCTAssertEqual(TimeOfDayOption.currentTimeForDate(date, context: nil).remoteId, 4, "")
//
//            // ----------------
//            
//            // 2:00 PM
//            date = NSDate(timeIntervalSince1970: 1429556400)
//            
//            XCTAssertEqual(TimeOfDayOption.currentTimeForDate(date, context: nil).remoteId, 5, "")
//            
//            // ----------------
//            
//            // 5:00 PM
//            date = NSDate(timeIntervalSince1970: 1429567200)
//            
//            XCTAssertEqual(TimeOfDayOption.currentTimeForDate(date, context: nil).remoteId, 6, "")
//            
//            // ----------------
//            
//            // 7:00 PM
//            date = NSDate(timeIntervalSince1970: 1429574400)
//            
//            XCTAssertEqual(TimeOfDayOption.currentTimeForDate(date, context: nil).remoteId, 7, "")
//            
//            // ----------------
//            
//            //  11:00 PM
//            date = NSDate(timeIntervalSince1970: 1429585200)
//            
//            XCTAssertEqual(TimeOfDayOption.currentTimeForDate(date, context: nil).remoteId, 8, "")
//
//            // ----------------
//            
//            expectation.fulfill()
//            
//        })
//        
//        waitForExpectationsWithTimeout(25, handler: { error in
//            if error != nil {
//                println("\(error)")
//            }
//        })
//
//    }
}
