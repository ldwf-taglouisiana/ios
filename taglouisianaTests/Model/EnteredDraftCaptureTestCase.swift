//
//  EnteredDraftCaptureTestCase.swift
//  taglouisiana
//
//  Created by Daniel Ward on 4/14/15.
//  Copyright (c) 2015 Daniel Ward. All rights reserved.
//

import UIKit
import XCTest
import MagicalRecord
@testable import taglouisiana

class EnteredDraftCaptureTestCase: XCTestCase {

    override func setUp() {
        super.setUp()
        
        MagicalRecord.setupStackWithInMemoryStore()
//        MagicalRecord.setDefaultModelFromClass(self.classForCoder)
    }
    
    override func tearDown() {
        MagicalRecord.cleanUp()
        
        super.tearDown()
    }

    func testAllAsDictionary() {
        
        let expectation = expectationWithDescription("testThatEntityWasSaved")
        
        Api.FetchPublicData.execute({
            let date1 = NSDate()
            let date2 = NSDate()
            let date3 = NSDate()
            let condition = FishConditionOption.MR_findFirst()
            let species = Species.MR_findFirst()
            let speciesLength = SpeciesLength.MR_findFirst()
            let timeOfDay = TimeOfDayOption.MR_findFirst()
            let uuid = NSUUID().UUIDString
            
            let draft = EnteredDraftCapture.MR_createEntity() as EnteredDraftCapture
            draft.remoteId = -3
            draft.comments = "comments"
            draft.date = date1
            draft.gpsType = GpsType.DM.rawValue
            draft.latitude = 30.000
            draft.longitude = -90.000
            draft.length = 34
            draft.locationDescription = "location description"
            draft.tagNumber = "LW123222"
            draft.fishCondition = condition
            draft.species = species
            draft.speciesLength = speciesLength
            draft.timeOfDay = timeOfDay
            draft.errorJson = "{\"errors\":null}"
            draft.recapture = NSNumber(bool: true)
            draft.savedAt = date2
            draft.shouldDelete = NSNumber(bool: false)
            draft.shouldSave = NSNumber(bool: true)
            draft.status = nil
            draft.uuid = uuid
            draft.updatedAt = date3
            
            draft.managedObjectContext?.MR_saveToPersistentStoreAndWait()
            
            // ----------------
            
            let dictionary = draft.toDictionary()
            
            XCTAssertEqual((dictionary["id"] as! Int), -3, "")
            XCTAssertEqual(dictionary["comments"] as? String, "comments", "")
            XCTAssertEqual(dictionary["capture_date"] as? String,  date1.iso8601, "")
            XCTAssertEqual(dictionary["entered_gps_type"] as? String,  GpsType.DM.rawValue, "")
            XCTAssertEqual(dictionary["latitude"] as? NSNumber, 30.000, "")
            XCTAssertEqual(dictionary["longitude"] as? NSNumber,  -90.000, "")
            XCTAssertEqual(dictionary["length"] as? NSNumber, 34, "")
            XCTAssertEqual(dictionary["location_description"] as? String, "location description", "")
            XCTAssertEqual(dictionary["tag_number"] as? String,  "LW123222", "")
            XCTAssertEqual(dictionary["fish_condition_option_id"] as? NSNumber,  condition.remoteId, "")
            XCTAssertEqual(dictionary["species_id"] as? NSNumber,  species.remoteId, "")
            XCTAssertEqual(dictionary["species_length_id"] as? NSNumber, speciesLength.remoteId, "")
            XCTAssertEqual(dictionary["time_of_day_option_id"] as? NSNumber,  timeOfDay.remoteId, "")
            XCTAssertEqual(dictionary["error_json"] as? String,  "{\"errors\":null}", "")
            XCTAssertEqual(dictionary["recapture"] as? NSNumber,  NSNumber(bool: true), "")
            XCTAssertEqual(dictionary["saved_at"] as? String,  date2.iso8601, "")
            XCTAssertEqual(dictionary["should_delete"] as? NSNumber,  NSNumber(bool: false), "")
            XCTAssertEqual(dictionary["should_save"] as? NSNumber,  NSNumber(bool: true), "")
            XCTAssertNil(dictionary["status"], "")
            XCTAssertEqual(dictionary["uuid"] as? String,  uuid, "")
            XCTAssertEqual(dictionary["version"] as? String,  date3.iso8601, "")
            
            expectation.fulfill()
            
        })
        
        waitForExpectationsWithTimeout(25, handler: { error in
            if error != nil {
                print("\(error)")
            }
        })

    }
}
